<?php declare(strict_types=1);

namespace Core\Page;

use Common\Http\Etag;
use Common\Http\Response;
use Core\Action\ActionInterface;
use Core\LoggerInterface;
use Di\Attribute\Required;

class ContentRouter implements ActionInterface
{
    #[Required] protected CurrentPage $currentPage;
    #[Required] protected Response $response;
    #[Required] protected LoggerInterface $logger;

    function __invoke(Etag $etag)
    {
        try {
            if ($etag->ifMatch()) {
                return true;
            }

            $content = $this->currentPage->generateContent();

            if ($etag->ifContentMatch($content)) {
                return true;
            }

            $this->sendContent($content);
            return true;
        } catch (\Throwable $e) {
            $this->response->setStatus(500);
            $this->response->flushHeaders();
            $this->logger->critical($e);
            throw $e;
        }
    }

    function sendContent($content)
    {
        if ($this->currentPage->isPrivate()) {
            $this->response->setHeader('Cache-Control', 'private');
        }

        $this->response->setHeader('Vary', 'Cookie');
        $this->response->setHtml($content);

        if ($this->currentPage->isCacheable() && $this->response->isStatus(200)) {
            $this->currentPage->responseCache->write($content);
        }
    }
}

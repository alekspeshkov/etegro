<?php declare(strict_types=1);

namespace test;

use UnitTestCase;
use Core\Page\PageNaming;

class ContentPageTest extends UnitTestCase
{
    function test_getParent() {
        $test = [
            ['path/to/page', 'path/to'],
            ['site', ''],
            ['', ''],
        ];

        foreach ($test as $case) {
            $this->assertIdentical(
                PageNaming::getParent($case[0]),
                $case[1]
            );
        }

    }

    function test_pageMatchList() {
        $test = [
            //['path/to/page', ['path/to/page', 'path/to/page*', 'path/to/*', 'path/to*', 'path/*', 'path*', '*']],
            //['path/to/*', ['path/to/*', 'path/to*', 'path/*', 'path*', '*']],
            //['path/to*', ['path/to*', 'path/*', 'path*', '*']],
            //['path/to', ['path/to', 'path/to*', 'path/*', 'path*', '*']],
            //['site/*', ['site/*', 'site*', '*']],
            //['site*', ['site*', '*']],
            //['site', ['site', 'site*', '*']],
            //['*', ['*']],
            //['', ['*']],
            ['site:path/to/page', ['site:path/to/page', 'site:path/to/page*', 'site:path/to/*', 'site:path/to*', 'site:path/*', 'site:path*', 'site:*', '*']],
            ['site:path/to/*', ['site:path/to/*', 'site:path/to*', 'site:path/*', 'site:path*', 'site:*', '*']],
            ['site:path/to*', ['site:path/to*', 'site:path/*', 'site:path*', 'site:*', '*']],
            ['site:path/to', ['site:path/to', 'site:path/to*', 'site:path/*', 'site:path*', 'site:*', '*']],
            ['site:*', ['site:*', '*']],
            ['site:', ['site:', 'site:*', '*']],
        ];

        foreach ($test as $case) {
            $this->assertIdentical(
                PageNaming::routeCandidates($case[0]),
                $case[1]
            );
        }
    }

    function test_ancestorsAndSelf() {
        $test = [
            //['path/to/page',['path/to/page', 'path/to', 'path']],
            //['site', ['site']],
            //['', []],
            ['site:path/to/page',['site:path/to/page', 'site:path/to', 'site:path', 'site:']],
            ['site:path',['site:path', 'site:']],
            ['site:', ['site:']],
            //['', []],
        ];

        foreach ($test as $case) {
            $this->assertIdentical(
                PageNaming::ancestorsAndSelf($case[0]),
                $case[1]
            );
        }

    }

}

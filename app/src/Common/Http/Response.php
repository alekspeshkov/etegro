<?php declare(strict_types=1);

namespace Common\Http;

class Response
{
    protected int $statusCode = 200;

    /** @var array<string, string> */
    protected array $headerData = [];

    /** @var array<string, array> */
    protected array $cookieData = [];

    protected string $body = '';

    function __destruct()
    {
        $this->flush();
    }

    function flush()
    {
        $this->flushHeaders();
        $this->flushBody();
    }

    function flushHeaders()
    {
        if (headers_sent()) {
            trigger_error('Headers already sent');
            return;
        }

        ksort($this->headerData);
        foreach ($this->headerData as $name => $value) {
            header("{$name}: {$value}");
        }

        ksort($this->cookieData);
        foreach ($this->cookieData as $args) {
            setcookie(...$args);
        }

        // should be after possible 'Location' header
        http_response_code($this->statusCode);

        flush();
        $this->statusCode = 200;
        $this->headerData = [];
        $this->cookieData = [];
    }

    function flushBody()
    {
        echo $this->body;

        flush();
        $this->body = '';
    }

    function setHtml(string $html)
    {
        $this->setContentType('text/html');
        $this->setContent($html);
    }

    function setText(string $text)
    {
        $this->setContentType('text/plain');
        $this->setContent($text);
    }

    protected function setContent(string $content)
    {
        $this->setHeader('Content-Length', (string)strlen($content));
        $this->body = $content;
    }

    function setRedirect(string $location, int $status = 303)
    {
        $this->setStatus($status);
        $this->setHeader('Location', $location);
    }

    function setHeader(string $name, string $value)
    {
        $this->headerData[$name] = $value;
    }

    function setDateHeader(string $name, Date $date)
    {
        $this->setHeader($name, (string)$date);
    }

    function setContentType(string $contentType, string $charset = 'UTF-8')
    {
        $this->setHeader('Content-Type', "{$contentType}; charset={$charset}");
    }

    function setCookie(string $name, ?string $value)
    {
        $c = session_get_cookie_params();

        if (is_null($value)) {
            $value = '';
            $expires = 1;
        } else {
            $lifetime = $c['lifetime'];
            $expires = ($lifetime > 1) ? time() + $lifetime : 1;
        }

        $this->cookieData[$name] = [$name, $value, $expires, $c['path'], $c['domain'], $c['secure'], $c['httponly']];
    }

    function setStatus(int $httpStatusCode)
    {
        if ($httpStatusCode < 200 || $httpStatusCode > 599) {
            trigger_error("Invalid HTTP status code: '{$httpStatusCode}'");
            $httpStatusCode = 500;
        }

        $this->statusCode = $httpStatusCode;
    }

    function isStatus(int $code): bool
    {
        return $this->statusCode === $code;
    }
}

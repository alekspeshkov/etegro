<?php declare(strict_types=1);

namespace Site\Admin;

use Common\File;
use Common\Str;
use Di\Attribute\Required;

class BackupFile
{
    #[Required] protected string $storageDir;
    #[Required] protected string $baseUrl;
    #[Required] protected string $latestFile;

    protected function storageFile(string $file)
    {
        return Str::concatDir([$this->storageDir, $file]);
    }

    function latestUrl()
    {
        return Str::concatUri([$this->baseUrl, $this->latestFile]);
    }

    function dailyUrl()
    {
        $files = scandir($this->storageDir);
        $daily = array_pop($files);
        if ($daily == $this->latestFile) {
            $daily = array_pop($files);
        }
        if ($daily == '..') {
            $daily = '';
        }

        if (!empty($daily)) {
            return Str::concatUri([$this->baseUrl, $daily]);
        }
    }

    function backup(string $dump)
    {
        $latestFile = $this->storageFile($this->latestFile);

        if (is_readable($latestFile)) {
            if ($dump == file_get_contents($latestFile)) {
                return; // avoid duplicate identical backups
            }

            // backup previous version
            $time = filemtime($latestFile);
            $dailyFile = $this->dailyFile($time);
            if (!is_readable($dailyFile)) {
                rename($latestFile, $dailyFile);
            }
        }

        File::write($latestFile, $dump);
    }

    function dailyFile(int $timestamp)
    {
        return $this->storageFile(date('Ymd', $timestamp) . '.sql');
    }
}

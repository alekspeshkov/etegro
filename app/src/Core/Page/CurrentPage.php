<?php declare(strict_types=1);

namespace Core\Page;

use Common\Normalize;
use Core\User\User;
use Core\Xml\Xslt;
use Di\Attribute\Lazy;
use Di\Attribute\Required;

class CurrentPage extends Page
{
    #[Required] protected PageRouting $pageRouting;
    #[Required] protected LayoutContent $layoutContent;
    #[Required] protected User $user;
    #[Required] protected XmlWriter $xml;
    #[Required] protected Xslt $xslt;

    #[Lazy] public ResponseCache $responseCache;
    #[Lazy] protected Table $pageFullContent;

    protected function __construct(?string $name)
    {
        $name ??= $this->pageNaming->getCurrent();
        parent::__construct($name);

        $this->lazyConfig->configure([
            'pageFullContent' => ['::entity' => fn () => $this->pageContent->naturalLeftJoin('page_title')->naturalLeftJoin('page_menu_order')],
            'responseCache' => ['::config' => fn () => ['path' => $this->getPathName()]],
        ]);
    }

    /** create full html representation */
    function generateContent(): string
    {
        $controller = $this->getControllerPage();
        $layout = $this->pageRouting->fetchOne('layout', $controller);
        $content = $controller->fetchFullContent();

        $content['layout'] = $this->layoutContent->fetch($layout);
        $content['controller'] = $this->xml->esc($content['path']);
        $content['path'] = $this->xml->esc($this->getPathName());
        $content['private'] = $this->isPrivate();

        $xml = $this->xml->xmlPage($content);
        $xslName = $this->pageRouting->fetchOne('template', $controller);
        $xhtml = $this->xslt->transformToXML($xml, $xslName);

        $xhtml = Normalize::normalizeHtml($xhtml);
        return $xhtml;
    }

    private readonly string $controllerName;

    protected function getControllerName(): string
    {
        return $this->controllerName ??= $this->pageRouting->fetchOne('content', $this);
    }

    private readonly Page $controllerPage;

    /** page controller is actual page template content */
    protected function getControllerPage(): static
    {
        return $this->controllerPage ??= (function () {
            if (!$this->isAccessible()) {
                $controller = $this->pageRouting->fetchOne('forbidden', $this);
            } else {
                $label = $this->pageTitle->wherePage($this)->fetchOneOrNull('label');
                if (!empty($label)) {
                    return $this;
                }
                $controller = $this->getControllerName();
            }
            return $this->getPage($controller, static::class);
        })();
    }

    private int $contentDepth;

    function fetch404Content(): ?string
    {
        if (!isset($this->contentDepth)) {
            $this->contentDepth = 0;
        } else {
            ++$this->contentDepth;
        }

        $routes = $this->pageRouting->fetchAll('content', $this);
        $routes = array_slice($routes, $this->contentDepth, null, true);
        if (empty($routes)) {
            return null;
        }
        /** @var string */
        $pageName = key($routes);

        return $this->pageContent->where(['page' => $pageName])->fetchOne('content');
    }

    private readonly array $fullContent;

    /** content and all meta information */
    function fetchFullContent()
    {
        return $this->fullContent ??= (function () {
            $content = $this->pageFullContent
                ->wherePage($this)
                ->fetchRowOrNull() ?? [];
            $content['path'] = $this->getPathName();

            return $content;
        })();
    }

    private readonly array $routeCandidates;

    /** @return string[]> */
    function getRouteCandidates()
    {
        return $this->routeCandidates ??= $this->pageNaming->routeCandidates($this->getPageName());
    }

    private readonly array $roles;

    /** @return array<string, array<string, string>> */
    function getRoles()
    {
        return $this->roles ??= $this->pageRouting->fetchAll('role', $this);
    }

    function isPrivate(): bool
    {
        return (bool)$this->getRoles();
    }

    function isAccessible(): bool
    {
        return $this->user->canAccess($this);
    }

    function isCacheable()
    {
        return !$this->isPrivate()
            && $this->formData->isEmpty()
            && $this->flashData->isEmpty();
    }

}

<?php declare(strict_types=1);

namespace Site\Admin;

use Common\Db\Connection as Db;
use Common\Db\Transaction;
use Common\Http\Redirect;
use Common\Str;
use Core\Page\CurrentPage;
use Di\Attribute\Required;

class AdminPage extends CurrentPage
{
    /** @var string[] */
    static array $tablesNames = ['page_content', 'page_title', 'page_menu_order'];

    #[Required] protected Db $db;
    #[Required] protected Redirect $redirect;

    function fetchHiddenChildren()
    {
        $result = $this->pageFullContent
            ->where(['title' => null], '~ IS NULL')
            ->whereParent($this)
            ->orderFunc('IFNULL(~, 100)', ['rank'])->order('label')->orderDesc('page')
            ->selectFunc("IFNULL(label, SUBSTRING_INDEX(page,'/',-1)) AS label")
            ->fetchAll('page');
        return $this->pageNaming->injectPath($result);
    }

    function fetchFullAncestors()
    {
        $pages = $this->getAncestorsAndSelf();
        if (empty($pages)) {
            return [];
        }

        $result = $this->pageFullContent
            ->where(['page' => $pages])->orderDesc(['page' => $pages])
            ->selectFunc("IFNULL(label, SUBSTRING_INDEX(page,'/',-1)) AS label")
            ->fetchAll('page');
        return $this->pageNaming->injectPath($result);
    }

    private readonly string $controllerBase;

    protected function getControllerBase(): string
    {
        return $this->controllerBase ??= (function () {
            $controller = $this->getControllerName();

            // strip match pattern suffix
            if (strpos($controller, '*') !== false) {
                $controller = substr($controller, 0, strpos($controller, '*') - 1);
                $controller = rtrim($controller, '/');
            }
            return $controller;
        })();
    }

    private readonly string $controllerTail;

    function getControllerTail(): string
    {
        return $this->controllerTail ??= (function () {
            $base = $this->getControllerBase();
            $tail = substr($this->getPageName(), strlen($base));
            $tail = ltrim($tail, '/');
            return $tail;
        })();
    }

    function getAdminPage(string $targetPage)
    {
        $adminBase = $this->getControllerBase();
        $adminTargetPage = $this->getPage(Str::concatUri([$adminBase, $targetPage]));
        return $adminTargetPage;
    }

    function setRedirectAdminPage(string $targetPage)
    {
        $adminTargetPage = $this->getAdminPage($targetPage);
        $adminTargetPage->formData->clear();
        $this->redirect->setPostBackPath($adminTargetPage->getPathName());
        return $adminTargetPage;
    }

    protected function filterCols(string $tableName, array $values): array
    {
        // meta info about the table $tableName
        $cols = $this->db->fetchCol('DESCRIBE ~', [$tableName]);

        return array_intersect_key($values, array_flip($cols));
    }

    function save(array $cols)
    {
        $cols = array_filter($cols, fn ($e) => !empty($e));

        $sql = [];
        foreach (static::$tablesNames as $tableName) {
            $theTable = $this->table->from($tableName);
            $data = $this->filterCols($tableName, $cols);

            //TRICK: currently the 1 column used as a key
            if (count($data) > 1) {
                $sql[] = $theTable->prepare_replace($data);
            } else {
                $sql[] = $theTable->prepare_delete($data);
            }
        }

        Transaction::executeTransaction($this->db, $sql);
        $this->responseCache->clear();
    }

    function delete(string $pageName)
    {
        $this->save(['page' => $pageName]);
    }
}

<?php declare(strict_types=1);

namespace Core\Xml\View;

class NotFoundException extends \UnderflowException
{
}

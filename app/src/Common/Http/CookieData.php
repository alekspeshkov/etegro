<?php declare(strict_types=1);

namespace Common\Http;

class CookieData extends \Common\ArraySymlink
{
    function __construct(
        protected Response $response
    ) {
        parent::__construct('$_COOKIE');
    }

    function offsetSet($name, $value): void
    {
        if (parent::set($name, $value)) {
            $this->response->setCookie($name, $value);
        }
    }

    function clear(): void
    {
        foreach ($this->getAll() as $name => $value) {
            // unset $name and create cookie to unset $name remotely
            $this->offsetSet($name, null);
        }
    }
}

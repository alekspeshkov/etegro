<?php declare(strict_types=1);

namespace Site\Ru;

use Core\Action\ActionRouter;
use Di\Attribute\Required;
use Site\Environment;

class App extends \Core\Page\App
{
    #[Required] protected Environment $environment;

    function __construct()
    {
        parent::__construct();

        $this->appConfig
            ->configure($this->environment->getConfig('ru'))
            ->configure([
                ActionRouter::class => ['phpActionNamespace' => __NAMESPACE__ . '\Action'],
                \Core\Xml\View\Helper::class => View\Helper::class,
            ]);
    }
}

<?php declare(strict_types=1);

namespace Core\Xml\View;

abstract class FileHelper
{
    /** Collect directories of class files using reflection
     * @return array<string, string>
     */
    static function getDirNames(object|string $objectOrClass): array
    {
        $result = [];

        for ($class = new \ReflectionClass($objectOrClass); $class; $class = $class->getParentClass()) {
            $fileName = $class->getFileName();
            if (!$fileName) {
                break;
            }
            $dirName = dirname($fileName);
            $result[$class->getName()] = $dirName;
        }

        return $result;
    }
}

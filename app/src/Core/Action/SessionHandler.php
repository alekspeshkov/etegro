<?php declare(strict_types=1);

namespace Core\Action;

use Common\Db\Table;

class SessionHandler implements \SessionHandlerInterface, \SessionUpdateTimestampHandlerInterface
{
    static string $tableName = 'session_data';
    protected readonly Table $theTable;

    function __construct(Table $table)
    {
        $this->theTable = $table->from(static::$tableName);
    }

    function open(string $path, string $name): bool
    {
        return true;
    }

    function close(): bool
    {
        return true;
    }

    function read(string $id): string|false
    {
        /** @var ?string */
        $json = $this->theTable->where(['id' => $id])->fetchOneOrNull('data');

        if (is_null($json)) {
            return '';
        }

        $data = json_decode($json, null, 16, JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);
        return serialize($data);
    }

    function write(string $id, string $data): bool
    {
        if (empty($data)) {
            return $this->destroy($id);
        }

        $array = unserialize($data);
        $json = json_encode($array, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE);

        $this->theTable->replace(['id' => $id, 'data' => $json]);
        return true;
    }

    function destroy(string $id): bool
    {
        $this->theTable->delete(['id' => $id]);
        return true;
    }

    function gc(int $max_lifetime): int|false
    {
        $time = time() - $max_lifetime;
        return $this->theTable->delete(['atime' => $time], 'UNIX_TIMESTAMP(~) < ?');
    }

    public function updateTimestamp(string $id, string $data): bool
    {
        $this->theTable->where(['id' => $id])->update(['atime' => time()], '~ = FROM_UNIXTIME(?)');
        return true;
    }

    public function validateId(string $id): bool
    {
        return $this->theTable->where(['id'=> $id])->exists();
    }

}

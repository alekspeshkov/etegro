<?php declare(strict_types=1);

namespace Site\Admin\Action;

use Common\Http\UploadedFiles;
use Common\Db\Connection as Db;
use Common\Db\Transaction;

class DbExecute extends Action
{
    function __invoke(Db $db, UploadedFiles $uploadedFiles)
    {
        $visitor = function ($file) use ($db) {
            $tmpname = $file['tmp_name'];
            $sql = file_get_contents($tmpname);
            unlink($tmpname);

            try {
                $result = Transaction::executeFile($db, $sql);
            } catch (\Throwable $e) {
                $this->logger->error($e->getMessage());
                $this->currentPage->flashData['database'] = $e->getMessage();
                return false;
            }

            //TRICK: clear all page forms
            $adminTargetRootPage = $this->currentPage->getAdminPage('');
            $adminTargetRootPage->formData->clear();
            $adminTargetRootPage->flashData->clear();

            $this->currentPage->responseCache->clearAll();

            $this->currentPage->flashData['affected'] = " {$result}";
        };

        $errors = $uploadedFiles->visit($visitor);
        $this->currentPage->flashData->merge($errors);
    }
}

<?php declare(strict_types=1);

use Di\ConfigRecord as Record;
use Di\AppConfigRegistry as Registry;

class DiConfigTest extends UnitTestCase
{
    protected Registry $config;

    function setUp()
    {
        $this->config = new Registry(Record::create());
    }

    function test_trivialCase()
    {
        $record = $this->config->createRecord('unknown');
        $this->assertClone(
            $this->config->resolveFrom($record),
            $record
        );

        $this->config->configure([
            'a' => 'b'
        ]);
        $this->assertClone(
            $this->config->resolve('a'),
            $this->config->createRecord('b')
        );
        $this->assertClone(
            $this->config->resolve('b'),
            $this->config->createRecord('b')
        );

        $this->config->configure([
            'a' => 'c'
        ]);
        $this->assertClone(
            $this->config->resolve('a'),
            $this->config->createRecord('c')
        );
        $this->assertClone(
            $this->config->resolve('b'),
            $this->config->createRecord('b')
        );
        $this->assertClone(
            $this->config->resolve('c'),
            $this->config->createRecord('c')
        );
    }

    function test_simpleCase()
    {
        $this->config->configure([
            'a' => 'b',
            'b' => 'c',
        ]);

        $this->assertClone(
            $this->config->resolve('c'),
            $this->config->createRecord('c'),
        );

        $this->assertClone(
            $this->config->resolve('b'),
            $this->config->createRecord('c'),
        );

        $this->assertClone(
            $this->config->resolve('a'),
            $this->config->createRecord('c'),
        );
    }

    function test_against_alias_to_itself()
    {
        $this->config->configure(['A' => 'A']);

        $this->assertClone(
            $this->config->resolve('A'),
            $this->config->createRecord('A'),
        );
    }

    function test_against_circular_dependence()
    {
        $this->config->configure(['A' => 'B']);
        $this->catchAssertion(function () {
            $this->config->configure(['B' => 'A']);
        });
    }

    function test_against_complex_circular_dependence()
    {
        $this->config->configure(['A' => 'B']);
        $this->config->configure(['B' => 'C']);
        $this->config->configure(['C' => 'D']);
        $this->catchAssertion(function () {
            $this->config->configure(['C' => 'A']);
        });
    }

    function catchAssertion(callable $test)
    {
        try {
            call_user_func($test);
        } catch (\AssertionError $e) {
            $this->pass();
            return;
        }

        $this->fail();
    }
}

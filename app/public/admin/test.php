<?php declare(strict_types=1);
require __DIR__ . '/../../src/autoload.php';

(function (string $baseDir) {
    $simpletest = "{$baseDir}/vendor/simpletest/simpletest/src";

    // tests the simpletest
    //require_once "{$simpletest}/test/unit_tests.php";
    //require_once "{$simpletest}/test/all_tests.php";

    // simpletest version 1.1beta or the above requered
    require_once "{$simpletest}/unit_tester.php";
    $tests = new TestSuite('Unit tests');

    $testsDir = "{$baseDir}/simpletests";
    $tests->addFile("{$testsDir}/StrConcatTest.php");
    $tests->addFile("{$testsDir}/ArrayExtTest.php");
    $tests->addFile("{$testsDir}/DiConfigTest.php");
    $tests->addFile("{$testsDir}/ContentPageTest.php");
    $tests->addFile("{$testsDir}/HttpRequestTest.php");
    $tests->addFile("{$testsDir}/XmlWriterTest.php");
    $tests->addFile("{$testsDir}/DbQuoteTest.php");
    $tests->addFile("{$testsDir}/DbTableTest.php");
    $tests->addFile("{$testsDir}/HttpDateTest.php");

    require_once "{$simpletest}/reporter.php";

    $mtime = array_sum( explode(' ', microtime()) );
    $result = $tests->run(new HtmlReporter('UTF-8'));
    $mtime = array_sum( explode(' ', microtime()) ) - $mtime;
    echo "Tests done in $mtime seconds";

    if (!$result) {
        include __DIR__ . '/phpinfo.php';
    }
    return $result;
})(realpath(__DIR__ . '/../../'));

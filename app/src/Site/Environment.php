<?php declare(strict_types=1);

namespace Site;

use Common\Db\Connection as Db;
use Common\EnvData;
use Common\Str;
use Core\LoggerInterface;
use Core\Page\PageNaming;
use Core\Page\ResponseCache;
use Di\Attribute\Required;

class Environment
{
    #[Required] protected EnvData $env;

    /** @return array<string, string|mixed[]|array<string, mixed>> */
    function getConfig(string $siteName = 'app'): array
    {
        $env = $this->env;
        return [
            Db::class => [
                'dsn' => $env['MYSQL_DSN'],
                'username' => $env['MYSQL_USER'],
                'password' => $env['MYSQL_PASSWORD'],
            ],
            PageNaming::class => ['currentSite' => $siteName],
            ResponseCache::class => [ 'storageDir' => Str::concatDir([$env['APP_VAR'], 'cache', $siteName]) ],
            LoggerInterface::class => [
                '::class' => Logger::class,
                'name' => $siteName,
                'file' => Str::concatDir([$env['APP_VAR'], "{$siteName}.log"]),
            ],
        ];
    }
}

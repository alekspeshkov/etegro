<?php declare(strict_types=1);

namespace Site\Admin;

use Di\Attribute\Required;

class ActionRouter extends \Core\Action\ActionRouter
{
    #[Required] protected string $loginAction;
    #[Required] protected AdminPage $currentPage;

    protected function resolveActions(): bool
    {
        return $this->isAuthorized() && parent::resolveActions();
    }

    protected function isAuthorized(): bool
    {
        if (isset($this->actions[$this->loginAction])) {
            $this->resolveAction($this->loginAction, $this->actions[$this->loginAction]);
            unset($this->actions[$this->loginAction]);
        }

        if (!empty($this->actions) && !$this->currentPage->isAccessible()) {
            $this->response->setStatus(403);
            $this->logger->error('POST request access denied');
            return false;
        }

        $this->currentPage->flashData->clear();
        return true;
    }
}

<?php declare(strict_types=1);

namespace Di;

use Di\Interface\ContainerAwareInterface;
use Di\Interface\ContainerInterface;
use Di\ReflectionFactory;

class ContainerFactory extends ReflectionFactory implements ContainerAwareInterface
{
    protected ContainerInterface $container;

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /** @param string $className class or interface */
    protected function getInstance(string $className): object
    {
        /** @var object $instance */
        $instance = $this->container->get($className);
        assert ($instance instanceof $className);
        return $instance;
    }
}

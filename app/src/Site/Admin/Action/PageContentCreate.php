<?php declare(strict_types=1);

namespace Site\Admin\Action;

class PageContentCreate extends PageContentUpdate
{
    function __invoke()
    {
        $this->savePostData();
        $this->validateCurrentPage();

        $pageName = $this->currentPage->formData['page'];
        if ($this->getPage($pageName)->exists() && empty($this->currentPage->flashData['page_empty'])) {
            $this->currentPage->flashData['overwrite'] = true;
        }

        return $this->currentPage->flashData->isEmpty() && $this->insert();
    }

    protected function insert()
    {
        try {
            $data = $this->currentPage->formData->getAll();
            $this->currentPage->save($data);

            $adminTargetPage = $this->currentPage->setRedirectAdminPage($data['page']);
            $adminTargetPage->flashData->setAll(['inserted' => true]);

            $this->currentPage->formData->clear();
        } catch (\Throwable $e) {
            $this->currentPage->flashData['database'] = $e->getMessage();
        }
    }
}

<?php declare(strict_types=1);

namespace Site\Admin\Action;

use Core\Action\InvokeAction;
use Core\User\User;

class Login extends Action
{
    use InvokeAction;

    function __invoke(User $user)
    {
        $name = $this->postData['user'];
        $password = $this->postData['pass'];

        if (!$name) {
            $user->logout();
            return;
        }

        if (!$user->login($name, $password)) {
            $this->loginInvalid($name);
            return false;
        }

        $this->invokeAction(DbBackup::class);
    }

    protected function loginInvalid($name)
    {
        sleep(1); //TRICK: weak defense against brute force password attack
        $this->logger->warning("Invalid password for user '{$name}'");
    }
}

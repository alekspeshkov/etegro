<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:a="page"
    xmlns:h="helper"
    exclude-result-prefixes="a h"
>
<xsl:import href='stylesheet://page-helper'/>
<xsl:import href='stylesheet://html'/>
<xsl:import href='stylesheet://html-form'/>

<!-- node-set variables -->
<xsl:variable name="page" select="/page"/>
<xsl:variable name="content" select="$page/content"/>
<xsl:variable name="title" select="$page/title"/>
<xsl:variable name="private" select="$page/private"/>

<!-- string variables -->
<xsl:variable name="path" select="string($page/path)"/>
<xsl:variable name="controller" select="string($page/controller)"/>
<xsl:variable name="name"><xsl:call-template name="basename"><xsl:with-param name="path" select="$path"/></xsl:call-template></xsl:variable>
<xsl:variable name="parent"><xsl:call-template name="dirname"><xsl:with-param name="path" select="$path"/></xsl:call-template></xsl:variable>

<!-- debugging -->
<xsl:template match="@a:*"><xsl:value-of select="local-name()"/></xsl:template>
<xsl:template match="a:*"><xsl:comment><xsl:value-of select="local-name()"/></xsl:comment></xsl:template>
<xsl:template mode="head" match="@a:* | a:*"/>

<xsl:template match="a:content" name="body">
    <xsl:param name="fragment" select="$content/node()"/>
    <xsl:choose>
        <xsl:when test="$fragment">
            <xsl:if test="not($fragment/../h1) and $title">
                <h1><xsl:value-of select="$title"/></h1>
            </xsl:if>
            <xsl:apply-templates select="$fragment"/>
        </xsl:when>
        <xsl:otherwise><xsl:call-template name="not-found-body"/></xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="not-found-head">
    <xsl:apply-templates mode="head" select="h:content('page_not_found_content',$controller)/content"/>
</xsl:template>

<xsl:template name="not-found-body">
    <xsl:apply-templates select="h:content('page_not_found_content',$controller)/content"/>
</xsl:template>

<xsl:template match="a:include[@path]">
    <xsl:apply-templates select="h:content('path_content',@path)/content"/>
</xsl:template>

<!-- control operators -->
<xsl:template match="a:else"/>
<xsl:template match="a:if[@path]">
    <xsl:choose>
        <xsl:when test="@path = $path"><xsl:apply-templates/></xsl:when>
        <xsl:otherwise><xsl:apply-templates select="a:else/node()"/></xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="a:get[@name]"><xsl:value-of select="h:form(@name)"/></xsl:template>
<xsl:template match="a:if[@name]">
    <xsl:choose>
        <xsl:when test="h:form(@name)!=''"><xsl:apply-templates/></xsl:when>
        <xsl:otherwise><xsl:apply-templates select="a:else/node()"/></xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="a:error[@name]" name="error">
    <xsl:param name="name" select="@name"/>
    <xsl:variable name="error" select="h:string('error',$name)"/>
    <xsl:if test="$error!=''"><span class="alert"><xsl:value-of select="$error"/></span></xsl:if>
</xsl:template>
<xsl:template match="a:if[@error]">
    <xsl:choose>
        <xsl:when test="h:string('error',@error)!=''"><span class="alert"><xsl:apply-templates/></span></xsl:when>
        <xsl:otherwise><xsl:apply-templates select="a:else/node()"/></xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="a:attribute"><xsl:apply-templates select="@*"/></xsl:template>
<xsl:template match="a:attribute[@name='href']">
    <xsl:variable name="link">
        <xsl:call-template name="url">
            <xsl:with-param name="href"><xsl:apply-templates/></xsl:with-param>
        </xsl:call-template>
    </xsl:variable>
    <xsl:attribute name="{@name}"><xsl:value-of select="$link"/></xsl:attribute>
</xsl:template>

<xsl:template match="a:header[@status]"><xsl:variable name="side-effect" select="h:string('set_status',@status)"/></xsl:template>

<!-- node-set functions -->
<xsl:template match="/page">
    <xsl:apply-templates select="layout/node()"/>
</xsl:template>

<xsl:template match="/page/content">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="content">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template name="html-ul-li-a-href">
    <xsl:param name="pages"/>
    <xsl:if test="$pages">
        <ul>
        <xsl:for-each select="$pages">
            <li>
                <xsl:if test="starts-with(concat($path,'/'), concat(path,'/')) and not( starts-with(concat($path,'/'), concat(following-sibling::path,'/')) )">
                    <xsl:attribute name="class">
                        <xsl:text>selected</xsl:text>
                    </xsl:attribute>
                </xsl:if>
                <xsl:call-template name="html-a-href"/>
            </li>
        </xsl:for-each>
        </ul>
    </xsl:if>
</xsl:template>

<xsl:template match="a:children" name="path-children">
    <xsl:param name="base" select="$path"/>

    <xsl:call-template name="html-ul-li-a-href">
        <xsl:with-param name="pages" select="h:pages('page_children_pages',$base)/pages/page"/>
    </xsl:call-template>
</xsl:template>

<xsl:template name="section-menu">
    <xsl:variable name="base" select="substring-before(concat($path,'/'),'/')"/>
    <xsl:call-template name="path-children">
        <xsl:with-param name="base" select="$base"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="a:local-navigation">
    <xsl:call-template name="section-menu"/>

    <xsl:variable name="base" select="substring-before(concat($path,'/'),'/')"/>
    <xsl:if test="$base != $parent and $parent != ''">
        <xsl:call-template name="path-children">
            <xsl:with-param name="base" select="$parent"/>
        </xsl:call-template>
    </xsl:if>
</xsl:template>

<xsl:template match="a:breadcrumbs">
    <xsl:param name="pages" select="h:pages('path_ancestors_pages',$path)/pages/page"/>
    <xsl:if test="$pages">
        <ul>
        <xsl:for-each select="$pages">
            <li>
                <xsl:call-template name="html-a-href">
                    <xsl:with-param name="body"><xsl:apply-templates select="label/node()"/></xsl:with-param>
                </xsl:call-template>
            </li>
        </xsl:for-each>
        </ul>
    </xsl:if>
</xsl:template>

<xsl:template match="a:path">
    <xsl:variable name="ancestor" select="h:pages('path_ancestors_pages', $path)/pages/page[position()=last()]/path"/>
    <xsl:if test="$ancestor">
        <xsl:call-template name="html-a-href">
            <xsl:with-param name="href" select="$ancestor"/>
            <xsl:with-param name="body" select="$ancestor/node()"/>
        </xsl:call-template>
    </xsl:if>
    <xsl:value-of select="substring-after($path, $ancestor)"/>
</xsl:template>

</xsl:stylesheet>

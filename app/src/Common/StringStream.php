<?php declare(strict_types=1);

namespace Common;

abstract class StringStream
{
    public $context; // used internally

    protected string $content;
    protected int $size;
    protected int $position;

    abstract function getContent($name);

    function stream_open($uri, $mode, $options, &$opened_path)
    {
        $name = parse_url($uri, PHP_URL_HOST);

        $this->content = $this->getContent($name);
        $this->size = strlen($this->content);
        $this->position = 0;

        return true;
    }

    function stream_read($count)
    {
        $result = substr($this->content, $this->position, $count);
        $this->position += strlen($result);
        return $result;
    }

    function stream_eof()
    {
        return $this->position >= $this->size;
    }

    function stream_flush()
    {
    }

    function stream_close()
    {
    }

    function url_stat()
    {
        return [];
    }
}

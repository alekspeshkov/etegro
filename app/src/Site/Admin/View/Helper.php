<?php declare(strict_types=1);

namespace Site\Admin\View;

use Common\Http\Request;
use Core\Page\GetPage;
use Core\Page\LayoutContent;
use Core\Page\PageRouting;
use Core\User\User;
use Site\Admin\BackupFile;
use Site\Admin\AdminPage as Page;
use Site\Admin\PageSite;

class Helper extends \Core\Page\View\Helper
{
    use GetPage;

    function page_hidden_children_pages($name)
    {
        return $this->getPage($name, Page::class)->fetchHiddenChildren();
    }

    function page_full_ancestors_pages($name)
    {
        return $this->getPage($name, Page::class)->fetchFullAncestors();
    }

    function user($_, User $user)
    {
        return $user->getUserName() ?: '';
    }

    function site_list($_, PageSite $pageSite)
    {
        return $pageSite->getAll();
    }

    function page_refresh($_, Page $currentPage)
    {
        if (!$currentPage->formData->isEmpty()) {
            return;
        }

        $locationPage = $currentPage->getControllerTail();

        if ($this->getPage($locationPage)->exists()) {
            $data = $this->getPage($locationPage, Page::class)->fetchFullContent();
            $currentPage->formData->setAll($data);
        } else {
            $currentPage->formData['page'] = $locationPage;
            $currentPage->flashData['notfound'] =  true;
        }
    }

    function template_list($_, PageRouting $pageRouting)
    {
        return $pageRouting->getAllTemplates();
    }

    function layout_list($_, LayoutContent $layoutContent)
    {
        return $layoutContent->getAll();
    }

    function layout_refresh($_, Page $currentPage, LayoutContent $layoutContent)
    {
        if (!$currentPage->formData->isEmpty()) {
            return;
        }

        $layout = $currentPage->getControllerTail();

        $currentPage->formData['layout'] = $layout;
        $currentPage->formData['content'] = $layoutContent->fetch($layout);

        if (empty($currentPage->formData['content'])) {
            $currentPage->flashData['notfound'] =  true;
        }
    }

    function page_validate_url($_, Page $currentPage, PageSite $pageSite, Request $request)
    {
        $url = $request->getHost() . $this->getViewUrl($currentPage, $pageSite);
        $url = urlencode($url);
        $result = "http://validator.w3.org/check?uri={$url}";
        return $result;
    }

    function page_view_url($_, Page $currentPage, PageSite $pageSite)
    {
        $result = $this->getViewUrl($currentPage, $pageSite);
        //$result = rawurlencode($result);
        return $result;
    }

    protected function getViewUrl(Page $currentPage, PageSite $pageSite)
    {
        $pageName = $currentPage->getControllerTail();
        return $pageSite->getViewUrl($pageName);
    }

    function backup_daily_url($_, BackupFile $backup)
    {
        return $backup->dailyUrl();
    }

    function backup_latest_url($_, BackupFile $backup)
    {
        return $backup->latestUrl();
    }
}

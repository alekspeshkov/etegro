<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:a="page"
    xmlns:h="helper"
    exclude-result-prefixes="a h"
>
<xsl:import href='stylesheet://page'/>

<xsl:decimal-format name="rub" decimal-separator='.' NaN='?' grouping-separator='&#160;'/>

<xsl:template match="a:disclaimer" name="disclaimer">
    <xsl:apply-templates select="h:content('path_content','disclaimer')/content"/>
</xsl:template>

<xsl:template match="img[starts-with(@src,'/img') and local-name(..) != 'a']">
    <xsl:if test="h:string('is_file',@src)='true'">
        <xsl:variable name="dirname">
            <xsl:call-template name="dirname"><xsl:with-param name="path" select="@src"/></xsl:call-template>
        </xsl:variable>
        <xsl:variable name="basename">
            <xsl:call-template name="basename"><xsl:with-param name="path" select="@src"/></xsl:call-template>
        </xsl:variable>
        <xsl:variable name="big" select="concat($dirname,'/big/',$basename)"/>
        <xsl:choose>
            <xsl:when test="h:string('is_file',$big)='true'">
                <xsl:variable name="big-src">
                    <xsl:call-template name="url"><xsl:with-param name="href" select="$big"/></xsl:call-template>
                </xsl:variable>
                <a href="{$big-src}" title="увеличить" class="_blank">
                <xsl:element name="{local-name()}">
                    <xsl:attribute name="alt"/>
                    <xsl:apply-templates select="@* | node()"/>
                </xsl:element>
                </a>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="{local-name()}">
                    <xsl:attribute name="alt"/>
                    <xsl:apply-templates select="@* | node()"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:if>
</xsl:template>

<xsl:template match="a:breadcrumbs">
    <xsl:variable name="ancestors" select="h:pages('path_ancestors_pages',$path)/pages/page"/>
    <ul>
        <xsl:for-each select="$ancestors">
            <li>
                <xsl:call-template name="html-a-href">
                    <xsl:with-param name="body"><xsl:apply-templates select="label/node()"/></xsl:with-param>
                </xsl:call-template>
            </li>
        </xsl:for-each>

        <xsl:if test="h:string('path_tail',$path) != ''">
            <xsl:variable name="suffixes" select="@*"/>
            <xsl:for-each select="h:value('product_meta',$path)/value">
                <xsl:if test="path != $ancestors[last()]/path">
                    <li>
                        <xsl:call-template name="html-a-href">
                            <xsl:with-param name="body" select="string(name)"/>
                        </xsl:call-template>
                    </li>
                </xsl:if>

                <xsl:variable name="suffix" select="suffix"/>
                <xsl:for-each select="$suffixes[local-name() = $suffix]">
                    <li>
                    <xsl:call-template name="html-a-href">
                        <xsl:with-param name="href" select="$path"/>
                        <xsl:with-param name="body" select="string(.)"/>
                    </xsl:call-template>
                    </li>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:if>
    </ul>
</xsl:template>

<xsl:template match="a:local-navigation[@type='news']">
    <xsl:call-template name="section-menu"/>
</xsl:template>

<xsl:template match="a:articles">
    <xsl:param name="pages" select="h:pages('page_children_pages',$path)/pages/page"/>
    <xsl:for-each select="$pages">
        <h2>
        <xsl:call-template name="html-a-href">
            <xsl:with-param name="body"><xsl:apply-templates select="title/text()"/></xsl:with-param>
        </xsl:call-template>
        </h2>
        <xsl:if test="description/text()">
            <p><xsl:value-of select="description/text()"/></p>
        </xsl:if>
    </xsl:for-each>
</xsl:template>

<xsl:template match="a:toc">
    <ul class="toc">
    <xsl:for-each select="//a[@name]">
        <li>
        <xsl:call-template name="html-a-href">
            <xsl:with-param name="href"><xsl:value-of select="concat('#',@name)"/></xsl:with-param>
            <xsl:with-param name="body"><xsl:apply-templates select="text()"/></xsl:with-param>
        </xsl:call-template>
        </li>
    </xsl:for-each>
    </ul>
</xsl:template>

<xsl:template match="a:archive-list">
    <xsl:variable name="support" select="string(@support)"/>
    <ul>
    <xsl:for-each select="h:pages('path_models_pages',$path)/pages/page">
        <li>
        <xsl:call-template name="html-a-href">
            <xsl:with-param name="href" select="string(path)"/>
            <xsl:with-param name="body" select="translate(label,' ','&#160;')"/>
        </xsl:call-template>
        <xsl:if test="$support">
            <xsl:text> </xsl:text>
            <xsl:call-template name="html-a-href">
                <xsl:with-param name="href" select="concat(path,'/support')"/>
                <xsl:with-param name="body" select="$support"/>
            </xsl:call-template>
        </xsl:if>
        </li>
    </xsl:for-each>
    </ul>
</xsl:template>

<xsl:template mode="head" match="a:model">
    <xsl:variable name="model" select="h:value('product_meta',$path)/value"/>
    <xsl:variable name="brand-name" select="concat('ETegro ', $model/name)"/>
    <xsl:choose>
        <xsl:when test="$model/suffix=''">
            <title><xsl:value-of select="concat(@desc, ' ', $brand-name)"/></title>
        </xsl:when>
        <xsl:when test="$model/suffix='spec' and @spec">
            <title><xsl:value-of select="concat(@spec, ' ', $brand-name)"/></title>
        </xsl:when>
        <xsl:when test="$model/suffix='buy' and @buy">
            <title><xsl:value-of select="concat(@buy, ' ', $brand-name)"/></title>
        </xsl:when>
        <xsl:when test="$model/suffix='support' and @support">
            <title><xsl:value-of select="concat(@support, ' ', $brand-name)"/></title>
            <meta name="robots" content="nofollow"/>
        </xsl:when>
        <xsl:otherwise><xsl:call-template name="not-found-head"/></xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="product-spec">
    <xsl:param name="alias"/>

    <xsl:variable name="spec" select="h:raw_value('product_spec',$alias)/value/element"/>
    <xsl:if test="$spec">
        <table class="spec">
        <xsl:for-each select="$spec">
            <tr>
                <th><xsl:apply-templates select="name/node()"/></th>
                <td><xsl:apply-templates select="content/node()"/></td>
            </tr>
        </xsl:for-each>
        </table>
        <xsl:call-template name="disclaimer"/>
    </xsl:if>
</xsl:template>

<xsl:template match="a:description">
    <xsl:variable name="alias">
        <xsl:choose>
            <xsl:when test="@model"><xsl:value-of select="@model"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="$name"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:call-template name="pictures"><xsl:with-param name="model" select="$alias"/></xsl:call-template>
    <div class="text">
        <xsl:apply-templates select="h:content('product_description_content',$alias)/content"/>
        <xsl:call-template name="disclaimer"/>
    </div>
</xsl:template>

<xsl:template match="a:description[node()]">
    <xsl:variable name="model">
        <xsl:choose>
            <xsl:when test="@img"><xsl:value-of select="@img"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="$name"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:call-template name="pictures"><xsl:with-param name="model" select="$model"/></xsl:call-template>
    <div class="text">
        <xsl:apply-templates/>
        <xsl:call-template name="disclaimer"/>
    </div>
</xsl:template>

<xsl:template match="a:action[@src and @title and @href]">
    <xsl:variable name="href">
        <xsl:call-template name="url"><xsl:with-param name="href" select="@href"/></xsl:call-template>
    </xsl:variable>
    <xsl:variable name="src">
        <xsl:call-template name="url"><xsl:with-param name="href" select="@src"/></xsl:call-template>
    </xsl:variable>
    <xsl:variable name="over">
        <xsl:call-template name="url"><xsl:with-param name="href" select="string('/img/design/1x1.gif')"/></xsl:call-template>
    </xsl:variable>
    <div class="action">
        <img src="{$src}" alt="{@title}"/>
        <div class="title"><xsl:value-of select="@title"/></div>
        <xsl:if test="@slogan">
            <div class="slogan"><xsl:value-of select="@slogan"/></div>
        </xsl:if>
        <xsl:if test="@price">
            <div class="price"><xsl:value-of select="@price"/></div>
        </xsl:if>
        <xsl:if test="@spec">
            <div class="spec"><xsl:value-of select="@spec"/></div>
        </xsl:if>
        <a href="{$href}"><img class="over" src="{$over}" alt=""/></a>
    </div>
</xsl:template>

</xsl:stylesheet>

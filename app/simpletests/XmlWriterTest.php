<?php declare(strict_types=1);

namespace test;

use UnitTestCase;
use Common\XmlWriter;

class XmlWriterTest extends UnitTestCase
{
    function testCore_Xml_Writer() {
        $xml = new XmlWriter;
        $this->assertIdentical( $xml->xml(null), $xml->xml() );
        $this->assertIdentical( $xml->xml(false), $xml->xml() );
        $this->assertIdentical(
            $xml->xml(['a:alpha xmlns:a="beta"'=>'']),
            $xml->xml('<a:alpha xmlns:a="beta"/>') );
        $this->assertIdentical(
            $xml->xml(['root'=>[ 'a'=>'aa','b'=>['b1'=>'','b2'=>'bb2'] ]]),
            $xml->xml("<root><a>aa</a><b><b1/><b2>bb2</b2></b></root>") );
        $this->assertIdentical(
            $xml->xmlText(['a'=>['a1'=>'<','a2'=>'','a3'=>'>'], 'b'=>'"\''],'root'),
            $xml->xml("<root><a><a1>&lt;</a1><a2/><a3>&gt;</a3></a><b>&quot;&#039;</b></root>") );
    }

    function test_esc() {
        $this->assertIdentical(
            (new XmlWriter)->esc('<&\'яблоко1">'),
            '&lt;&amp;&apos;яблоко1&quot;&gt;');
    }

}

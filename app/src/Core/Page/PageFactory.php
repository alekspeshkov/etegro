<?php declare(strict_types=1);

namespace Core\Page;

use Di\Attribute\Required;
use Di\Interface\ContainerInterface;
use Di\ReflectionHelper;

/** Service for creating and caching Page objects */
class PageFactory
{
    #[Required] protected ContainerInterface $container;
    #[Required] protected PageNaming $pageNaming;

    /** @var array<string, \Core\Page\Page> */
    protected array $pages = [];

    /** Get cached or newly created Page object
     * @template T
     * @param ?class-string<T> $className
     * @return T|\Core\Page\Page
     */
    function getPage(string $name, ?string $className = null)
    {
        $className ??= Page::class;
        $pageName = $this->pageNaming->normalize($name);

        if (!isset($this->pages[$pageName])) {
            return $this->pages[$pageName] = $this->container->newEntity($className, [$pageName]);
        }

        if (is_a($this->pages[$pageName], $className)) {
            return $this->pages[$pageName];
        }

        $newPage = $this->container->newEntity($className, [$pageName]);
        ReflectionHelper::initFromParent($newPage, $this->pages[$pageName]);

        return $this->pages[$pageName] = $newPage;
    }
}

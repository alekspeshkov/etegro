<?php declare(strict_types=1);

namespace Common;

abstract class Normalize
{
    static array $tags = ['pre', 'textarea'];

    static function normalizeHtml($html)
    {
        foreach (static::$tags as $tag) {
            $skip = static::substrBetween($html, "<{$tag}", "</{$tag}>");
            if (is_array($skip)) {
                [$starting, $size] = $skip;
                $before = substr($html, 0, $starting);
                $ignore = substr($html, $starting, $size);
                $after  = substr($html, $starting + $size);
                return static::stripBlanks($before) . $ignore . static::normalizeHtml($after);
            }
        }

        return static::stripBlanks($html);
    }

    /** seeks for the first substring bound between $opening and $closing substrings
     * @return [int $startingIndex, int $substringSize]|false
     */
    static function substrBetween(string $string, string $opening, string $closing): array|false
    {
        $starting = strpos($string, $opening);
        if (false !== $starting) {
            $ending = strpos($string, $closing, $starting);
            if (false !== $ending) {
                $size = $ending - $starting + strlen($closing);
                return [$starting, $size];
            }
        }
        return false;
    }

    static function stripBlanks($html)
    {
        return preg_replace(['/[\040\t]+/', '/[\s]*[\n]/'], [' ', "\n"], $html);
    }
}

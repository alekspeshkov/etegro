<?php declare(strict_types=1);

namespace Di;

use Di\Interface\AppConfigInterface;
use Di\Interface\FactoryInterface;
use Di\Interface\ContainerAwareInterface;
use Di\Interface\ContainerInterface;
use Di\Interface\ConfigRecordInterface;

class Container implements ContainerInterface
{
    /** @var array<string, mixed> */
    protected array $entities;

    function __construct(
        protected readonly FactoryInterface $factory,
        protected readonly AppConfigInterface $appConfig,
    ) {
        $args = func_get_args();
        foreach ($args as $object) {
            if ($object instanceof ContainerAwareInterface) {
                $object->setContainer($this);
            }
        }

        $this->entities = [
            ContainerInterface::class => $this,
            AppConfigInterface::class => $this->appConfig,
        ];
    }

    function newEntity(string $serviceName, #[\SensitiveParameter] array $params = [])
    {
        $record = $this->appConfig->resolve($serviceName, $params);
        return $record->newEntity($this->getEntityBuilder());
    }

    protected function getEntityBuilder()
    {
        return fn (#[\SensitiveParameter]...$args) => $this->factory->newInstance(...$args);
    }

    function invoke(#[\SensitiveParameter] object $instance, ?string $method = null, #[\SensitiveParameter] array $params = [])
    {
        return $this->factory->invoke($instance, $method, $params);
    }

    function has(string $serviceName): bool
    {
        $record = $this->appConfig->createRecord($serviceName);
        $record = $this->appConfig->resolveFrom($record);
        return isset($this->entities[$record->getName()]);
    }

    function get(string $serviceName)
    {
        $record = $this->appConfig->createRecord($serviceName);
        return $this->getOrSetFrom($record);
    }

    protected function getOrSetFrom(#[\SensitiveParameter] ConfigRecordInterface $record)
    {
        return $this->entities[$record->getName()] ??= $this->newEntityFrom($record);
    }

    protected function newEntityFrom(#[\SensitiveParameter] ConfigRecordInterface $record)
    {
        $isResolved = $this->appConfig->resolveOnce($record);
        return $isResolved ? $record->newEntity($this->getEntityBuilder()) : $this->getOrSetFrom($record);
    }
}

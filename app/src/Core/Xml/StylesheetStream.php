<?php declare(strict_types=1);

namespace Core\Xml;

use Core\Xml\View\Helper;

class StylesheetStream extends \Common\StringStream
{
    public static ?Helper $staticHelper = null;

    function getContent($name)
    {
        return static::$staticHelper->getStylesheet($name);
    }
}

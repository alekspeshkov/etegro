<?php declare(strict_types=1);

namespace Common\Db;

class NotFoundException extends \UnderflowException
{
}

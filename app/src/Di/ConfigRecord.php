<?php declare(strict_types=1);

namespace Di;

use Di\Interface\ConfigRecordInterface;

/** immutable class */
readonly class ConfigRecord implements ConfigRecordInterface
{
    protected function __construct(
        protected ?string $serviceName = null,
        #[\SensitiveParameter] protected array $params = [],
    ) {
    }

    static function create(?string $className = null, #[\SensitiveParameter] array $params = []): static
    {
        if (isset($params['::class'])) {
            $className = $params['::class'];
            unset($params['::class']);
        }
        return new static($className, $params);
    }

    function getName(): string
    {
        return $this->serviceName;
    }

    function isNameNull(): bool
    {
        return is_null($this->serviceName);
    }

    function toArray(): array
    {
        return [$this->serviceName, $this->params];
    }

    /** Update this ConfigRecordInterface with parameter data */
    function extendFrom(#[\SensitiveParameter] ?ConfigRecordInterface $record): static
    {
        return new static($record?->serviceName ?? $this->serviceName, $this->params + ($record?->params ?? []));
    }

    function extendParams(#[\SensitiveParameter] ?ConfigRecordInterface $record): static
    {
        return new static($this->serviceName, $this->params + ($record?->params ?? []));
    }

    function extendWith(?string $serviceName, #[\SensitiveParameter] array $params = []): static
    {
        return $this->extendFrom(static::create($serviceName, $params));
    }

    /** @return object|mixed usually object */
    function newEntity(callable $factory)
    {
        $params = $this->params;

        if (isset($params['::entity'])) {
            // direct factory function
            $entityBuilder = $params['::entity'];
            unset($params['::entity']);
            return $entityBuilder($factory, $params);
        }

        if (isset($params['::config'])) {
            // lazy lazy config
            $configBuilder = $params['::config'];
            unset($params['::config']);
            $config = $configBuilder($factory, $params);
            $record = static::create(null, $config);
            $record = $record->extendWith($this->serviceName, $params);
            return $record->newEntity($factory);
        }

        $className = $params['::class'] ?? $this->serviceName;

        return $factory($className, $params);
    }
}

<?php declare(strict_types=1);

namespace Core\Xml;

use Core\Xml\StylesheetStream;
use Core\Xml\View\Helper;

class Xslt
{
    function __construct(
        protected Helper $viewHelper
    ) {
    }

    function transformToXML(\DOMDocument $xml, string $xslName): string
    {
        $previousHelper = StylesheetStream::$staticHelper;
        StylesheetStream::$staticHelper = $this->viewHelper;

        try {
            if (!$previousHelper) {
                stream_wrapper_register('stylesheet', StylesheetStream::class);
            }

            $proc = new \XSLTProcessor;
            $proc->setParameter('', 'callbackFunction', 'callbackWrapper');
            $proc->registerPHPFunctions(['callbackWrapper' => [$this->viewHelper, 'callbackWrapper']]);

            $xslString = $this->viewHelper->getStylesheet($xslName);
            $xsl = new \DOMDocument;
            $xsl->loadXML($xslString);
            $proc->importStyleSheet($xsl);

            return $proc->transformToXML($xml);
        }
        finally {
            StylesheetStream::$staticHelper = $previousHelper;
        }

    }
}

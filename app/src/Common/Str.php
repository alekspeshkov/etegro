<?php declare(strict_types=1);

namespace Common;

abstract class Str
{
    /** @param string[] $strings */
    static function filterEmpty(array $strings)
    {
        return array_filter($strings, fn (string $str) => strlen($str) > 0);
    }

    /** @param string[] $array */
    static function concat(array $strings, string $x): string
    {
        $strings = static::filterEmpty($strings);

        switch (count($strings)) {
            case 0:
                return '';
            case 1:
                return reset($strings);
            default:
                $middle = array_slice($strings, 1, -1);

                $middle = static::filterEmpty( array_map(fn (string $str) => trim($str, $x), $middle) );

                $result = rtrim(reset($strings), $x) . $x;

                if ($middle) {
                    $result .= implode($x, $middle) . $x;
                }

                $result .= ltrim(end($strings), $x);

                return $result;
        }
    }

    /** @param string[] $array */
    static function concatUri(array $array): string
    {
        return static::concat($array, '/');
    }

    /** @param string[] $array */
    static function concatDir(array $array): string
    {
        return static::concat($array, DIRECTORY_SEPARATOR);
    }

    static function toPascalCase(string $token): string
    {
        $token = strtr($token, ['_' => ' ', '-' => ' ', '.' => ' ', '\\' => ' ', '/' => ' ']);
        $token = ucwords(strtolower($token));
        $token = strtr($token, [' ' => '']);
        return $token;
    }

    /** Convert data to string (recursively if array). Trim leading and trailing spaces.
     *
     * @param bool $removeEmptyString convert empty strings to null
     * @return string|string[]|null
     */
    static function normalize($data, bool $removeEmptyString = false): string|array|null
    {
        if (is_array($data)) {
            $array = [];
            foreach ($data as $key => $value) {
                $normalValue = static::normalize($value, $removeEmptyString);
                if (!is_null($normalValue)) {
                    $array[$key] = $normalValue;
                }
            }
            if (empty($array)) {
                return null;
            }
            return $array;
        }

        if (is_scalar($data)) {
            $result = trim("{$data}");
            if ($removeEmptyString && strlen($result) == 0) {
                return null;
            }
            return $result;
        }

        assert(is_null($data));
        return null;
    }
}

<?php declare(strict_types=1);

namespace Di\Attribute;

/** property with Required attribute should be initialized before __construct() call */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
class Required
{
}

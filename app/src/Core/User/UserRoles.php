<?php declare(strict_types=1);

namespace Core\User;

use Common\Db\Table;

class UserRoles
{
    static string $tableName = 'user_role';
    protected readonly Table $theTable;

    function __construct(Table $table)
    {
        $this->theTable = $table->from(static::$tableName);
    }

    /** @return string[] */
    function getUserRoles(string $userName): array
    {
        if (empty($userName)) {
            return [];
        }

        return $this->theTable
            ->where(['user' => $userName])
            ->fetchCol('role');
    }
}

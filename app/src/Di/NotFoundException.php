<?php declare(strict_types=1);

namespace Di;

class NotFoundException extends \UnexpectedValueException
{
}

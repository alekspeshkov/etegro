<?php declare(strict_types=1);

namespace Common\Db;

abstract class Transaction
{
    /** @param string[] $sqls */
    static function executeTransaction(\PDO $db, array $sqls): int
    {
        $autocommit = $db->getAttribute(\PDO::ATTR_AUTOCOMMIT);

        // execute an array of SQL statements
        try {
            $db->setAttribute(\PDO::ATTR_AUTOCOMMIT, 0);
            $db->beginTransaction();

            $affectedRowsCount = 0;
            foreach ($sqls as $sql) {
                $affectedRowsCount += $db->exec($sql);
            }

            $db->commit();
        } catch (\Throwable $e) {
            $db->rollback();
            throw $e;
        } finally {
            $db->setAttribute(\PDO::ATTR_AUTOCOMMIT, $autocommit);
        }

        return $affectedRowsCount;
    }

    /** Execute SQL statements delimited with ';' */
    static function executeFile(\PDO $db, string $dump): int
    {
        $sqls = preg_split('/;\s*$/m', $dump, -1, PREG_SPLIT_NO_EMPTY);
        return static::executeTransaction($db, $sqls);
    }
}

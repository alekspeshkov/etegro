<?php declare(strict_types=1);

namespace Core\User;

use Common\Db\Table;

class UserAuth
{
    static string $tableName = 'user_auth';
    protected readonly Table $theTable;

    function __construct(Table $table)
    {
        $this->theTable = $table->from(static::$tableName);
    }

    function isValid(string $userName, #[\SensitiveParameter] string $password)
    {
        return $this->theTable
            ->where(['user' => $userName])
            ->where(['password' => $password], "IF(?='',~='',~=MD5(?))")
            ->exists();
    }
}

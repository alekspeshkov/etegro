<?php declare(strict_types=1);

namespace Core\Page;

use Di\Attribute\Lazy;
use Di\Attribute\Required;
use Di\GetLazyConfigurableProperty;

class Page
{
    use GetLazyConfigurableProperty;
    use GetPage;

    #[Required] protected PageNaming $pageNaming;
    #[Required] protected Table $table;

    // configurable per object
    #[Lazy] protected Table $pageContent;
    #[Lazy] protected Table $pageTitle;
    #[Lazy] public SessionPageData $formData;
    #[Lazy] public SessionPageData $flashData;

    /** @param string|null $name normalized page name "site:path" | current page deduced the HTTP request
     */
    protected function __construct(string $name)
    {
        $this->pageName = $name;

        $this->lazyConfig->configure([
            'pageContent' => ['::entity' => fn () => $this->table->from('page_content')],
            'pageTitle' => ['::entity' => fn () => $this->table->from('page_title')],
            'formData' => ['::config' => fn () => ['namespace' => 'form', 'path' => $this->getPathName()]],
            'flashData' => ['::config' => fn () => ['namespace' => 'flash', 'path' => $this->getPathName()]],
        ]);
    }

    private readonly string $pageName;

    function getPageName(): string
    {
        return $this->pageName;
    }

    private readonly string $pathName;

    function getPathName()
    {
        return $this->pathName ??= $this->pageNaming->pagePath($this->pageName);
    }

    private readonly string $content;

    function fetchContent(): string
    {
        return $this->content ??= $this->pageContent->wherePage($this)->fetchOneOrNull('content') ?? '';
    }

    function exists()
    {
        return $this->fetchContent() !== '';
    }

    private readonly array $children;

    function fetchChildren()
    {
        return $this->children ??= (function () {
            $children = $this->pageTitle->naturalLeftJoin('page_menu_order')
                ->whereParent($this)
                ->orderFunc('IFNULL(~, 100)', ['rank'])->order('label')->orderDesc('page')
                ->fetchAll();
            return $this->pageNaming->injectPath($children);
        })();
    }

    function hasChildren()
    {
        return $this->pageContent->whereParent($this)->exists();
    }

    private readonly array $ancestorAndSelf;

    /** @return string[] */
    protected function getAncestorsAndSelf(): array
    {
        return $this->ancestorAndSelf ??= $this->pageNaming->ancestorsAndSelf($this->getPageName());
    }

    function getParentPage()
    {
        $pages = $this->getAncestorsAndSelf();
        array_shift($pages);

        $pageName = $this->pageContent->where(['page' => $pages])->order(['page' => $pages])->fetchOne('page');
        return $this->getPage($pageName);
    }

    /** breadcrumb navigation generation */
    function fetchAncestors()
    {
        $pages = $this->getAncestorsAndSelf();
        $ancestors = $this->pageTitle->where(['page' => $pages])->orderDesc(['page' => $pages])->fetchAll();
        return $this->pageNaming->injectPath($ancestors);
    }

    function fetchTail()
    {
        $pages = $this->getAncestorsAndSelf();
        $parent = $this->pageTitle->where(['page' => $pages])->order(['page' => $pages])->fetchOne('page');
        $tail = substr($this->getPageName(), strlen($parent) + 1);
        return $tail;
    }

}

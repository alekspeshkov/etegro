<?php declare(strict_types=1);

namespace Core\Xml\View;

use Common\Str;
use Common\XmlWriter;
use Di\Attribute\Required;
use Di\Interface\ContainerInterface;

abstract class Helper
{
    #[Required] protected ContainerInterface $container;

    private array $searchDirs;

    function getStylesheet(string $xslName): string
    {
        $baseName = "{$xslName}.xsl";

        $this->searchDirs ??= FileHelper::getDirNames(get_class($this));
        foreach ($this->searchDirs as $dirName) {
            $fileName = Str::concatDir([$dirName, $baseName]);
            if (is_readable($fileName)) {
                $content = file_get_contents($fileName);
                if ($content) {
                    return $content;
                }
            }
        }

        throw new NotFoundException("'{$baseName}' file not found", 404);
    }

    function callbackWrapper(string $decoratorMethod, string $method, ...$args)
    {
        $result = $this->container->invoke($this, $method, $args);
        $result = $this->container->invoke($this, $decoratorMethod, [$result]);
        return $result;
    }

    function _str($result)
    {
        if (is_string($result)) {
            return $result;
        }

        if (is_numeric($result)) {
            return "{$result}";
        }

        if ($result === true) {
            return 'true';
        }

        if (is_null($result) || $result === false) {
            return '';
        }

        $type = gettype($result);
        throw new \InvalidArgumentException("Illegal type '{$type}' expecting string");
    }

    function _nodeset($result, XmlWriter $xml)
    {
        /** @var $result DomNode|null */
        return $result ?? $xml->xml();
    }

    function _raw_value($result, XmlWriter $xml)
    {
        return $xml->xml($result, 'value', 'element');
    }

    function _value($result, XmlWriter $xml)
    {
        return $xml->xmlText($result ?? '', 'value', 'element');
    }
}

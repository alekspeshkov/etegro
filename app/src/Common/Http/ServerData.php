<?php declare(strict_types=1);

namespace Common\Http;

class ServerData extends \Common\ArraySymlink
{
    function __construct()
    {
        parent::__construct('$_SERVER');
    }
}

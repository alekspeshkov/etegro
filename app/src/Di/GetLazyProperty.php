<?php declare(strict_types=1);

namespace Di;

use Di\Attribute\Required;
use Di\Interface\ContainerInterface;
use Di\ReflectionHelper;

/** Lazy property is object property that will be injected from the factory on the first property read */
trait GetLazyProperty
{
    /** application dependency injector */
    #[Required] protected ContainerInterface $container;

    function &__get(string $propertyName): object
    {
        $classOrInterfaceName = ReflectionHelper::getPropertyClassName($this, $propertyName);
        $this->{$propertyName} = $this->container->get($classOrInterfaceName);
        return $this->{$propertyName};
    }
}

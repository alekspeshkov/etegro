<?php declare(strict_types=1);

namespace Core;

interface LoggerInterface extends \Psr\Log\LoggerInterface
{
}

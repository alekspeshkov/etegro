<?php declare(strict_types=1);

namespace Site\Admin\Action;

class PageContentOverwrite extends PageContentUpdate
{
    function __invoke()
    {
        $this->savePostData();
        $this->validateCurrentPage();

        $targetPage = $this->currentPage->formData['page'];
        $locationPage = $this->currentPage->getControllerTail();

        if ($locationPage !== $targetPage) {
            $this->currentPage->flashData['overwrite'] = true;
        }

        if (!$this->getPage($targetPage)->exists()) {
            $this->currentPage->flashData['notfound'] = true;
        }

        return $this->currentPage->flashData->isEmpty() && $this->overwrite();
    }

    function overwrite()
    {
        try {
            $data = $this->currentPage->formData->getAll();
            $this->currentPage->save($data);

            $adminTargetPage = $this->currentPage->setRedirectAdminPage($data['page']);
            $adminTargetPage->flashData['updated'] = true;

            $this->currentPage->formData->clear();
        } catch (\Throwable $e) {
            $this->currentPage->flashData['database'] = $e->getMessage();
            return false;
        }
    }
}

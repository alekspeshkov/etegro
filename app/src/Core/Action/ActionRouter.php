<?php declare(strict_types=1);

namespace Core\Action;

use Common\Http\PostData;
use Common\Http\Redirect;
use Common\Http\Request;
use Common\Http\Response;
use Common\Str;
use Core\LoggerInterface;
use Di\Attribute\Required;

class ActionRouter implements ActionInterface
{
    use InvokeAction;

    #[Required] protected string $actionField;
    #[Required] protected string $phpActionNamespace;

    /** @var array<string, string> $actions */
    protected array $actions;

    #[Required] protected Request $request;
    #[Required] protected Response $response;
    #[Required] protected PostData $postData;
    #[Required] protected Redirect $redirect;
    #[Required] protected LoggerInterface $logger;

    function __invoke()
    {
        try {
            if (!$this->request->isValidOrigin()) {
                $this->response->setStatus(400);
                $this->logger->error('POST request origin validation failed');
                return false;
            }

            $this->actions = (array)$this->postData[$this->actionField];

            if ($this->resolveActions() === false) {
                if ($this->response->isStatus(200)) {
                    $this->response->setStatus(400);
                }
                return false;
            }

            $this->redirect->setRedirect();
            return true;
        } catch (\Throwable $e) {
            $this->response->setStatus(500);
            $this->response->flushHeaders();
            $this->logger->critical($e);
            throw $e;
        }
    }

    protected function resolveActions(): bool
    {
        foreach ($this->actions as $actionName => $actionValue) {
            $this->resolveAction($actionName, $actionValue);
        }

        return true;
    }

    protected function resolveAction(string $actionName, string $actionValue)
    {
        $actionClassName = $this->actionToClass($actionName);
        return $this->invokeAction($actionClassName, [$this->actionField => $actionValue]);
    }

    protected function actionToClass(string $actionName)
    {
        return Str::concat([$this->phpActionNamespace, Str::toPascalCase($actionName)], '\\');
    }
}

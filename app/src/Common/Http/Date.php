<?php declare(strict_types=1);

namespace Common\Http;

class Date
{
    protected int $timestamp;

    function __construct(?Date $date = null)
    {
        $this->timestamp = $date?->timestamp ?? time();
    }

    static function createFromTimestamp(int $timestamp): Date
    {
        $instance = new static();
        $instance->timestamp = $timestamp;
        return $instance;
    }

    static function createFromHeader(string $gmdate): Date
    {
        $date = date_create_from_format('D, d M Y H:i:s T+', $gmdate, timezone_open('GMT'));

        if ($date === false) {
            $message = implode(';', date_get_last_errors()['errors']);
            throw new \UnexpectedValueException($message);
        }

        return static::createFromTimestamp(date_timestamp_get($date));
    }

    function __toString()
    {
        return gmdate('D, d M Y H:i:s', $this->timestamp) . ' GMT';
    }

    function getTimestamp(): int
    {
        return $this->timestamp;
    }
}

<?php declare(strict_types=1);

namespace Common\Http;

class PostData extends \Common\ArraySymlink
{
    function __construct()
    {
        parent::__construct('$_POST');
    }
}

function include(uri) {
    var script  = document.createElement('script');
    script.type = 'text/javascript';
    script.src  = uri;
    document.getElementsByTagName('head')[0].appendChild(script);
};

function _blank() {
    if (document.getElementsByClassName) {
        links = document.getElementsByClassName('_blank');
        for (i=0; i<links.length; i++) {
            links[i].target = '_blank';
        }
    }
};
document.addEventListener("DOMContentLoaded", _blank);

<?php declare(strict_types=1);

namespace Site\Ru\View;

use Common\Db\Connection as Db;
use Common\Db\Table;
use Common\Http\Request;

class Configurator
{
    static string $tableName = 'creator_computermodel';
    protected readonly Table $theTable;

    function __construct(
        protected Request $request,
        protected Db $db,
        Table $table
    ) {
        $this->theTable = $table->from(static::$tableName);
    }

    function productMeta($path, $url = 'url')
    {
        $result = $this->db->fetchRowOrNull(
            'SELECT ~ AS path, name, alias AS model, '
                . "IF(SUBSTR(?, LENGTH(~)+1, 1) = '/', SUBSTR(?, LENGTH(~)+2), '') AS suffix "
                . 'FROM creator_computermodel '
                . 'WHERE (~ = ?) '
                . "OR (LENGTH(?) > LENGTH(~)+1 AND ~ = LEFT(?, LENGTH(~)) AND SUBSTR(?, LENGTH(~)+1, 1) = '/') "
                . 'LIMIT 1',
            [
                $url, $path, $url, $path, $url, $url, $path, $path, $url, $url, $path, $url, $path, $url
            ]
        );
        return $result;
    }

    protected function productContent($model, $column)
    {
        $result = $this->theTable->where(['alias' => $model])->fetchOne($column);
        return $result;
    }

    function productSupport($model)
    {
        return $this->productContent($model, 'support');
    }

    function productSpec($alias)
    {
        $model = $this->theTable->where(['alias' => $alias])->fetchRowOrNull('id', 'name', 'default_price');

        if (is_null($model)) {
            return [];
        }

        $header = [];
        if (!empty($model['name'])) {
            $header[] = ['name' => 'Модель', 'content' => $model['name']];
            if (!empty($result['default_price'])) {
                $header[] = ['name' => 'Цена базовой конфигурации, руб.', 'content' => $model['default_price']];
            }
        }

        $spec = $this->getSpecFromId($model['id'], 'name', 'svalue_html');
        $result = array_merge($header, $spec);
        return $result;
    }

    function pathModels($path, $url = 'url')
    {
        $models = $this->db->fetchAll(
            'SELECT id, alias, default_price, name AS label, ~ AS path FROM creator_computermodel '
                . 'WHERE LEFT(~, LENGTH(?)) = ? '
                . 'ORDER BY name',
            [$url, $url, $path, $path]
        );
        return $models;
    }

    function specSummary($path)
    {
        $models = $this->pathModels($path);

        $summary = [];
        $count = 0;
        foreach ($models as $model) {
            $spec = $this->categorySummaryData($model);

            foreach ($spec as $row) {
                $id = $row['id'];
                $summary[$id]['name'] = $row['name'];

                // fill previous models
                for ($i = 0; $i < $count; $i++) {
                    if (!array_key_exists($i, $summary[$id])) {
                        $summary[$id][$i] = '';
                    }
                }

                $summary[$id][$count] = $row['content'];
            }

            $count += 1;
        }

        // fill the holes
        foreach ($summary as &$row) {
            for ($i = 0; $i < $count; $i++) {
                if (!array_key_exists($i, $row)) {
                    $row[$i] = '';
                }
            }
        }

        return $summary;
    }

    function categorySummaryData($model)
    {
        $header[] = ['id' => -5, 'name' => 'path', 'content' => $model['path']];
        $header[] = ['id' => -4, 'name' => 'alias', 'content' => $model['alias']];
        $header[] = ['id' => -3, 'name' => 'label', 'content' => $model['label']];
        $header[] = ['id' => -2, 'name' => 'default_price', 'content' => empty($model['default_price']) ? '' : $model['default_price']];

        $spec = $this->getSpecFromId($model['id'], 'name', 'svalue_html', true);
        $spec = array_merge($header, $spec);
        return $spec;
    }

    function getSpecFromId($id, $name = 'name', $content = 'svalue_html', $brief = false)
    {
        $spec = $this->db->fetchAll(
            'SELECT creator_specificationkey.id, creator_specificationkey.~ AS name, creator_specification.~ AS content '
                . 'FROM creator_specification '
                . 'JOIN creator_specificationkey ON creator_specificationkey.id=creator_specification.skey_id '
                . 'JOIN creator_computermodel ON creator_specification.computermodel_id=creator_computermodel.id '
                . 'WHERE creator_computermodel.id=? '
                . ($brief ? 'AND is_summary=1 ' : '')
                . 'ORDER BY creator_specificationkey.order',
            [$name, $content, $id]
        );
        return $spec;
    }

    function modelPrice($model)
    {
        $model = $this->theTable->where(['alias' => $model])->fetchRow('url', 'default_price');
        return $model;
    }

    function modelPictures($model)
    {
        $stripLeadingDir = function ($file, $dir) {
            if (strpos($file, $dir) === 0) {
                $file = substr($file, strlen($dir));
                $file = ltrim($file, DIRECTORY_SEPARATOR);
            }
            return $file;
        };

        $filePath = $this->request->webPath('img', substr($model, 0, 2), substr($model, 2));
        $list = glob("{$filePath}/{*.*}") ?: [];

        $webRoot = $this->request->webPath();
        foreach ($list as &$pic) {
            $pic = $stripLeadingDir($pic, $webRoot);
            $pic = strtr($pic, DIRECTORY_SEPARATOR, '/');
        }
        sort($list);
        return $list;
    }

}

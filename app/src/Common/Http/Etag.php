<?php declare(strict_types=1);

namespace Common\Http;

use Common\Str;

class Etag
{
    private array $requestEtag;
    private array $sessionEtag;

    function __construct(
        protected ServerData $serverData,
        protected Session $session,
        protected Response $response
    ) {
    }

    function ifMatch()
    {
        $requestEtag = $this->getRequestEtag();
        if (empty($requestEtag)) {
            return false;
        }

        $sessionEtag = $this->getSessionEtag();
        if (empty($sessionEtag)) {
            return false;
        }

        if ($sessionEtag == array_slice($requestEtag, 0, count($sessionEtag))) {
            $this->response->setStatus(304);
            return true;
        }

        return false;
    }

    function ifContentMatch(string $content)
    {
        if (!$this->response->isStatus(200)) {
            return false;
        }

        $requestEtag = $this->getRequestEtag();
        $sessionEtag = $this->getSessionEtag();
        $contentEtag = $this->getContentEtag($content);
        $responseEtag = array_merge($sessionEtag, $contentEtag);

        if ($contentEtag == array_slice($requestEtag, -count($contentEtag))) {
            if ($requestEtag != $responseEtag) {
                $this->setEtagHeader($responseEtag);
            }
            $this->response->setStatus(304);
            return true;
        }

        $md5 = $contentEtag[array_search('md5', $contentEtag) + 1];

        $this->response->setHeader('Content-MD5', $md5);
        $this->setEtagHeader($responseEtag);
        return false;
    }

    /** @param string[] $etag */
    protected function setEtagHeader(array $etag)
    {
        $this->response->setHeader('ETag', '"' . Str::concat($etag, '-') . '"');
    }

    /** @return string[] */
    protected function getRequestEtag(): array
    {
        return $this->requestEtag ??= (function () {
            $etag = $this->serverData['HTTP_IF_NONE_MATCH'] ?? '';
            $etag = trim($etag, '"');
            $etag = explode('-', $etag);
            return $etag;
        })();
    }

    /** @return string[] */
    protected function getSessionEtag(): array
    {
        return $this->sessionEtag ??= (function () {
            $etag = [];

            $sessionId = $this->session->getSessionId();
            if ($sessionId) {
                $age = $this->session->getAge();
                $etag = ['session', $sessionId, 'age', $age];
            }

            return $etag;
        })();
    }

    /** @return string[] */
    protected function getContentEtag(string $content): array
    {
        $size = strlen($content);
        $md5 = base64_encode(md5($content, true));
        return ['size', (string)$size, 'md5', $md5];
    }
}

<?php declare(strict_types=1);

namespace Di\Attribute;

/** property with Lazy attribute will be initialized just before first read try */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
class Lazy
{
}

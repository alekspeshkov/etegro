<?php declare(strict_types=1);

namespace Site\Admin;

use Common\EnvData;
use Common\Str;
use Di\Attribute\Required;
use Site\Environment;

class App extends \Core\Page\App
{
    #[Required] protected Environment $environment;

    #[Required] protected EnvData $env;

    function __construct()
    {
        parent::__construct();

        $this->appConfig
            ->configure($this->environment->getConfig('admin'))
            ->configure([
                BackupFile::class => [
                    'latestFile' => 'latest.sql',
                    'baseUrl' => '/admin/backup',
                    'storageDir' => Str::concatDir([$this->env['APP_VAR'], 'backup']),
                ],
                \Core\Action\ActionRouter::class => [
                    '::class' => ActionRouter::class,
                    'phpActionNamespace' => __NAMESPACE__ . '\Action',
                    'loginAction' => 'login',
                ],
                \Core\Xml\View\Helper::class => View\Helper::class,
            ]);
    }
}

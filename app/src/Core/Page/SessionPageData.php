<?php declare(strict_types=1);

namespace Core\Page;

use Common\Http\Session;
use Common\ArrayAccessExt;
use Di\Attribute\Required;

class SessionPageData implements \ArrayAccess
{
    use ArrayAccessExt;

    protected array $offsetBase;

    #[Required] protected Session $session;

    function __construct($path, $namespace)
    {
        $location = explode('/', "{$path}/#");
        $this->offsetBase = array_merge([$namespace], $location);
    }

    function offsetGet($offset): mixed
    {
        $offset = $this->getPageOffset($offset);
        return $this->session->offsetGet($offset);
    }

    function offsetSet($offset, $value): void
    {
        $offset = $this->getPageOffset($offset);
        $this->session->offsetSet($offset, $value);
    }

    function merge(array $data): void
    {
        $this->setAll($data + $this->getAll());
    }

    protected function getPageOffset($offset)
    {
        return array_merge($this->offsetBase, (array)$offset);
    }

    /** Maps HTML form field names to Form Access data array */
    function getHtmlFormField($name)
    {
        $offset = null;
        if (!empty($name)) {
            foreach (explode('[', $name) as $value) {
                $offset[] = rtrim($value, ']');
            }
        }
        return $this->offsetGet($offset);
    }
}

<?php declare(strict_types=1);

namespace Site\Admin\Action;

use Common\Http\PostData;
use Core\Action\ActionInterface;
use Core\LoggerInterface;
use Core\Page\GetPage;
use Di\Attribute\Required;
use Site\Admin\AdminPage;

abstract class Action implements ActionInterface
{
    use GetPage;

    #[Required] protected AdminPage $currentPage;
    #[Required] protected PostData $postData;
    #[Required] protected LoggerInterface $logger;

    function savePostData()
    {
        $postData = $this->postData->getAll();
        $this->currentPage->formData->setAll($postData);
    }
}

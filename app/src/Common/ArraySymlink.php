<?php declare(strict_types=1);

namespace Common;

class ArraySymlink implements \ArrayAccess
{
    use ArrayAccessExt;

    /** @var string $name Global array-variable symbolic name (starts with $) */

    function __construct(protected readonly string $name)
    {
        assert (eval("return isset($name);") || true); // check $name syntax
    }

    /** @param string|string[]|null $offset */
    function offsetGet($offset): mixed
    {
        $arrayVar = $this->arrayVar($offset);
        return eval("return {$arrayVar} ?? null;");
    }

    /** @param string|string[]|null $offset */
    function offsetSet($offset, $value): void
    {
        $this->set($offset, $value);
    }

    /** @param string|string[]|null $offset */
    protected function set($offset, $value): bool
    {
        $arrayVar = $this->arrayVar($offset);

        if (!is_null($value)) {
            if (eval("return isset({$arrayVar}) && ({$arrayVar} === \$value);")) {
                return false; // not modified
            }
            eval("{$arrayVar} = \$value;");
            return true;
        }

        // unset:

        if (eval("return !isset({$arrayVar});")) {
            return false; // not modified
        }

        if (!is_null($offset)) {
            // clean up the parent if removing its last child
            $array = (array)$offset;
            while (!is_null(array_pop($array))) {
                $var = $this->arrayVar($array);
                if (eval("return count({$var}) > 1;")) {
                    break;
                }
                $arrayVar = $var;
            }
        }

        eval("unset({$arrayVar});");
        return true;
    }

    /** @param string|string[]|null $offset */
    protected function arrayVar($offset = null): string
    {
        $result = $this->name;

        /** @var string $key */
        foreach ((array)$offset as $key) {
            $index = addcslashes((string)$key, "'\\");
            $result .= "['{$index}']";
        }

        return $result;
    }
}

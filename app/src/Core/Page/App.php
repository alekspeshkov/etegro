<?php declare(strict_types=1);

namespace Core\Page;

class App extends \Core\Action\App
{
    protected function __construct()
    {
        parent::__construct();

        $this->appConfig->configure([
            PageNaming::class => ['::config' => fn () => ['currentPath' => $this->request->getPath()]],
            \Common\Db\Table::class => Table::class,
            \Common\XmlWriter::class => XmlWriter::class,
        ]);
    }

    protected function routeMethodGet()
    {
        return $this->invokeAction(ContentRouter::class);
    }
}

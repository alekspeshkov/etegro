<?php declare(strict_types=1);

namespace Di;

use Di\Attribute\Lazy;
use Di\Attribute\Required;
use Di\Interface\FactoryInterface;
use Di\ReflectionHelper;

class ReflectionFactory implements FactoryInterface
{
    function newInstance(string $className, #[\SensitiveParameter] array $params = []): object
    {
        $class = new \ReflectionClass($className);

        if (ReflectionHelper::isInternalClass($class)) {
            // cannot make instance of internal class lazy
            return $this->newInstanceNow($className, $params);
        }

        $initializer = function (#[\SensitiveParameter] object $instance) use ($params) {
            $this->initObject($instance, $params);
        };

        return $class->newLazyGhost($initializer, \ReflectionClass::SKIP_INITIALIZATION_ON_SERIALIZE);
    }

    /** used for checking against infinite factory loop
     * @var array<string, object>
     */
    private array $instancesDependency = [];

    function newInstanceNow(string $className, #[\SensitiveParameter] array $params = []): object
    {
        if (isset($this->instancesDependency[$className])) {
            // break circular initalization loop
            return $this->instancesDependency[$className];
        }

        $class = new \ReflectionClass($className);

        $instance = $class->newInstanceWithoutConstructor();

        $this->instancesDependency[$className] = $instance;
        $instance = $this->initObject($instance, $params);
        array_pop($this->instancesDependency);

        return $instance;
    }

    function initObject(#[\SensitiveParameter] object $instance, #[\SensitiveParameter] array $params = []): object
    {
        $class = new \ReflectionClass($instance);
        $constructor = $class->getConstructor();

        if ($constructor) {
            $constructorArgs = $this->prepareArgs($constructor->getParameters(), $params);
            // $params are cleared off $constructorArgs
        }

        // inject properties before invoking the constructor
        $this->injectProps($instance, $class->getProperties(), $params);

        if ($constructor) {
            $constructor->invokeArgs($instance, $constructorArgs);
        }

        return $instance;
    }

    function invoke(#[\SensitiveParameter] object $instance, ?string $methodName = null, #[\SensitiveParameter] array $params = [])
    {
        $class = new \ReflectionObject($instance);
        $method = $class->getMethod($methodName ?? '__invoke');
        $args = $this->prepareArgs($method->getParameters(), $params);
        return $method->invokeArgs($instance, $args);
    }

    protected function getInstance(string $className): object
    {
        return $this->newInstance($className);
    }

    protected function getTypeInstanceOrNull(?\ReflectionType $type): ?object
    {
        $className = ReflectionHelper::getTypeClassNameOrNull($type);
        return is_null($className) ? null : $this->getInstance($className);
    }

    /**
     * @param \ReflectionParameter[] $parameters
     * @param array<int|string, mixed> $params
     */
    protected function prepareArgs(array $parameters, #[\SensitiveParameter] array &$params): array
    {
        $args = [];
        foreach ($parameters as $i => $parameter) {
            $name = $parameter->getName();

            if (array_key_exists($i, $params)) {
                $args[$name] = $params[$i];
                continue;
            }

            if (array_key_exists($name, $params)) {
                $args[$name] = $params[$name];
                unset($params[$name]);
                continue;
            }

            if ($parameter->isDefaultValueAvailable()) {
                $args[$name] = $parameter->getDefaultValue();
                continue;
            }

            if ($parameter->allowsNull()) {
                $args[$name] = null;
                continue;
            }

            $instance = $this->getTypeInstanceOrNull($parameter->getType());
            if (!is_null($instance)) {
                $args[$name] = $instance;
                continue;
            }
        }

        return $args;
    }

    /**
     * @param \ReflectionProperty[] $properties
     * @param array<string, mixed> $params
     */
    protected function injectProps(#[\SensitiveParameter] object $instance, array $properties, array $params = [])
    {
        foreach ($properties as $property) {
            $propertyName = $property->getName();

            if (array_key_exists($propertyName, $params)) {
                $property->setValue($instance, $params[$propertyName]);
            } else {
                $this->injectProperty($instance, $property);
            }
        }
    }

    protected function injectProperty(#[\SensitiveParameter] object $instance, \ReflectionProperty $property)
    {
        if ($property->getAttributes(Required::class)) {
            $objectOrNull = $this->getTypeInstanceOrNull($property->getType());
            if (!is_null($objectOrNull)) {
                $property->setValue($instance, $objectOrNull);
            }
            return;
        }

        if ($property->getAttributes(Lazy::class)) {
            $propertyName = $property->getName();
            // 1) trick to make unset typed properties invoke magic __get()
            // 2) trick to ignore protected or private attribute
            (function () use ($propertyName) {
                unset($this->{$propertyName});
            })->call($instance);
            return;
        }
    }
}

<?php declare(strict_types=1);

namespace Common;

class EnvData extends \Common\ArraySymlink
{
    function __construct()
    {
        parent::__construct('$_ENV');
    }
}

<?php declare(strict_types=1);

include dirname(__DIR__) . '/vendor/autoload.php';

/** @param string $className class, interface or trait */
function autoload(string $className)
{
    $fileName = __DIR__ . DIRECTORY_SEPARATOR . strtr($className, '\\', DIRECTORY_SEPARATOR) . '.php';
    if (is_readable($fileName)) {
        include $fileName;
    }
}

spl_autoload_register('autoload', true, true);

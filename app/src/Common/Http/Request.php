<?php declare(strict_types=1);

namespace Common\Http;

use Common\Str;

/**
 * - $fullUrl  == "{$scheme}://{$host}{$fullPath}"
 * - $fullUrl  == "{$baseUrl}{$path}"
 * - $fullPath == "{$basePath}{$path}"
 */
class Request
{
    function __construct(
        protected ServerData $serverData
    ) {
    }

    function webPath(...$args): string
    {
        $webRoot = $this->serverData['DOCUMENT_ROOT'];
        array_unshift($args, $webRoot);
        return Str::concatDir($args);
    }

    private readonly string $fullUrl;

    function getFullUrl()
    {
        return $this->fullUrl ??= (function (): string {
            if (!empty($this->serverData['SCRIPT_URI'])) {
                return $this->serverData['SCRIPT_URI'];
            }

            $scheme = $this->getScheme();
            $host = $this->getHost();
            $fullPath = $this->getFullPath();
            return "{$scheme}://{$host}{$fullPath}";
        })();
    }

    private readonly string $baseUrl;

    function getBaseUrl()
    {
        return $this->baseUrl ??= (function (): string {
            $url = $this->getFullUrl();
            $path = $this->getPath();

            assert (empty($path) || substr($url, -strlen($path)) === $path);
            return substr($url, 0, strlen($url) - strlen($path));
        })();
    }

    private readonly string $scheme;

    function getScheme()
    {
        return $this->scheme ??= (function (): string {
            if (!empty($this->serverData['REQUEST_SCHEME'])) {
                return $this->serverData['REQUEST_SCHEME'];
            }

            if (!empty($this->serverData['SCRIPT_URI'])) {
                return parse_url($this->serverData['SCRIPT_URI'], PHP_URL_SCHEME);
            }

            return (empty($this->serverData['HTTPS']) || $this->serverData['HTTPS'] === 'off') ? 'http' : 'https';
        })();
    }

    private readonly string $host;

    function getHost()
    {
        return $this->host ??= (function (): string {
            if (!empty($this->serverData['HTTP_HOST'])) {
                return $this->serverData['HTTP_HOST'];
            }

            $host = (string)$this->serverData['SERVER_NAME'];
            if (!empty($this->serverData['SERVER_PORT'])) {
                $port = (int)$this->serverData['SERVER_PORT'];
                $host .= ":{$port}";
            }

            return $host;
        })();
    }

    private readonly string $fullPath;

    function getFullPath()
    {
        return $this->fullPath ??= (function (): string {
            if (!empty($this->serverData['REQUEST_URI'])) {
                $path = $this->serverData['REQUEST_URI'];
            } else if (!empty($this->serverData['UNENCODED_URL'])) {
                $url = $this->serverData['UNENCODED_URL'];
                $path = parse_url($url, PHP_URL_PATH);
            } else {
                $path = $this->serverData['SCRIPT_NAME'];
            }
            return rawurldecode($path);
        })();
    }

    private readonly string $path;

    function getPath()
    {
        return $this->path ??= (function (): string {
            if (isset($this->serverData['PATH_INFO'])) {
                $path = $this->serverData['PATH_INFO'];
            } else {
                $path = $this->getFullPath();

                if (!empty($this->serverData['SCRIPT_NAME'])) {
                    $script = $this->serverData['SCRIPT_NAME'];
                    if (strpos($path, $script) === 0) {
                        $path = substr($path, strlen($script));
                    } else {
                        $script_dir = substr($script, 0, strrpos($script, '/'));
                        if (!empty($script_dir) && strpos($path, $script_dir) === 0) {
                            $path = substr($path, strlen($script_dir));
                        }
                    }
                }
            }

            return ltrim($path, '/');
        })();
    }

    private readonly string $basePath;

    function getBasePath()
    {
        return $this->basePath ??= (function (): string {
            $fullPath = $this->getFullPath();
            $path = $this->getPath();

            assert (empty($path) || substr($fullPath, -strlen($path)) === $path);
            $basePath = substr($fullPath, 0, strlen($fullPath) - strlen($path));
            assert (parse_url($this->getBaseUrl(), PHP_URL_PATH) === $basePath);
            return $basePath;
        })();
    }

    function isPost(): bool
    {
        return $this->serverData['REQUEST_METHOD'] === 'POST';
    }

    function isValidOrigin(): bool
    {
        if (!empty($this->serverData['HTTP_ORIGIN'])) {
            return $this->isSameOrigin($this->getFullUrl(), $this->serverData['HTTP_ORIGIN']);
        }

        if (!empty($this->serverData['HTTP_REFERER']) && parse_url($this->serverData['HTTP_REFERER'], PHP_URL_HOST)) {
            return $this->isSameOrigin($this->getFullUrl(), $this->serverData['HTTP_REFERER']);
        }

        return true;
    }

    protected static function isSameOrigin(string $url, string $url2): bool
    {
        assert (parse_url($url, PHP_URL_SCHEME));
        if (parse_url($url, PHP_URL_SCHEME) === parse_url($url2, PHP_URL_SCHEME)) {
            assert (parse_url($url, PHP_URL_HOST));
            if (parse_url($url, PHP_URL_HOST) === parse_url($url2, PHP_URL_HOST)) {
                if (parse_url($url, PHP_URL_PORT) === parse_url($url2, PHP_URL_PORT)) {
                    return true;
                }
            }
        }
        return false;
    }
}

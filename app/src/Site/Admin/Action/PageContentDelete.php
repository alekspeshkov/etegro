<?php declare(strict_types=1);

namespace Site\Admin\Action;

class PageContentDelete extends Action
{
    function __invoke()
    {
        $targetPage = $this->postData['page'];
        return $this->validate($targetPage) && $this->delete($targetPage);
    }

    protected function validate(string $pageName)
    {
        if (empty($pageName)) {
            $this->currentPage->flashData['page_empty'] = true;
        }

        if (!$this->getPage($pageName)->exists()) {
            $this->currentPage->flashData['notfound'] = true;
        }

        if ($this->currentPage->flashData->isEmpty() && $this->getPage($pageName)->hasChildren()) {
            $this->currentPage->flashData['isfolder'] = true;
        }

        $locationPage = $this->currentPage->getControllerTail();
        if ($this->currentPage->flashData->isEmpty() && $locationPage !== $pageName) {
            $this->currentPage->flashData['notcurrent'] = true;
        }

        if ($this->currentPage->flashData->isEmpty() && !$this->postData['confirm']) {
            $this->currentPage->flashData['confirm'] = true;
        }

        return $this->currentPage->flashData->isEmpty();
    }

    protected function delete(string $pageName)
    {
        try {
            $this->currentPage->delete($pageName);

            $parentOfDeletedPage = $this->getPage($pageName)->getParentPage()->getPageName();
            $adminTargetPage = $this->currentPage->setRedirectAdminPage($parentOfDeletedPage);
            $adminTargetPage->flashData->setAll(['deleted' => true]);

            $this->currentPage->formData->clear();
        } catch (\Throwable $e) {
            $this->currentPage->flashData->setAll(['database' => $e->getMessage()]);
            return false;
        }
    }
}

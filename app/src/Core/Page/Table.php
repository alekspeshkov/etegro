<?php declare(strict_types=1);

namespace Core\Page;

class Table extends \Common\Db\Table
{
    protected string $pageCol = 'page';

    function wherePage(Page $page)
    {
        $pageName = $page->getPageName();

        return $this->where([$this->pageCol => $pageName]);
    }

    function whereParent(Page $page)
    {
        $pageName = $page->getPageName();

        // home page children
        $home = implode(' AND ', [
            $this->db->quotePair('INSTR(~, ?)=0', $this->pageCol, '/'),
            $this->db->quotePair('CHAR_LENGTH(~) > CHAR_LENGTH(?)', $this->pageCol, $pageName)
        ]);

        // common parent and simple child
        $else = implode(' AND ', [
            $this->db->quoteInto('SUBSTR(~,CHAR_LENGTH(?)+1,1) = ?', [$this->pageCol, $pageName, '/']),
            $this->db->quoteInto('INSTR(SUBSTR(~,CHAR_LENGTH(?)+2),?)=0', [$this->pageCol, $pageName, '/'])
        ]);

        $sql = implode(' AND ', [
            $this->db->quotePair('LEFT(~,CHAR_LENGTH(?)) = ?', $this->pageCol, $pageName),
            $this->db->quoteInto("IF(RIGHT(?, 1) = ?, {$home}, {$else})", [$pageName, ':'])
        ]);

        $where = ["({$sql})"];
        return $this->extend(['where' => $where]);
    }
}

<?php declare(strict_types=1);

namespace Site\Admin\Action;

use Core\Page\PageRouting;

class PageLayoutDelete extends Action
{
    function __invoke(PageRouting $pageRouting)
    {
        $page = $this->postData['page'];

        if (empty($page) || !$pageRouting->exists('layout', $page)) {
            return false;
        }

        try {
            $pageRouting->delete('layout', $page);
            $this->currentPage->flashData['page_layout_deleted'] = true;
        } catch (\Throwable $e) {
            $this->currentPage->flashData['database'] = $e->getMessage();
            return false;
        }
    }
}

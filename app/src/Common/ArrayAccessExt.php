<?php declare(strict_types=1);

namespace Common;

trait ArrayAccessExt # implements \ArrayAccess
{
    abstract function offsetGet($offset);
    abstract function offsetSet($offset, $value): void;

    function offsetExists($offset): bool
    {
        return !is_null($this->offsetGet($offset));
    }

    function offsetUnset($offset): void
    {
        $this->offsetSet($offset, null);
    }

    function getAll(): array
    {
        return (array)$this->offsetGet(null);
    }

    function setAll($value): void
    {
        $this->offsetSet(null, $value);
    }

    function isEmpty(): bool
    {
        return empty($this->getAll());
    }

    function clear(): void
    {
        $this->setAll(null);
    }
}

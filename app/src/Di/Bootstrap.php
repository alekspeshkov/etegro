<?php declare(strict_types=1);

namespace Di;

use Di\Interface\ContainerInterface;
use Di\AppConfigRegistry;
use Di\Container;
use Di\ContainerFactory;

abstract class Bootstrap
{
    protected static ContainerInterface $container;

    static function getContainer()
    {
        return static::$container ??= static::createContainer();
    }

    /** @param string $className should be concrete instatiable class, as no service configued yet */
    static function invokeClass(string $className)
    {
        $container = static::getContainer();
        $instance = $container->get($className);
        return $container->invoke($instance);
    }

    protected static function createContainer(): ContainerInterface
    {
        $appConfig = new AppConfigRegistry(ConfigRecord::create());
        return new Container(new ContainerFactory(), $appConfig);
    }
}

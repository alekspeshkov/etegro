<?php declare(strict_types=1);

namespace Common;

class XmlWriter
{
    /** @param string|string[] $html */
    /** @return string|string[] */
    static function esc(string|array $html): string|array
    {
        if (is_array($html)) {
            return array_map([__CLASS__, __FUNCTION__], $html);
        } else {
            return htmlspecialchars($html, ENT_QUOTES | ENT_XML1 | ENT_DISALLOWED | ENT_SUBSTITUTE, 'UTF-8');
        }
    }

    static function xmlRaw(string $xmlString)
    {
        $dom = new \DOMDocument;
        $dom->loadXML($xmlString);
        return $dom;
    }

    /** escape raw data using $this->esc() BEFORE applying xml() */
    static function xml($content = false, ?string $root = null, string $defaultTag = 'element')
    {
        $xmlString = static::document($content, $root, $defaultTag);
        return static::xmlRaw($xmlString);
    }

    static function xmlText($content, $root = 'value', $defaultTag = 'element')
    {
        $content = static::esc($content);
        return static::xml($content, $root, $defaultTag);
    }

    protected static function document($content = false, ?string $root = null, string $defaultTag = 'element')
    {
        $xml = new \XMLWriter;

        $xml->openMemory();
        $xml->startDocument('1.0', 'UTF-8');
        $xml->startDTD('xsl:stylesheet');
        $xml->writeDTDEntity('nbsp', '&#160;');
        $xml->endDTD();

        if (!empty($root)) {
            static::element($xml, $root, $content, $defaultTag);
        } else {
            static::node($xml, $content, $defaultTag);
        }
        $xml->endDocument();

        return $xml->outputMemory();
    }

    protected static function node(\XMLWriter $xml, $content, string $defaultTag)
    {
        if (is_array($content)) {
            foreach ($content as $element => $value) {
                $name = is_numeric($element) ? $defaultTag : $element;
                static::element($xml, $name, $value, $defaultTag);
            }
        } else if (is_bool($content) || is_null($content)) {
            return $xml->writeElement(($content) ? 'true' : 'false');
        } else {
            return $xml->writeRaw((string)$content);
        }
    }

    protected static function element(\XMLWriter $xml, string $element, $content, string $defaultTag)
    {
        $tokens = explode(' ', $element);
        $name = array_shift($tokens);

        $xml->startElement($name);
        foreach ($tokens as $attr) {
            [$name, $value] = explode('=', $attr, 2);
            $value = trim($value, '"\'');
            $xml->writeAttribute($name, $value);
        }
        if (!empty($content) || $content === 0) {
            static::node($xml, $content, $defaultTag);
        }
        $xml->endElement();
    }
}

<?php declare(strict_types=1);

namespace Di;

use Di\Interface\ConfigInterface;
use Di\Interface\ConfigRecordInterface;

class ConfigRegistry implements ConfigInterface
{
    function __construct(
        protected readonly ConfigRecordInterface $templateRecord
    ) {
    }

    /** @var array<string, \Di\Interface\ConfigRecordInterface> */
    protected array $configs = [];

    function createRecord(?string $className, #[\SensitiveParameter] array $params = []): ConfigRecordInterface
    {
        return $this->templateRecord->extendWith($className, $params);
    }

    function get(string $name): ?ConfigRecordInterface
    {
        return $this->configs[$name] ?? null;
    }

    function extendFrom(#[\SensitiveParameter] ConfigRecordInterface $record, string $name): ConfigRecordInterface
    {
        return $record->extendFrom($this->get($name));
    }

    function extendParams(#[\SensitiveParameter] ConfigRecordInterface $record, string $name): ConfigRecordInterface
    {
        return $record->extendParams($this->get($name));
    }

    function configure(#[\SensitiveParameter] array $entries): static
    {
        foreach ($entries as $name => $params) {
            $this->set($name, $params);
        }

        return $this;
    }

    function set(string $name, #[\SensitiveParameter] array|string $params): static
    {
        $record = $this->createRecord(null, $this->normalizeParams($params));
        $this->configs[$name] = $this->extendFrom($record, $name);

        return $this;
    }

    protected function normalizeParams(#[\SensitiveParameter] array|string $params): array
    {
        if (is_string($params)) {
            $params = ['::class' => $params];
        }
        return $params;
    }
}

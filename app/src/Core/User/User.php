<?php declare(strict_types=1);

namespace Core\User;

use Core\Page\CurrentPage;

class User
{
    function __construct(
        protected UserAuth $userAuth,
        protected UserRoles $userRoles,
        protected UserSession $userSession,
    ) {
    }

    protected string|false $userName;

    function getUserName()
    {
        return $this->userName ??= $this->userSession->getName() ?: false;
    }

    function login(?string $userName, #[\SensitiveParameter] ?string $password)
    {
        if (!$this->userAuth->isValid($userName ?: '', $password ?: '')) {
            return false;
        }

        $this->userName = $userName;
        $this->userSession->setName($userName);
        return true;
    }

    function logout()
    {
        $this->userName = false;
        $this->userSession->logout();
    }

    function canAccess(CurrentPage $page)
    {
        $allPagesRoles = $page->getRoles();
        if (empty($allPagesRoles)) {
            return true;
        }

        $userRoles = $this->userRoles->getUserRoles($this->getUserName() ?: '');

        // check user access rights for each protected page in the ancestor list
        foreach ($allPagesRoles as $pageRoles) {
            $intersect = array_intersect($pageRoles, $userRoles);
            if (empty($intersect)) {
                // some page in the controller's list is forbidden
                return false;
            }
        }

        // no restrictions found
        return true;
    }
}

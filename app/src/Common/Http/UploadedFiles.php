<?php declare(strict_types=1);

namespace Common\Http;

class UploadedFiles extends \Common\ArraySymlink
{
    function __construct()
    {
        parent::__construct('$_FILES');
    }

    function visit(callable $visitor)
    {
        $errors = [];
        foreach ($this->getAll() as $name => $file) {
            if (is_array($file['error'])) {
                $files = [];
                foreach ($file as $key => $array) {
                    foreach ($array as $index => $value) {
                        $files["{$name}[{$index}]"][$key] = $value;
                    }
                }

                foreach ($files as $name => $file) {
                    $file['input_name'] = $name;
                    static::visitUploadedFile($visitor, $file, $errors);
                }
            } else {
                $file['input_name'] = $name;
                static::visitUploadedFile($visitor, $file, $errors);
            }
        }
        return $errors;
    }

    protected static function visitUploadedFile(callable $visitor, array $file, array &$errors)
    {
        if ($file['error'] === UPLOAD_ERR_OK) {
            return call_user_func($visitor, $file);
        } else if ($file['error'] !== UPLOAD_ERR_NO_FILE) {
            $errors[$file['input_name']] = $file['error'];
        }
    }
}

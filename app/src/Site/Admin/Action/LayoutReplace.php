<?php declare(strict_types=1);

namespace Site\Admin\Action;

use Core\Page\LayoutContent;
use Core\Page\Validate;

class LayoutReplace extends Action
{
    function __invoke(LayoutContent $layoutContent, Validate $validator)
    {
        $this->savePostData();

        $isOk = true;
        $form = $this->currentPage->formData->getAll();

        if (empty($form['layout'])) {
            $this->currentPage->flashData['notfound'] = true;
            $this->currentPage->formData->clear();
            $isOk = false;
        } else {
            $layoutName = $form['layout'];
            $errorMsg = $validator->validateString($layoutName);
            if (!empty($errorMsg)) {
                $this->currentPage->flashData['layout'] = $errorMsg;
                $isOk = false;
            }
        }

        if (empty($form['content'])) {
            $this->currentPage->flashData['notfound'] = true;
            $isOk = false;
        } else {
            $errorMsg = $validator->validateXml($form['content']);
            if (!empty($errorMsg)) {
                $this->currentPage->flashData['content'] = $errorMsg;
                $isOk = false;
            }
        }

        return $isOk && $this->overwrite($layoutContent);
    }

    function overwrite(LayoutContent $layoutContent)
    {
        try {
            $layoutName = $this->currentPage->formData['layout'];
            $content = $this->currentPage->formData['content'];
            $layoutContent->save($layoutName, $content);

            $adminTargetPage = $this->currentPage->setRedirectAdminPage($layoutName);
            $adminTargetPage->flashData->setAll(['updated' => true]);

            $this->currentPage->formData->clear();
        } catch (\Throwable $e) {
            $this->currentPage->flashData['database'] = $e->getMessage();
            return false;
        }
    }
}

<?php declare(strict_types=1);

namespace test;

use UnitTestCase;
use Common\Db\Connection as Db;
use Core\Page\Table;

class DbTableTest extends UnitTestCase
{
    protected Table $table;

    function __construct() {
        $db = new Db($_ENV['MYSQL_DSN'], $_ENV['MYSQL_USER'], $_ENV['MYSQL_PASSWORD']);
        $this->table = new Table($db);
    }

    function test_prepare_delete() {
        $table = $this->table->from('table');

        $this->assertIdentical(
            'DELETE FROM `table`',
            $table->prepare_delete()
        );
        $this->assertIdentical(
            'DELETE FROM `table` WHERE `a`=1',
            $table->where(['a'=>1])->prepare_delete()
        );
        $this->assertIdentical(
            'DELETE FROM `table` WHERE `a`=1 AND `b`=2',
            $table->where(['a'=>1,'b'=>2])->prepare_delete()
        );
        $this->assertIdentical(
            'DELETE FROM `table` WHERE `a`=1 AND `b`=2',
            $table->where(['a'=>1])->where(['b'=>2])->prepare_delete()
        );
        $this->assertIdentical(
            'DELETE FROM `table` WHERE `a`=1',
            $table->prepare_delete(['a'=>1])
        );
        $this->assertIdentical(
            'DELETE FROM `table` WHERE `a`=1 AND `b`=2',
            $table->where(['a'=>1])->prepare_delete(['b'=>2])
        );
    }

    function test_update() {
        $this->assertIdentical( "UPDATE `page_title` SET `page`='test', `title`='t', `label`='d'",
            $this->table->from('page_title')->prepare_update(['page'=>'test', 'title'=>'t', 'label'=>'d'])
        );
        $this->assertIdentical( "UPDATE `page_title` SET `title`='t', `label`='d' WHERE `page`='test'",
            $this->table->from('page_title')->where(['page'=>'test'])->prepare_update(['title'=>'t', 'label'=>'d'])
        );
    }

    function test_insert() {
        $this->assertIdentical( "INSERT INTO `page_title` (`page_title`,`page_content`) VALUES (1,2),(3,4)",
            $this->table->from('page_title')->prepare_insert( [['page_title'=>1, 'page_content'=>2],['page_title'=>3, 'page_content'=>4]] )
        );
    }

    function test_prepare_query_1() {
        $this->assertIdentical( "SELECT `page_title` FROM `page_title` WHERE `page`='test' LIMIT 1",
           $this->table->from('page_title')->where(['page'=>'test'])->prepare_query_1('page_title')
        );
        $this->assertIdentical( "SELECT `password` FROM `auth_user` WHERE `username`='user' AND `is_active`=1 LIMIT 1",
            $this->table->from('auth_user')->where(['username'=>'user', 'is_active'=>1])->prepare_query_1('password')
        );
        $this->assertIdentical( "SELECT * FROM `page_title` WHERE `page`='test' LIMIT 1",
            $this->table->from('page_title')->where(['page'=>'test'])->prepare_query_1()
        );
        $this->assertIdentical( "SELECT `route` AS `page` FROM `page_title` WHERE `page`='test' LIMIT 1",
            $this->table->from('page_title')->where(['page'=>'test'])->prepare_query_1(['route' => 'page'])
        );
    }

    function test_prepare_query() {
        $this->assertIdentical( "SELECT `page_title`, `page_content` FROM `page_title` WHERE `page`='test'",
            $this->table->from('page_title')->where(['page'=>'test'])->prepare_query('page_title','page_content')
        );
        $this->assertIdentical( "SELECT `route` AS `page`, `layout` FROM `page_title` WHERE `route`='test' LIMIT 1",
            $this->table->from('page_title')->where(['route'=>'test'])->prepare_query_1(['route' => 'page'], 'layout')
        );
        $this->assertIdentical( "SELECT * FROM `page_title` WHERE `page`='test'",
            $this->table->from('page_title')->where(['page'=>'test'])->prepare_query()
        );
        $pages = ['a','b', 'c'];
        $this->assertIdentical( "SELECT * FROM `page_title` WHERE `page` IN('a','b','c') ORDER BY FIELD(`page`, 'a','b','c')",
            $this->table->from('page_title')->where(['page'=>$pages])->order(['page'=>$pages])->prepare_query()
        );
        $this->assertIdentical( "SELECT * FROM `page_title` WHERE `page` IN('a','b','c') AND `title`='' ORDER BY FIELD(`page`, 'a','b','c') DESC",
            $this->table->from('page_title')->where(['page'=>$pages])->where(['title'=>''])->orderDesc(['page'=>$pages])->prepare_query()
        );

        $this->assertIdentical( "SELECT * FROM `page_title` NATURAL LEFT JOIN `page_layout`",
            $this->table->from('page_title')->naturalLeftJoin('page_layout')->prepare_query()
        );
        $this->assertIdentical( "SELECT * FROM `page_title` NATURAL LEFT JOIN `page_layout` NATURAL LEFT JOIN `page_template`",
            $this->table->from('page_title')->naturalLeftJoin('page_layout')->naturalLeftJoin('page_template')->prepare_query()
        );

        /*$this->assertIdentical( "SELECT COUNT(*) FROM `page_title` WHERE `page`='test'",
            $this->table->from('page_title')->where(['page'=>'test'])->fetchCount()
        );*/
        $this->assertIdentical( "SELECT DISTINCT `page` FROM `page_title` WHERE `page`='test'",
            $this->table->from('page_title')->where(['page'=>'test'])->selectFunc('DISTINCT ~', ['page'])->prepare_query()
        );

        $this->assertIdentical( "SELECT * FROM `page_title` WHERE `page`='test' AND LEFT(`page`, LENGTH('ru/support')) = 'ru/support'",
            $this->table->from('page_title')->where(['page'=>'test'])->where(['page'=>'ru/support'], 'LEFT(~, LENGTH(?)) = ?')->prepare_query()
        );

    }

}

<?php declare(strict_types=1);

namespace Core\User;

use Common\Http\Session;

class UserSession
{
    static string $user = 'user';

    function __construct(
        protected Session $session
    ) {
    }

    function getName(): string
    {
        return $this->session[static::$user] ?? '';
    }

    function setName(string $name)
    {
        $this->session[static::$user] = $name;
    }

    function logout()
    {
        $this->session->destroy();
    }
}

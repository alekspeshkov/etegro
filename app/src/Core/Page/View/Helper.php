<?php declare(strict_types=1);

namespace Core\Page\View;

use Common\Http\Request;
use Common\Http\Response;
use Core\Page\GetPage;
use Core\Page\CurrentPage as Page;
use Core\Page\XmlWriter;

class Helper extends \Core\Xml\View\Helper
{
    use GetPage;

    function form($name, Page $currentPage)
    {
        return $currentPage->formData->getHtmlFormField($name);
    }

    function error($name, Page $currentPage)
    {
        return $currentPage->flashData->getHtmlFormField($name);
    }

    function page_not_found_content($name)
    {
        return $this->getPage($name)->fetch404Content();
    }

    function path_content($path)
    {
        return $this->getPage($path)->fetchContent();
    }

    function path_ancestors_pages($path)
    {
        return $this->getPage($path)->fetchAncestors();
    }

    function page_children_pages($name)
    {
        return $this->getPage($name)->fetchChildren();
    }

    function path_tail($path)
    {
        return $this->getPage($path)->fetchTail();
    }

    function is_file($filename, Request $request)
    {
        $pathname = $request->webPath($filename);
        return is_readable($pathname) && is_file($pathname);
    }

    function set_status($status, Response $response)
    {
        $response->setStatus((int)$status);
    }

    function data_uri($filename, Request $request)
    {
        $pathname = $request->webPath($filename);
        $content = file_get_contents($pathname);

        $mime = finfo_buffer(finfo_open(FILEINFO_MIME_TYPE), $content);
        $base64 = base64_encode($content);
        return "data:{$mime};base64,{$base64}";
    }

    function _pages($result, XmlWriter $xml)
    {
        return $xml->xml($result, 'pages xmlns:a="page"', 'page');
    }

    function _content($result, XmlWriter $xml)
    {
        return $xml->xml($result, 'content xmlns:a="page"');
    }
}

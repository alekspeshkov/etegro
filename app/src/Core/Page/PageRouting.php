<?php declare(strict_types=1);

namespace Core\Page;

use Common\Db\Table;

class PageRouting
{
    static string $tableName = 'type_route_page';

    protected readonly Table $theTable;

    function __construct(Table $table) {
        $this->theTable = $table->from(static::$tableName);
    }

    function fetchOne(string $type, CurrentPage $page): string
    {
        $routes = $this->fetchAll($type, $page);

        if (empty($routes)) {
            $pageName = $page->getPageName();
            throw new NotFoundException("No route '{$type}' for the page '{$pageName}'", 404);
        }

        /** @var string */
        $route = key($routes);
        return reset($routes[$route]);
    }

    /** @return array<string, array<string, string>> */
    function fetchAll(string $type, CurrentPage $page): array
    {
        $candidates = $page->getRouteCandidates();

        // unordered
        $routes = $this->theTable->where(['type' => $type, 'route' => $candidates])->fetchPairs('route', 'page');

        $result = [];
        foreach ($candidates as $candidate) {
            if (isset($routes[$candidate])) {
                $result[$candidate] = [$routes[$candidate] => $routes[$candidate]];
            }
        }

        return $result;
    }

    function getAllTemplates()
    {
        return $this->theTable->where(['type' => 'template'])
            ->order(['page', 'route'])
            ->fetchAll(['page' => 'template'], ['route' => 'page']);
    }

    function findData(string $type, string $page): ?string
    {
        return $this->theTable->where(['type' => $type, 'page' => $page])->fetchOneOrNull('route');
    }

    function exists(string $type, string $route)
    {
        return $this->theTable->where(['type' => $type, 'route' => $route])->exists();
    }

    function insert(string $type, string $route, string $page)
    {
        $this->theTable->insert(['type' => $type, 'route' => $route, 'page' => $page]);
    }

    function delete(string $type, string $route)
    {
        $this->theTable->delete(['type' => $type, 'route' => $route]);
    }
}

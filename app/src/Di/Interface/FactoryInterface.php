<?php declare(strict_types=1);

namespace Di\Interface;

interface FactoryInterface
{
    /** Create the concrete class instance
     * @template T
     * @param class-string<T> $className concrete existing class name
     * @param array<int|string, mixed> constructor $params
     * @return T instance of $className
     */
    function newInstance(string $className, array $params = []): object;

    /** Invoke $instance's method with resolved parameters
     * @param array<int|string, mixed> $params */
    function invoke(object $instance, ?string $methodName = null, array $params = []);
}

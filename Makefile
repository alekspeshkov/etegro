include .env

.PHONY: help start stop restart up down build sh clean clean-all build-all composer system init
.DEFAULT_GOAL := help

help: ## this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

start: stop up

stop: down

restart: start

up: ## docker up
	docker compose up -d

down: ## docker down
	docker compose down --remove-orphans

build: stop ## docker build
	docker compose up --build -d

sh: up ## execute terminal in php cli container
	docker exec -it $(PHP_CLI) sh

clean: stop
	rm -rf var/app/*
	rm -rf var/log/nginx/*
	rm -rf var/log/php/*
	rm -rf var/log/mysql/*

clean-all: down ## clean everything
	docker compose rm -f
	docker system prune
	rm -rf .${APP_ROOT}/vendor
	rm -f  .${APP_ROOT}/composer.lock
	rm -rf var

build-all: clean-all composer var/ssl build ## clean everything then rebuild everything

composer: ## run composer update
	docker run --rm \
		--tty \
		--volume ./$(APP_ROOT):$(APP_ROOT) \
		composer update --ignore-platform-reqs

system: ## set privileged port access
	sudo setcap cap_net_bind_service=ep $(shell which rootlesskit)
	systemctl --user restart docker

init: .env system build-all

var/ssl: var ## create ssl data for nginx https
	mkdir -p var/ssl
	docker run --rm \
		--env SSL_SUBJECT="localhost" \
		--volume ./var/ssl:/certs \
		paulczar/omgwtfssl

var:
	mkdir -p var
	mkdir -p var/app
	mkdir -p var/log
	mkdir -p var/log/nginx
	mkdir -p var/log/php
	mkdir -p var/log/mysql && chmod 2777 var/log/mysql
	mkdir -p var/mysql && chmod 2777 var/mysql

.env:
	cp example.env .env

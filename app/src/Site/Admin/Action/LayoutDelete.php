<?php declare(strict_types=1);

namespace Site\Admin\Action;

use Core\Page\LayoutContent;
use Core\Page\PageRouting;

class LayoutDelete extends Action
{
    function __invoke(LayoutContent $layoutContent, PageRouting $pageRouting)
    {
        $layout = $this->postData['layout'];

        if (empty($layout) || !$layoutContent->exists($layout)) {
            return false;
        }

        if ($pageRouting->findData('layout', $layout)) {
            $this->currentPage->flashData['layout_used'] = true;
            return false;
        }

        try {
            $layoutContent->delete($layout);
            $this->currentPage->flashData['layout_deleted'] = true;
        } catch (\Throwable $e) {
            $this->currentPage->flashData['database'] = $e->getMessage();
            return false;
        }
    }
}

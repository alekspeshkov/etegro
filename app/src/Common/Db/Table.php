<?php declare(strict_types=1);

namespace Common\Db;

use Common\Db\Connection as Db;
use Common\Str;

/** Immutable implementation */
class Table
{
    function __construct(protected readonly Db $db)
    {
    }

    private readonly string $tableName;
    private readonly string $from;

    /** @var string[] */
    private readonly array $select;
    /** @var string[] */
    private readonly array $where;
    /** @var string[] */
    private readonly array $orderBy;

    function extend(array $changes): static
    {
        $clone = new static($this->db);

        $clone->tableName = $changes['tableName'] ?? $this->tableName;
        $clone->from = $changes['from'] ?? $this->from;
        $clone->select = array_merge($this->select ?? [], $changes['select'] ?? []);
        $clone->where = array_merge($this->where ?? [], $changes['where'] ?? []);
        $clone->orderBy = array_merge($this->orderBy ?? [], $changes['orderBy'] ?? []);

        return $clone;
    }

    /** @param string[] $strings */
    protected static function concat(array $strings, string $glue = ' ')
    {
        return Str::concat($strings, $glue);
    }

    function from(string $tableName)
    {
        $from = $this->db->quoteInto('FROM ~', [$tableName]);
        return $this->extend(['tableName' => $tableName, 'from' => $from]);
    }

    function naturalLeftJoin(string $tableName)
    {
        $from = $this->from . $this->db->quoteInto(' NATURAL LEFT JOIN ~', [$tableName]);
        return $this->extend(['from' => $from]);
    }

    function leftJoin(string $tableName, string $col1, string $col2)
    {
        $from = $this->from . $this->db->quoteInto(' LEFT JOIN ~ ON ~ = ~', [$tableName, $col1, $col2]);
        return $this->extend(['from' => $from]);
    }

    function selectFunc($sql, array $args = [])
    {
        $select = [$this->db->quoteInto($sql, $args)];
        return $this->extend(['select' => $select]);
    }

    function where(array $columns = [], $predicate = '~=?')
    {
        $where = [];

        foreach ($columns as $column => $value) {
            if (is_array($value)) {
                if (empty($value)) {
                    $value = '';
                }
                $where[] = $this->db->quotePair('~ IN(?)', $column, $value);
            } else {
                $where[] = $this->db->quotePair($predicate, $column, $value);
            }
        }

        return $this->extend(['where' => $where]);
    }

    function whereFunc(string $sql, array $args = [])
    {
        $where = [$this->db->quoteInto($sql, $args)];
        return $this->extend(['where' => $where]);
    }

    /** @param string|array<int|string, string> $columns */
    function order($columns)
    {
        $orderBy = [];
        foreach ((array)$columns as $key => $value) {
            if (is_int($key)) {
                $orderBy[] = $this->db->quoteIdentifier($value);
            } else if (is_array($value) && !empty($value)) {
                $orderBy[] = $this->db->quotePair('FIELD(~, ?)', $key, $value);
            }
        }

        return $this->extend(['orderBy' => $orderBy]);
    }

    /** @param string|array<int|string, string> $columns */
    function orderDesc($columns)
    {
        $orderBy = [];
        foreach ((array)$columns as $key => $value) {
            if (is_int($key)) {
                $orderBy[] = $this->db->quoteInto('~ DESC', [$value]);
            } else if (is_array($value) && !empty($value)) {
                $orderBy[] = $this->db->quotePair('FIELD(~, ?) DESC', $key, $value);
            }
        }

        return $this->extend(['orderBy' => $orderBy]);
    }

    function orderFunc(string $sql, array $args = []): static
    {
        $orderBy = [$this->db->quoteInto($sql, $args)];
        return $this->extend(['orderBy' => $orderBy]);
    }

    protected function select(array $cols)
    {
        $select = [];

        foreach ($cols as $column => $alias) {
            if (is_array($alias)) {
                $select = array_merge($select, $this->select($alias));
            } else if (is_string($column)) {
                $select[] = $this->db->quoteInto('~ AS ~', [$column, $alias]);
            } else {
                assert(is_int($column));
                $select[] = $this->db->quoteInto('~', [$alias]);
            }
        }

        return $select;
    }

    function prepare_query(...$args): string
    {
        $select = $this->select($args);

        $self = $this->extend(['select' => $select]);

        $sql[] = $self->prepare_select();
        $sql[] = $self->from;
        $sql[] = $self->prepare_where();
        $sql[] = $self->prepare_order();
        $sql = static::concat($sql);
        return $sql;
    }

    function prepare_query_1(...$args): string
    {
        $sql = $this->prepare_query(...$args);
        return $sql . ' LIMIT 1';
    }

    protected function prepare_select()
    {
        $result = static::concat($this->select, ', ');
        return 'SELECT ' . (empty($result) ? '*' : $result);
    }

    protected function prepare_where()
    {
        $result = static::concat($this->where, ' AND ');
        return empty($result) ? '' : "WHERE {$result}";
    }

    protected function prepare_order()
    {
        $result = static::concat($this->orderBy, ', ');
        return empty($result) ? '' : "ORDER BY {$result}";
    }

    function prepare_update($data, $predicate = '~=?')
    {
        if (empty($data)) {
            return '';
        }

        $set = [];
        foreach ($data as $column => $value) {
            $set[] = $this->db->quotePair($predicate, $column, $value);
        }

        $sql[] = $this->db->quoteInto('UPDATE ~', [$this->tableName]);
        $sql[] = 'SET ' . static::concat($set, ', ');
        $sql[] = $this->prepare_where();
        $sql = static::concat($sql);
        return $sql;
    }

    function prepare_insert($values)
    {
        if (is_numeric(key($values))) {
            $cols = array_keys(current($values));
        } else {
            $cols = array_keys($values);
            $values = [$values];
        }

        $sql[] = $this->db->quoteInto('INSERT INTO ~', [$this->tableName]);
        $sql[] = $this->db->quoteInto('(~) VALUES ?', [$cols, $values]);
        $sql[] = $this->prepare_where();
        $sql = static::concat($sql);
        return $sql;
    }

    function prepare_replace(...$args)
    {
        $sql = $this->prepare_insert(...$args);
        $sql = 'REPLACE' . substr($sql, strlen('INSERT'));
        return $sql;
    }

    function prepare_delete(...$args)
    {
        $sql = [$this->db->quoteInto('DELETE FROM ~', [$this->tableName])];

        $self = $this->where(...$args);
        $sql[] = $self->prepare_where();

        $sql = static::concat($sql);
        return $sql;
    }

    function update(...$args)
    {
        $sql = $this->prepare_update(...$args);
        return $this->db->exec($sql);
    }

    function replace(...$args)
    {
        $sql = $this->prepare_replace(...$args);
        return $this->db->exec($sql);
    }

    function insert(...$args)
    {
        $sql = $this->prepare_insert(...$args);
        return $this->db->exec($sql);
    }

    function delete(...$args)
    {
        $sql = $this->prepare_delete(...$args);
        return $this->db->exec($sql);
    }

    function fetchAll(...$args)
    {
        $sql = $this->prepare_query(...$args);
        return $this->db->fetchAll($sql);
    }

    function fetchCol(...$args)
    {
        $sql = $this->prepare_query(...$args);
        return $this->db->fetchCol($sql);
    }

    function fetchPairs(...$args)
    {
        $sql = $this->prepare_query(...$args);
        return $this->db->fetchPairs($sql);
    }

    function fetchPairsMany(...$args)
    {
        $sql = $this->prepare_query(...$args);
        return $this->db->fetchPairsMany($sql);
    }

    function fetchRow(...$args)
    {
        $sql = $this->prepare_query_1(...$args);
        return $this->db->fetchRow($sql);
    }

    function fetchRowOrNull(...$args)
    {
        $sql = $this->prepare_query_1(...$args);
        return $this->db->fetchRowOrNull($sql);
    }

    function fetchOne(...$args)
    {
        $sql = $this->prepare_query_1(...$args);
        return $this->db->fetchOne($sql);
    }

    function fetchOneOrNull(...$args)
    {
        $sql = $this->prepare_query_1(...$args);
        return $this->db->fetchOneOrNull($sql);
    }

    function exists()
    {
        return $this->selectFunc('COUNT(*)')->fetchOne() > 0;
    }

}

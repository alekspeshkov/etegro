<?php declare(strict_types=1);

namespace Common;

abstract class File
{
    static function makeFile(string $filename, string $content): bool
    {
        if (file_exists($filename)) {
            return false;
        }

        $dirname = realpath(dirname($filename));
        if (!is_writable($dirname)) {
            return false;
        }

        $tempname = tempnam($dirname, basename($filename));
        if ($tempname === false) {
            return false;
        }

        if (dirname($tempname) !== $dirname) {
            return false;
        }

        $perms = fileperms($dirname);
        if ($perms === false) {
            return false;
        }

        if (file_put_contents($tempname, $content) === false) {
            return false;
        }

        if (
            !(
                chmod($tempname, $perms & ~07111)
                && rename($tempname, $filename)
            )
        ) {
            unlink($tempname);
            return false;
        }

        return true;
    }

    /** writes a file, making directory tree if needed */
    static function write(string $filename, string $content): bool
    {
        return
            static::makeDir(dirname($filename))
            && static::makeFile($filename, $content);
    }

    /** updates a file, if content differs, making directory tree if needed */
    static function updateSafe(string $filename, string $content): bool
    {
        if (strlen($content) == 0) {
            return !file_exists($filename) || unlink($filename);
        }

        if (file_exists($filename)) {
            if (file_get_contents($filename) === $content) {
                return true;
            }

            unlink($filename);
        }

        return static::write($filename, $content);
    }

    /** makes directory tree recursively and copy parent's permissions */
    static function makeDir(string $path): bool
    {
        if (is_dir($path)) {
            return true;
        }

        $parent = dirname($path);

        return static::makeDir($parent)
            && is_writable($parent)
            && mkdir($path)
            && chmod($path, fileperms($parent))
        ;
    }

    static function copy(string $from, string $to): bool
    {
        return is_readable($from)
            && static::makeDir(dirname($to))
            && copy($from, $to)
        ;
    }
}

<?php declare(strict_types=1);

namespace Core\Page;

use Di\Attribute\Required;

trait GetPage
{
    #[Required] protected PageFactory $pageFactory;

    /** Get cached or newly created Page object
     * @return \Core\Page\Page
     */
    protected function getPage(string $name, ?string $className = null): Page
    {
        return $this->pageFactory->getPage($name, $className);
    }
}

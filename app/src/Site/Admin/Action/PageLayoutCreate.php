<?php declare(strict_types=1);

namespace Site\Admin\Action;

use Core\Page\PageRouting;

class PageLayoutCreate extends Action
{
    function __invoke(PageRouting $pageRouting)
    {
        $postData = $this->postData->getAll();

        if (empty($postData['layout']) || empty($postData['page'])) {
            return false;
        }

        $route = $postData['page'];
        $layout = $postData['layout'];
        try {
            if ($pageRouting->exists('layout', $route)) {
                $pageRouting->delete('layout', $route);
                $pageRouting->insert('layout', $route, $layout);
                $this->currentPage->flashData['page_layout_updated'] = true;
            } else {
                $pageRouting->insert('layout', $route, $layout);
                $this->currentPage->flashData['page_layout_created'] = true;
            }
        } catch (\Throwable $e) {
            $this->currentPage->flashData['database'] = $e->getMessage();
            return false;
        }
    }
}

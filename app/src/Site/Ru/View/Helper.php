<?php declare(strict_types=1);

namespace Site\Ru\View;

class Helper extends \Core\Page\View\Helper
{
    function product_meta($path, Configurator $configurator)
    {
        return $configurator->productMeta($path);
    }

    function product_support_content($model, Configurator $configurator)
    {
        return $configurator->productSupport($model);
    }

    function product_description_content($model, Configurator $configurator)
    {
        return '';
    }

    function product_spec($model, Configurator $configurator)
    {
        return $configurator->productSpec($model);
    }

    function spec_summary($path, Configurator $configurator)
    {
        return $configurator->specSummary($path);
    }

    function path_models_pages($path, Configurator $configurator)
    {
        return $configurator->pathModels($path);
    }

    function model_pictures($model, Configurator $configurator)
    {
        return $configurator->modelPictures($model);
    }

    function model_price($model, Configurator $configurator)
    {
        return $configurator->modelPrice($model);
    }
}

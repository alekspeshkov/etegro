<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href='stylesheet://output'/>
<xsl:import href='stylesheet://url'/>

<xsl:template mode="head" match="@* | node()"/>

<xsl:strip-space elements="*"/>
<xsl:preserve-space elements="pre"/>

<xsl:template match="title[local-name(..) != 'head'] | meta[local-name(..) != 'head'] | link[local-name(..) != 'head'] | script[local-name(..) != 'head']"/>
<xsl:template mode="head" match="title | meta | link | script"><xsl:element name="{local-name()}"><xsl:apply-templates select="@* | node()"/></xsl:element></xsl:template>

<xsl:template mode="head" match="h1">
    <xsl:if test="not(../title) and not($title/text())">
        <title><xsl:apply-templates/></title>
    </xsl:if>
</xsl:template>

<xsl:template mode="head" match="title">
    <xsl:if test="text()">
        <xsl:element name="{local-name()}"><xsl:apply-templates/></xsl:element>
    </xsl:if>
</xsl:template>

<xsl:template match="head">
    <xsl:element name="{local-name()}">
        <xsl:apply-templates mode="head" select="$title"/>
        <xsl:apply-templates mode="head" select="$content/node()"/>

        <xsl:apply-templates/>

        <xsl:if test="$private/node()">
            <meta name="robots" content="noindex"/>
        </xsl:if>
    </xsl:element>
</xsl:template>

<xsl:template match="a/@href | script/@src | img/@src | q/@cite | link/@href | form/@action">
    <xsl:attribute name="{local-name()}">
        <xsl:call-template name="url"><xsl:with-param name="href" select="."/></xsl:call-template>
    </xsl:attribute>
</xsl:template>

<xsl:template match="a[@href]">
    <xsl:choose>
        <xsl:when test="$path = @href">
            <xsl:apply-templates/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:element name="{local-name()}">
                <xsl:apply-templates select="@* | node()"/>
            </xsl:element>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="img[not(@alt)]">
    <xsl:element name="{local-name()}">
        <xsl:attribute name="alt"></xsl:attribute>
        <xsl:apply-templates select="@* | node()"/>
    </xsl:element>
</xsl:template>

<xsl:template match="li[a[@href]]">
    <xsl:variable name="pathx" select="concat($path,'/')"/>
    <xsl:variable name="hrefx" select="concat(a/@href,'/')"/>
    <xsl:element name="{local-name()}">
        <xsl:if test="a/@href=$path or (starts-with($pathx,$hrefx) and not(following-sibling::li/a[@href=$path])) and not(following-sibling::li[@class='selected'])">
            <xsl:attribute name="class">selected</xsl:attribute>
        </xsl:if>
        <xsl:apply-templates select="@* | node()"/>
    </xsl:element>
</xsl:template>

<xsl:template match="form[not(@action)]">
    <xsl:element name="{local-name()}">
        <xsl:attribute name="action">
            <xsl:call-template name="url"><xsl:with-param name="href" select="$path"/></xsl:call-template>
        </xsl:attribute>
        <xsl:apply-templates select="@* | node()"/>
    </xsl:element>
</xsl:template>

<xsl:template match="input[not(@id) and not(@type='hidden' or @type='image' or @type='submit' or @type='reset' or @type='button')]/@name | textarea[not(@id)]/@name | select[not(@id)]/@name">
    <xsl:copy/>
    <xsl:attribute name="id"><xsl:value-of select="."/></xsl:attribute>
</xsl:template>

<xsl:template name="html-a-href">
    <xsl:param name="href" select="string(path)"/>
    <xsl:param name="body" select="label/node()"/>
    <a>
        <xsl:if test="$href != $path">
            <xsl:attribute name="href">
                <xsl:call-template name="url"><xsl:with-param name="href" select="string($href)"/></xsl:call-template>
            </xsl:attribute>
        </xsl:if>
        <xsl:copy-of select="$body"/>
    </a>
</xsl:template>

</xsl:stylesheet>

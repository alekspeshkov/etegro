<?php declare(strict_types=1);

namespace Core\Page;

use Di\Attribute\Required;

readonly class PageNaming
{
    #[Required] protected string $currentSite;
    #[Required] protected string $currentPath;
    protected string $currentPageName;

    function getCurrent()
    {
        return $this->currentPageName ??= static::joinSitePath($this->currentSite, $this->currentPath);
    }

    function normalize(string $name)
    {
        return static::hasSite($name) ? $name : static::joinSitePath($this->currentSite, $name);
    }

    static function joinSitePath(string $site, string $path)
    {
        return "{$site}:{$path}";
    }

    /** search for site prefix at the begining of $page */
    static function hasSite(string $page)
    {
        $root = explode('/', $page, 2)[0];
        return (strpos($root, ':') !== false);
    }

    static function pageSite(string $page)
    {
        return explode(':', $page, 2)[0];
    }

    static function pagePath(string $page)
    {
        return explode(':', $page, 2)[1];
    }

    static function root(string $page)
    {
        return static::pageSite($page) . ':';
    }

    /** @param array<string, string>[] $array
     * @return array<string, string>[]
     */
    static function injectPath(array $array): array
    {
        /** @param array<string, string> $row */
        return array_map(function (array $row) {
            $row['path'] = static::pagePath($row['page']);
            return $row;
        }, $array);
    }

    static function getParent(string $path, string $delimiter = '/')
    {
        $pos = strrpos($path, $delimiter);
        return substr($path, 0, $pos ?: 0);
    }

    /** creates a list of pathes: ['path/to/page', 'path/to', 'path']
     *
     * @return string[]
     */
    static function ancestorsAndSelf(string $self): array
    {
        $ancestors = [];
        for ($page = $self; $page; $page = static::getParent($page)) {
            $ancestors[] = $page;
        }

        if (static::hasSite($self)) {
            $root = static::root($self);
            if ($self != $root) {
                $ancestors[] = $root;
            }
        }

        return $ancestors;
    }

    /**
     * creates a list of pathes:
     * site:path/to/page -> ['site:path/to/page', 'site:path/to/page*', 'site:path/to/*', 'site:path/to*', 'site:path/*', 'site:path*', 'site:*', '*']
     *
     * @return string[]
     */
    static function routeCandidates(string $page, string $suffix = '*'): array
    {
        $site = static::root($page);

        $pages = [];

        if (strrpos($page, $suffix, -strlen($suffix)) !== false) {
            if ($page != $suffix && $page[strlen($page) - strlen($suffix) - 1] != '/') {
                // xxx* -> xxx*
                // xxx/* -> none
                // * -> none
                if ($page != "{$site}{$suffix}") {
                    $pages[] = $page;
                }
            }
        } else {
            if (!empty($page)) {
                // xxx -> xxx, xxx*
                // none -> none
                $pages[] = $page;
                if ($page != $site) {
                    $pages[] = "{$page}{$suffix}";
                }
            }
        }

        while ($page = static::getParent($page)) {
            $pages[] = "{$page}/{$suffix}";
            $pages[] = "{$page}{$suffix}";
        }

        if (!empty($site)) {
            $pages[] = "{$site}{$suffix}";
        }

        $pages[] = $suffix;

        return $pages;
    }
}

<?php declare(strict_types=1);

namespace Di;

abstract class ReflectionHelper
{
    /** @return null|string null or class or interface name */
    static function getTypeClassNameOrNull(?\ReflectionType $type): ?string
    {
        if ($type instanceof \ReflectionNamedType) {
            return $type->isBuiltin() ? null : $type->getName();
        }

        if ($type instanceof \ReflectionUnionType || $type instanceof \ReflectionIntersectionType) {
            foreach ($type->getTypes() as $subtype) {
                $name = static::getTypeClassNameOrNull($subtype);
                if (!is_null($name)) {
                    return $name;
                }
            }
        }

        return null;
    }

    /** @return string class or interface */
    static function getReflectionPropertyClassName(\ReflectionProperty $property): string
    {
        $propertyClassName = static::getTypeClassNameOrNull($property->getType());

        if (is_null($propertyClassName)) {
            $class = $property->getDeclaringClass();
            $className = $class->getName();
            $propertyName = $property->getName();
            throw new NotFoundException("Invalid typed property {$className}::{$propertyName}", 404);
        }

        return $propertyClassName;
    }

    static function getPropertyClassName(object $instance, string $propertyName): string
    {
        $property = new \ReflectionProperty($instance, $propertyName);
        return static::getReflectionPropertyClassName($property);
    }

    static function isInternalClass(\ReflectionClass $class): bool
    {
        for (; $class; $class = $class->getParentClass()) {
            if ($class->isInternal()) {
                return true;
            }
        }

        return false;
    }

    static function initFromParent(object $instance, object $parentInstance)
    {
        assert ($instance instanceof $parentInstance);

        $class = new \ReflectionObject($parentInstance);
        for (; $class; $class = $class->getParentClass()) {
            $properties = $class->getProperties();
            foreach ($properties as $property) {
                /** @var \ReflectionProperty $property */
                if (
                    $property->isInitialized($parentInstance)
                    && !$property->isInitialized($instance)
                ) {
                    $value = $property->getValue($parentInstance);
                    $property->setValue($instance, $value);
                }
            }
        }

        return $instance;
    }

}

<?php declare(strict_types=1);

namespace Site\Admin\Action;

class PageGoto extends Action
{
    function __invoke()
    {
        $pageName = $this->postData['page'] ?? $this->postData['layout'];
        $this->currentPage->setRedirectAdminPage($pageName);
    }
}

<?php declare(strict_types=1);

namespace Core\Page;

class Validate
{
    function __construct(
        protected XmlWriter $xml
    ) {
    }

    function validateString($string)
    {
        $use_errors = libxml_use_internal_errors(true);

        $this->xml->xmlPage($string);
        $errors = $this->errors($string);

        libxml_use_internal_errors($use_errors);
        return $errors;
    }

    function validateXml($xmlString)
    {
        $use_errors = libxml_use_internal_errors(true);

        $this->xml->xmlRaw($xmlString);
        $errors = $this->errors($xmlString);

        libxml_use_internal_errors($use_errors);
        return $errors;
    }

    protected function errors($content)
    {
        $message = '';

        foreach (libxml_get_errors() as $error) {
            $lines = explode("\n", $content);
            $line = $lines[($error->line) - 2] ?? '';
            $file = empty($error->file) ? '' : "{$error->file}:";
            $message .= "{$error->message}: {$file}{$line}\n";
        }
        libxml_clear_errors();

        return $message;
    }
}

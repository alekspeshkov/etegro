<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:a="page"
    xmlns:h="helper"
    exclude-result-prefixes="a h"
>
<xsl:import href='stylesheet://etegro'/>

<xsl:template name="compare">
    <xsl:param name="compare"/>
    <div class="pictures">
        <xsl:variable name="alias">
            <xsl:call-template name="basename"><xsl:with-param name="path" select="url"/></xsl:call-template>
        </xsl:variable>

        <xsl:for-each select="h:value('model_pictures',$alias)/value/element[1]">
            <xsl:call-template name="picture"/>
        </xsl:for-each>

        <h2>Сравните цены:</h2>
        <ul>
        <xsl:for-each select="h:pages('path_models_pages',$compare)/pages/page">
            <xsl:variable name="category">
                <xsl:call-template name="dirname"><xsl:with-param name="path" select="path"/></xsl:call-template>
            </xsl:variable>
            <li>
            <xsl:if test="not(starts-with(preceding-sibling::*[1]/path, $category))">
                <xsl:attribute name="class">break</xsl:attribute>
            </xsl:if>
            <xsl:call-template name="html-a-href">
                <xsl:with-param name="href" select="concat(path,'/buy')"/>
                <xsl:with-param name="body" select="translate(label,' ','&#160;')"/>
            </xsl:call-template>
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template name="picture">
    <xsl:variable name="src">
        <xsl:call-template name="url"><xsl:with-param name="href" select="text()"/></xsl:call-template>
    </xsl:variable>

    <xsl:variable name="big">
        <xsl:call-template name="dirname"><xsl:with-param name="path" select="text()"/></xsl:call-template>
        <xsl:text>/big/</xsl:text>
        <xsl:call-template name="basename"><xsl:with-param name="path" select="text()"/></xsl:call-template>
    </xsl:variable>

    <xsl:choose>
        <xsl:when test="h:string('is_file',$big)='true'">
            <xsl:variable name="big-src">
                <xsl:call-template name="url"><xsl:with-param name="href" select="$big"/></xsl:call-template>
            </xsl:variable>

            <a href="{$big-src}" title="увеличить" class="_blank">
                <img src="{$src}" alt="{$content/h1/text()}"/>
            </a>
        </xsl:when>
        <xsl:otherwise>
            <img src="{$src}" alt="{$content/h1/text()}"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="pictures" match="a:pictures">
    <xsl:param name="model">
        <xsl:choose>
            <xsl:when test="@model"><xsl:value-of select="@model"/></xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$name"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:param>
    <div class="pictures">
        <xsl:for-each select="h:value('model_pictures',$model)/value/element">
            <xsl:call-template name="picture"/>
        </xsl:for-each>
    </div>
</xsl:template>

<xsl:template mode="head" match="a:spec">
    <xsl:variable name="title" select="string('Спецификация для')"/>
    <xsl:variable name="parent-content" select="h:content('path_content',$parent)/content"/>
    <title><xsl:value-of select="concat($title, ' ', $parent-content/h1)"/></title>
</xsl:template>
<xsl:template match="a:spec">
    <xsl:variable name="alias">
        <xsl:choose>
            <xsl:when test="@model"><xsl:value-of select="@model"/></xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="basename"><xsl:with-param name="path" select="$parent"/></xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="parent-content" select="h:content('path_content',$parent)/content"/>
    <h1><xsl:value-of select="$parent-content/h1"/></h1>

    <xsl:call-template name="product-tabs">
        <xsl:with-param name="base" select="$parent"/>
    </xsl:call-template>

    <xsl:call-template name="pictures"><xsl:with-param name="model" select="$alias"/></xsl:call-template>
    <div class="text">
        <xsl:call-template name="product-spec"><xsl:with-param name="alias" select="$alias"/></xsl:call-template>
    </div>
</xsl:template>

<xsl:template mode="head" match="a:support">
    <xsl:variable name="title" select="string('Поддержка для')"/>
    <xsl:variable name="parent-content" select="h:content('path_content',$parent)/content"/>
    <title><xsl:value-of select="concat($title, ' ', $parent-content/h1)"/></title>
    <meta name="robots" content="nofollow"/>
</xsl:template>
<xsl:template match="a:support">
    <xsl:variable name="alias">
        <xsl:choose>
            <xsl:when test="@model"><xsl:value-of select="@model"/></xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="basename"><xsl:with-param name="path" select="$parent"/></xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="parent-content" select="h:content('path_content',$parent)/content"/>
    <h1><xsl:value-of select="$parent-content/h1"/></h1>

    <xsl:call-template name="product-tabs">
        <xsl:with-param name="base" select="$parent"/>
    </xsl:call-template>

    <xsl:call-template name="pictures"><xsl:with-param name="model" select="$alias"/></xsl:call-template>
    <div class="text">
        <xsl:apply-templates select="h:content('product_support_content',$alias)/content"/>
    </div>
</xsl:template>

<xsl:template match="a:spec-summary[@model]">
    <xsl:variable name="price" select="@price"/>
    <xsl:variable name="spec" select="h:raw_value('spec_summary',$path)/value/element"/>
    <table class="spec">
        <tr><th rowspan="2"><xsl:value-of select="@model"/></th>
            <xsl:for-each select="$spec[2]/element"><xsl:variable name="n" select="position()"/>
            <td>
                <xsl:variable name="pictures" select="h:value('model_pictures',.)/value/element"/>
                <xsl:variable name="src">
                    <xsl:call-template name="url">
                        <xsl:with-param name="href">
                            <xsl:choose>
                                <!-- skip image with '-' in the filename -->
                                <xsl:when test="contains($pictures[1], '-')">
                                    <xsl:value-of select="$pictures[2]"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$pictures[1]"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:with-param>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:call-template name="html-a-href">
                    <xsl:with-param name="href" select="$spec[1]/element[$n]"/>
                    <xsl:with-param name="body">
                        <img
                            alt="{$spec[3]/element[$n]}"
                            src="{$src}"
                        />
                    </xsl:with-param>
                </xsl:call-template>
            </td>
            </xsl:for-each>
        </tr>

        <tr>
            <xsl:for-each select="$spec[3]/element"><xsl:variable name="n" select="position()"/>
            <td>
                <xsl:call-template name="html-a-href">
                    <xsl:with-param name="href" select="$spec[1]/element[$n]"/>
                    <xsl:with-param name="body" select="translate(.,' ','&#160;')"/>
                </xsl:call-template>
            </td>
            </xsl:for-each>
        </tr>

        <xsl:if test="$price and $spec[4]/element[text()]">
            <tr>
                <th><xsl:value-of select="$price"/></th>
                <xsl:for-each select="$spec[4]/element"><xsl:variable name="n" select="position()"/>
                <td>
                    <xsl:call-template name="html-a-href">
                        <xsl:with-param name="href" select="concat($spec[1]/element[$n],'/buy')"/>
                        <xsl:with-param name="body" select="format-number(text(), '#&#160;##0', 'rub')"/>
                    </xsl:call-template>
                </td>
                </xsl:for-each>
            </tr>
        </xsl:if>

        <xsl:for-each select="$spec[position()>4]">
            <tr>
                <xsl:for-each select="name"><th><xsl:apply-templates/></th></xsl:for-each>
                <xsl:for-each select="element"><td><xsl:apply-templates/></td></xsl:for-each>
            </tr>
        </xsl:for-each>

        <xsl:apply-templates select="tr"/>
    </table>

    <xsl:call-template name="disclaimer"/>
</xsl:template>

</xsl:stylesheet>

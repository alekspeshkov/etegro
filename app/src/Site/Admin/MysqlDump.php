<?php declare(strict_types=1);

namespace Site\Admin;

use Common\Db\Connection as Db;
use Common\Db\Table;

/** MySQL specific batch SQL processing */
class MysqlDump
{
    function __construct(
        protected Db $db,
        protected Table $table
    ) {
    }

    function backup($tables = [])
    {
        $selected_tables = $this->db->fetchCol('SHOW TABLES');

        if (!empty($tables)) {
            $selected_tables = array_intersect($tables, $selected_tables);
        }

        $data = '';
        foreach ($selected_tables as $table) {
            $data .= $this->dumpTable($table);
        }
        return $data;
    }

    protected function dumpTable($table)
    {
        $EOLN = PHP_EOL;
        $SEMI = ";{$EOLN}";

        $data = '';
        $data .= $this->db->quoteInto('DROP TABLE IF EXISTS ~', [$table]) . $SEMI;
        $data .= $this->describeTable($table) . $SEMI;

        $order = $this->orderBy($table);

        $values = $this->table->from($table)->orderFunc($order)->fetchAll();
        foreach ($values as $row) {
            $data .= $this->table->from($table)->prepare_insert($row) . $SEMI;
        }

        $data .= $EOLN;
        return $data;
    }

    protected function describeTable($table)
    {
        $EOLN = PHP_EOL;
        $COMMA = ",{$EOLN}";

        $cols = implode($COMMA, $this->describeCols($table));
        $keys = $this->describeKeys($table);
        $def = empty($keys) ? $cols : "$cols$COMMA$keys";
        return $this->db->quoteInto("CREATE TABLE ~ ({$EOLN}{$def}{$EOLN})", [$table]);
    }

    protected function describeCols($table)
    {
        $cols = [];
        foreach ($this->db->fetchAll('DESCRIBE ~', [$table]) as $c) {
            $col  = $this->db->quoteInto('~', [$c['Field']]) . " {$c['Type']}";

            if ($c['Null'] !== 'YES') {
                $col .= ' NOT NULL';
            }

            $default_generated = false;
            if (!empty($c['Extra'])) {
                $extra = $c['Extra'];

                $pos = strpos($extra, 'DEFAULT_GENERATED');
                if ($pos !== false) {
                    $extra  =
                        substr($extra, 0, $pos)
                        . substr($extra, $pos + strlen('DEFAULT_GENERATED'));
                    $default_generated = true;
                }
                $col .= " {$extra}";
            }

            if (!empty($c['Default'])) {
                $col .= ' DEFAULT ';
                if ($default_generated) {
                    $col .= $c['Default'];
                } else {
                    $col .= $this->db->quote($c['Default']);
                }
            }

            $cols[] = "    {$col}";
        }
        return $cols;
    }

    protected function describeKeys($table)
    {
        $EOLN = PHP_EOL;
        $COMMA = ",{$EOLN}";

        $keys = $this->collectKeys($table);
        foreach ($keys as $name => $columns) {
            $keys[$name] = "    {$name} ({$columns})";
        }
        $keys = implode($COMMA, $keys);
        return $keys;
    }

    protected function collectKeys($table)
    {
        $keys = [];
        foreach ($this->db->fetchAll("SHOW INDEXES FROM ~", [$table]) as $i) {
            $key_name = $i['Key_name'];
            if ('PRIMARY' === $key_name) {
                $name = 'PRIMARY KEY';
            } else {
                $key = $i['Non_unique'] ? 'KEY ~' : 'UNIQUE ~';
                $name = $this->db->quoteInto($key, [$key_name]);
            }
            $keys[$name][$i['Seq_in_index']] = $i['Column_name'];
        }
        foreach ($keys as $name => $columns) {
            ksort($columns, SORT_NUMERIC);
            $keys[$name] = $this->db->quoteInto('~', [$columns]);
        }
        return $keys;
    }

    protected function orderBy($table)
    {
        $order = '';
        $keys = $this->collectKeys($table);
        foreach ($keys as $name => $columns) {
            if ($name === 'PRIMARY KEY') {
                $order = $columns;
                break;
            }
            if (strpos($name, 'UNIQUE ') === 0) {
                $order = $columns;
            }
            if (empty($order)) {
                $order = $columns;
            }
        }
        return $order;
    }
}

<?php declare(strict_types=1);

namespace Site;

use Core\LoggerInterface;
use Monolog\Handler\StreamHandler;

class Logger extends \Monolog\Logger implements LoggerInterface
{
    function __construct(string $name, string $file)
    {
        parent::__construct($name);
        $this->pushHandler(new StreamHandler($file));
    }
}

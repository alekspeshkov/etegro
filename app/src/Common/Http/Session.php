<?php declare(strict_types=1);

namespace Common\Http;

use Common\ArrayAccessExt;

class Session implements \ArrayAccess
{
    use ArrayAccessExt;

    protected array $cookieParams = [
        'lifetime' => 12 * 60 * 60, //12 hours
        'path' => '/', //$this->request->getBasePath();
        'domain' => '',
        'secure' => false,
        'httponly' => true,
    ];

    function __construct(
        protected Request $request,
        protected CookieData $cookieData,
        protected SessionData $sessionData,
        protected \SessionHandlerInterface $sessionHandler
    ) {
        $this->start();
    }

    function offsetGet($offset): mixed
    {
        return $this->sessionData[$offset];
    }

    function offsetSet($offset, $value): void
    {
        $this->set($offset, $value);
    }

    protected function set($offset, $value): bool
    {
        if ($this->sessionData[$offset] === $value) {
            return false;
        }

        $this->updateAge();
        $this->sessionData[$offset] = $value;
        return true;
    }

    protected function updateAge()
    {
        $newAge = strtr(base64_encode(random_bytes(6)), '+/', '-_');
        $this->sessionData['age'] = $newAge;
        $this->cookieData['age'] = $newAge;
    }

    function getAge(): string
    {
        if (!isset($this->sessionData['age'])) {
            $this->updateAge();
        }
        return $this->sessionData['age'];
    }

    function getSessionId()
    {
        return session_id();
    }

    protected function start()
    {
        if (session_status() !== PHP_SESSION_NONE) {
            return;
        }

        $sessionName = session_name();
        $hasSessionCookie = isset($this->cookieData[$sessionName]);

        if (!$hasSessionCookie && !$this->request->isPost()) {
            // do not create a new empty session, unless something is posted
            return;
        }

        $this->setSessionParams();

        if ($hasSessionCookie) {
            session_id($this->cookieData[$sessionName]);
        }

        session_start();

        $this->cookieData[$sessionName] = session_id();
    }

    function destroy()
    {
        $this->sessionData->clear();
        $this->cookieData->clear();

        session_destroy();
    }

    protected function setSessionParams()
    {
        ini_set('session.gc_maxlifetime', $this->cookieParams['lifetime']);
        session_set_cookie_params($this->cookieParams);

        ini_set('session.use_strict_mode', true);
        ini_set('session.serialize_handler', 'php_serialize');
        session_set_save_handler($this->sessionHandler);

        // we set up cache headers and sesion cookie manually
        session_cache_limiter('');
        ini_set('session.use_cookies', false);
    }
}

<?php declare(strict_types=1);

namespace Core\Action;

use Di\Attribute\Required;
use Di\Interface\ContainerInterface;

trait InvokeAction
{
    #[Required] protected ContainerInterface $container;

    function invokeAction(string $className, #[\SensitiveParameter] array $params = []): bool
    {
        $instance = $this->container->get($className);
        assert ($instance instanceof ActionInterface);
        return $this->container->invoke($instance, null, $params) ?? true;
    }
}

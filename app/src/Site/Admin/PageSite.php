<?php declare(strict_types=1);

namespace Site\Admin;

use Common\Db\Table;
use Core\Page\PageNaming;

class PageSite
{
    static string $tableName = 'page_site';
    protected readonly Table $theTable;

    function __construct(Table $table)
    {
        $this->theTable = $table->from(static::$tableName);
    }

    function getViewUrl(string $page)
    {
        $root = PageNaming::root($page);
        $base = $this->theTable->where(['page' => $root])->fetchOne('base');
        $path = PageNaming::pagePath($page);
        return "{$base}{$path}";
    }

    function getAll()
    {
        return $this->theTable->order(['rank', 'label', 'page'])->fetchAll();
    }
}

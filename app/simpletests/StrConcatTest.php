<?php declare(strict_types=1);

namespace test;

use UnitTestCase;
use Common\Str;

class StrConcatTest extends UnitTestCase
{
    function test() {
        $testCases = [
            [[], ''],
            [[''], ''],
            [['', ''], ''],
            [['', '', ''], ''],
            [['a'], 'a'],
            [['a', 'b'], 'a/b'],
            [['a', 'b', 'c'], 'a/b/c'],
            [['a', 'b', 'c', 'd'], 'a/b/c/d'],
            [['//a//'], '//a//'],
            [['//a//', '//b//'], '//a/b//'],
            [['//a//', '//b//', '//c//'], '//a/b/c//'],
            [['//a//', '//b//', '//c//', '//d//'], '//a/b/c/d//'],
            [['//a//', '//', '//b//'], '//a/b//'],
            [['//a//', '//', '//', '//b//'], '//a/b//'],
            [['//a//', '', '//b//'], '//a/b//'],
            [['//a//', '', '', '//b//'], '//a/b//'],
        ];

        foreach ($testCases as $i => $case) {
            $this->assertIdentical(
                Str::concat($case[0], '/'),
                $case[1],
                $i
            );
        }

    }

}

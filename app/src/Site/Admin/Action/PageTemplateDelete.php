<?php declare(strict_types=1);

namespace Site\Admin\Action;

use Core\Page\PageRouting;

class PageTemplateDelete extends Action
{
    function __invoke(PageRouting $pageRouting)
    {
        $page = $this->postData['page'];

        if (empty($page) || !$pageRouting->exists('template', $page)) {
            return false;
        }

        try {
            $pageRouting->delete('template', $page);
            $this->currentPage->flashData['page_template_deleted'] = true;
        } catch (\Throwable $e) {
            $this->currentPage->flashData['database'] = $e->getMessage();
            return false;
        }
    }
}

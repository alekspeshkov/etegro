<?xml version="1.0"?>
<stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform"
    xmlns:func="http://exslt.org/functions"
    xmlns:php="http://php.net/xsl"
    extension-element-prefixes="func php"

    xmlns:h="helper"
>
<param name="callbackFunction"/>

<func:function name="h:string">
    <param name="func"/>
    <param name="param" select="string('')"/>
    <func:result select="php:functionString($callbackFunction, '_str', string($func), string($param))"/>
</func:function>

<func:function name="h:nodeset">
    <param name="func"/>
    <param name="param" select="string('')"/>
    <func:result select="php:function($callbackFunction, '_nodeset', string($func), string($param))"/>
</func:function>

<func:function name="h:raw_value">
    <param name="func"/>
    <param name="param" select="string('')"/>
    <func:result select="php:function($callbackFunction, '_raw_value', string($func), string($param))"/>
</func:function>

<func:function name="h:value">
    <param name="func"/>
    <param name="param" select="string('')"/>
    <func:result select="php:function($callbackFunction, '_value', string($func), string($param))"/>
</func:function>

</stylesheet>

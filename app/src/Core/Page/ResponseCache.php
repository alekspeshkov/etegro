<?php declare(strict_types=1);

namespace Core\Page;

use Common\File;
use Common\Str;
use Di\Attribute\Required;

/** Caching HTTP response into static files */
class ResponseCache
{
    #[Required] protected string $storageDir;
    protected string $suffix = '.html';

    protected readonly string $filename;

    function __construct(string $path)
    {
        $filePath = ((strlen($path) == 0) ? 'index' : $path) . $this->suffix;
        $dirs = explode('/', $filePath);
        array_unshift($dirs, $this->storageDir);
        $this->filename = Str::concatDir($dirs);
    }

    function write(string $content)
    {
        File::updateSafe($this->filename, $content);
    }

    function clear()
    {
        $this->write('');
    }

    function clearAll()
    {
        exec("rm -rf {$this->storageDir}");
    }
}

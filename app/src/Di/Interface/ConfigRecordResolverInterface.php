<?php declare(strict_types=1);

namespace Di\Interface;

use Di\Interface\ConfigRecordInterface;

interface ConfigRecordResolverInterface
{
    function resolve(string $serviceName, #[\SensitiveParameter] array $params = []): ConfigRecordInterface;

    function resolveFrom(ConfigRecordInterface $record): ConfigRecordInterface;

    function resolveOnce(ConfigRecordInterface &$record): bool;
}

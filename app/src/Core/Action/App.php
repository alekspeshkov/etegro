<?php declare(strict_types=1);

namespace Core\Action;

use Common\Http\Request;
use Di\Attribute\Required;
use Di\Interface\AppConfigInterface;
use Di\Interface\ConfigRecordInterface;
use Di\ConfigRecord;

abstract class App implements ActionInterface
{
    use InvokeAction;

    /** application configuration */
    #[Required] protected AppConfigInterface $appConfig;
    #[Required] protected Request $request;

    protected function __construct()
    {
        $this->appConfig->configure([
            ConfigRecordInterface::class => ConfigRecord::class,
            \SessionHandlerInterface::class => SessionHandler::class,
            ActionRouter::class => ['actionField' => 'action'],
        ]);
    }

    function __invoke()
    {
        return $this->request->isPost() ? $this->routeMethodPost() : $this->routeMethodGet();
    }

    abstract protected function routeMethodGet();

    protected function routeMethodPost()
    {
        return $this->invokeAction(ActionRouter::class);
    }
}

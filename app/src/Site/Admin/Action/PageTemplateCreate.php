<?php declare(strict_types=1);

namespace Site\Admin\Action;

use Core\Page\PageRouting;

class PageTemplateCreate extends Action
{
    function __invoke(PageRouting $pageRouting)
    {
        $postData = $this->postData->getAll();

        if (empty($postData['template']) || empty($postData['page'])) {
            return false;
        }

        $page = $postData['page'];
        $template = $postData['template'];
        try {
            if ($pageRouting->exists('template', $page)) {
                $pageRouting->delete('template', $page);
                $pageRouting->insert('template', $page, $template);
                $this->currentPage->flashData['page_template_updated'] = true;
            } else {
                $pageRouting->insert('template', $page, $template);
                $this->currentPage->flashData['page_template_created'] = true;
            }
        } catch (\Throwable $e) {
            $this->currentPage->flashData['database'] = $e->getMessage();
            return false;
        }
    }
}

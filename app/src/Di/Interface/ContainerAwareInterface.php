<?php declare(strict_types=1);

namespace Di\Interface;

interface ContainerAwareInterface
{
    function setContainer(ContainerInterface $container);
}

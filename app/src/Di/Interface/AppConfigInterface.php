<?php declare(strict_types=1);

namespace Di\Interface;

interface AppConfigInterface extends ConfigInterface, ConfigRecordResolverInterface
{
}

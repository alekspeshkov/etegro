<?xml version="1.0"?>
<stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform"
    xmlns:func="http://exslt.org/functions"
    xmlns:php="http://php.net/xsl"
    extension-element-prefixes="func php"

    xmlns:h="helper"
>
<import href='stylesheet://helper'/>

<param name="callbackFunction"/>

<func:function name="h:content">
    <param name="func"/>
    <param name="param" select="string('')"/>
    <func:result select="php:function($callbackFunction, '_content', string($func), string($param))"/>
</func:function>

<func:function name="h:pages">
    <param name="func"/>
    <param name="param" select="string('')"/>
    <func:result select="php:function($callbackFunction, '_pages', string($func), string($param))"/>
</func:function>

<func:function name="h:form">
    <param name="name"/>
    <func:result select="php:functionString($callbackFunction, '_str', 'form', string($name))"/>
</func:function>

</stylesheet>

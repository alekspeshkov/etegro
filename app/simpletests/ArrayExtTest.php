<?php declare(strict_types=1);

namespace test;

use UnitTestCase;
use Common\ArraySymlink;

class ArrayExtTest extends UnitTestCase
{
    protected $a;
    protected $var = '_Test_Core_Array';

    function setUp() {
        $this->a = new ArraySymlink("\$GLOBALS['{$this->var}']");
    }

    function test() {
        $var = $this->var;

        $this->assertFalse( isset($GLOBALS[$var]) );
        $this->assertFalse( isset($this->a[null]) );

        $this->assertIdentical( $this->a[null], null );

        $this->a[null] = [];
        $this->assertIdentical($this->a[null], [] );
        $this->assertTrue( isset($this->a[null]) );
        $this->assertTrue( isset($GLOBALS[$var]) );

        $i = 1;
        $j2 = "\t\$wo']\n'";
        $j3 = 'apple';
        $v2 = 2.0;
        $v3 = 3;
        $this->a[[$i,$j2]] = $v2;
        $this->a[[$i,$j2]] = $v2;
        $this->assertIdentical($this->a[null], $this->a[null]);
        $this->assertIdentical($GLOBALS[$var], $this->a[null]);

        $this->a[[$i,$j3]] = $v3;
        $this->assertIdentical([$i=>[$j2=>$v2, $j3=>$v3]], $this->a[null] );
        $this->assertIdentical([$j2=>$v2, $j3=>$v3], $this->a[$i] );
        $this->assertIdentical($v2, $this->a[[$i,$j2]] );

        // normalizing array
        $this->a[[$i,$j2]] = null;
        $this->assertTrue(isset($GLOBALS[$var]) );

        $this->a[[$i,$j3]] = null;
        $this->assertIdentical( $this->a[null], null );
        $this->assertFalse( isset($this->a[null]) );
        $this->assertFalse( isset($GLOBALS[$var]) );

        $this->a[null] = null;
        $this->assertIdentical( $this->a[null], null );
        $this->assertFalse( isset($GLOBALS[$var]) );
    }

}

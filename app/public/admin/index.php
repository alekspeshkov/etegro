<?php declare(strict_types=1);
require __DIR__ . '/../../src/autoload.php';

\Di\Bootstrap::invokeClass(Site\Admin\App::class);

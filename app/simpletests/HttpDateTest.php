<?php declare(strict_types=1);

namespace test;

use UnitTestCase;
use Common\Http\Date;

class HttpDateTest extends UnitTestCase
{
    function test__constructClone() {
        $date1 = new Date();
        $date2 = new Date($date1);
        $this->assertClone($date1, $date2);

        $timestamp = $date2->getTimestamp();
        $date3 = Date::createFromTimestamp($timestamp);
        $this->assertClone($date2, $date3);
        $this->assertIdentical((string)$date2, (string)$date3);

        $string = (string)$date3;
        $date4 = Date::createFromHeader($string);
        $this->assertClone($date3, $date4);
        $this->assertIdentical($date3->getTimestamp(), $date4->getTimestamp());
    }

    function test_stringFormat() {
        $date1 = Date::createFromHeader('Thu, 01 Jan 1970 00:00:12 GMT');
        $this->assertIdentical($date1->getTimestamp(), 12);

        $date2 = Date::createFromTimestamp(60);
        $this->assertIdentical($date2->__toString(), 'Thu, 01 Jan 1970 00:01:00 GMT');
    }

    function test_createFromHeader_exception() {
        $this->expectException();
        $date = Date::createFromHeader('01.01.1970');
    }

}

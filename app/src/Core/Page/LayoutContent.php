<?php declare(strict_types=1);

namespace Core\Page;

use Common\Db\Table;

class LayoutContent
{
    static string $tableName = 'layout_content';
    protected readonly Table $theTable;

    function __construct(Table $table)
    {
        $this->theTable = $table->from(static::$tableName);
    }

    function getAll()
    {
        return $this->theTable
            ->leftJoin('type_route_page', 'page', 'layout')->where(['type' => 'layout'])
            ->order(['layout', 'route'])->fetchAll(['route' => 'page'], 'layout');
    }

    function exists(string $name): bool
    {
        return $this->theTable->where(['layout' => $name])->exists();
    }

    function fetch(string $name): string
    {
        return $this->theTable->where(['layout' => $name])->fetchOne('content');
    }

    function save(string $name, string $content)
    {
        if (strlen($content) == 0) {
            return $this->delete($name);
        }

        $this->theTable->replace(['layout' => $name, 'content' => $content]);
    }

    function delete(string $name)
    {
        $this->theTable->delete(['layout' => $name]);
    }
}

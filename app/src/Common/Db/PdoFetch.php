<?php declare(strict_types=1);

namespace Common\Db;

trait PdoFetch
{
    /** @var \PDO $this */

    /** @method \PDOStatement query() query(string $sql, ?int $fetchMode = null, ...$fetchModeArgs) */

    /** @method string quoteInto() quoteInto(string $sql, array $args = []) */

    protected function queryInto(string $sql, array $args = []): \PDOStatement
    {
        $safeSql = $this->quoteInto($sql, $args);
        return $this->query($safeSql);
    }

    protected function queryRow(string $sql, array $args = [])
    {
        $pdoStatement = $this->queryInto($sql, $args);

        /** @var false|array<string, mixed> */
        $data = $pdoStatement->fetch(\PDO::FETCH_NAMED);

        $pdoStatement->closeCursor();

        return $data;
    }

    protected function queryOne(string $sql, array $args = [])
    {
        $pdoStatement = $this->queryInto($sql, $args);

        /** @var false|array<string, mixed> */
        $data = $pdoStatement->fetch(\PDO::FETCH_NUM);

        $pdoStatement->closeCursor();

        return $data;
    }

    /** @return array<string, mixed>[] */
    function fetchAll(string $sql, array $args = []): array
    {
        return $this->queryInto($sql, $args)->fetchAll(\PDO::FETCH_NAMED);
    }

    /** @return mixed[] */
    function fetchCol(string $sql, array $args = []): array
    {
        return $this->queryInto($sql, $args)->fetchAll(\PDO::FETCH_COLUMN);
    }

    function fetchPairs(string $sql, array $args = []): array
    {
        return $this->queryInto($sql, $args)->fetchAll(\PDO::FETCH_KEY_PAIR);
    }

    function fetchPairsMany(string $sql, array $args = []): array
    {
        return $this->queryInto($sql, $args)->fetchAll(\PDO::FETCH_COLUMN | \PDO::FETCH_GROUP);
    }

    /** @return array<string, mixed> */
    function fetchRow(string $sql, array $args = []): array
    {
        $data = $this->queryRow($sql, $args);

        if ($data === false) {
            throw new NotFoundException('Invalid empty result', 404);
        }

        return $data;
    }

    /** @return null|array<string, mixed> */
    function fetchRowOrNull(string $sql, array $args = []): ?array
    {
        $data = $this->queryRow($sql, $args);

        if ($data === false) {
            return null;
        }

        return $data;
    }

    function fetchOne(string $sql, array $args = []): mixed
    {
        $data = $this->queryOne($sql, $args);

        if ($data === false) {
            throw new NotFoundException('Invalid empty result', 404);
        }

        return $data[0];
    }

    function fetchOneOrNull(string $sql, array $args = []): mixed
    {
        $data = $this->queryOne($sql, $args);

        if ($data === false) {
            return null;
        }

        return $data[0];
    }

}

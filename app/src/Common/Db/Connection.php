<?php declare(strict_types=1);

namespace Common\Db;

class Connection extends \PDO
{
    use PdoFetch;
    use PdoQuoteInto;
}

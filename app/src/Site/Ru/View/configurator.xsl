<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:a="page"
    xmlns:h="helper"
    exclude-result-prefixes="a h"
>
<xsl:import href='stylesheet://product'/>

<xsl:template mode="head" match="a:model">
    <xsl:variable name="type" select="@type"/>
    <xsl:variable name="model" select="h:value('product_meta',$path)/value"/>
    <xsl:if test="not($model)"><xsl:call-template name="not-found-head"/></xsl:if>
    <xsl:for-each select="$model">
        <xsl:variable name="brand-name" select="concat('ETegro ', name)"/>
        <xsl:choose>
            <xsl:when test="suffix=''">
                <title>Описание <xsl:value-of select="$brand-name"/></title>
            </xsl:when>
            <xsl:when test="suffix='spec'">
                <title>Cпецификация <xsl:value-of select="$brand-name"/></title>
            </xsl:when>
            <xsl:when test="suffix='buy' and $type='buy'">
                <script src="/static/configurator/prototype.js"  type="text/javascript" ></script>
                <script src="/static/configurator/configurator.js" type="text/javascript"></script>
                <script src="/wizard/jsi18n/" type="text/javascript"></script>
                <title>Конфигуратор <xsl:value-of select="$brand-name"/></title>
                <!--<meta name="robots" content="noindex"/>-->
            </xsl:when>
            <xsl:when test="suffix='support'">
                <title>Поддержка <xsl:value-of select="$brand-name"/></title>
                <meta name="robots" content="nofollow"/>
            </xsl:when>
            <xsl:otherwise><xsl:call-template name="not-found-head"/></xsl:otherwise>
        </xsl:choose>
    </xsl:for-each>
</xsl:template>

<xsl:template match="a:model">
    <xsl:variable name="type" select="@type"/>
    <xsl:variable name="compare" select="@compare"/>
    <xsl:variable name="model" select="h:value('product_meta',$path)/value"/>
    <xsl:if test="not($model)"><xsl:call-template name="not-found-body"/></xsl:if>
    <xsl:for-each select="$model">
        <xsl:choose>
            <xsl:when test="suffix='' or suffix='spec' or suffix='support' or (suffix='buy' and $type='buy')">
                <xsl:variable name="alias" select="model"/>
                <xsl:variable name="brand-name" select="concat('ETegro ', name)"/>
                <h1><xsl:value-of select="$brand-name"/></h1>

                <xsl:call-template name="product-tabs">
                    <xsl:with-param name="base" select="path"/>
                    <xsl:with-param name="type" select="$type"/>
                </xsl:call-template>

                <xsl:choose>
                    <xsl:when test="suffix='buy' and $compare">
                        <xsl:call-template name="compare"><xsl:with-param name="compare" select="$compare"/></xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="pictures"><xsl:with-param name="model" select="$alias"/></xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>

                <div class="text">
                    <xsl:choose>
                        <xsl:when test="suffix=''">
                            <xsl:apply-templates select="h:content('product_description_content',$alias)/content"/>
                            <xsl:call-template name="disclaimer"/>
                        </xsl:when>
                        <xsl:when test="suffix='support'">
                            <xsl:apply-templates select="h:content('product_support_content',$alias)/content"/>
                        </xsl:when>
                        <xsl:when test="suffix='spec'">
                            <xsl:call-template name="product-spec"><xsl:with-param name="alias" select="$alias"/></xsl:call-template>
                        </xsl:when>
                        <xsl:when test="suffix='buy'">
                        </xsl:when>
                    </xsl:choose>
                </div>
            </xsl:when>
            <xsl:otherwise><xsl:call-template name="not-found-body"/></xsl:otherwise>
        </xsl:choose>
    </xsl:for-each>
</xsl:template>

<xsl:template match="a:product-tabs" name="product-tabs">
    <xsl:param name="base">
        <xsl:choose>
            <xsl:when test="@base"><xsl:value-of select="@base"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="/page/path/text()"/></xsl:otherwise>
        </xsl:choose>
    </xsl:param>
    <xsl:param name="type">
        <xsl:choose>
            <xsl:when test="@type"><xsl:value-of select="@type"/></xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:param>
    <xsl:param name="desc">
        <xsl:choose>
            <xsl:when test="@desc"><xsl:value-of select="@desc"/></xsl:when>
            <xsl:otherwise>Описание</xsl:otherwise>
        </xsl:choose>
    </xsl:param>
    <xsl:param name="spec">
        <xsl:choose>
            <xsl:when test="@spec"><xsl:value-of select="@spec"/></xsl:when>
            <xsl:otherwise>Спецификация</xsl:otherwise>
        </xsl:choose>
    </xsl:param>
    <xsl:param name="buy">
        <xsl:choose>
            <xsl:when test="@buy"><xsl:value-of select="@buy"/></xsl:when>
            <xsl:otherwise>Конфигуратор</xsl:otherwise>
        </xsl:choose>
    </xsl:param>
    <xsl:param name="support">
        <xsl:choose>
            <xsl:when test="@support"><xsl:value-of select="@support"/></xsl:when>
            <xsl:otherwise>Поддержка</xsl:otherwise>
        </xsl:choose>
    </xsl:param>
    <div class="product-navigation">
        <ul>
        <xsl:if test="$buy and ($type != 'archive')">
            <li>
                <xsl:if test="$path != concat($base,'/buy')">
                    <xsl:attribute name="class">buy</xsl:attribute>
                </xsl:if>
                <xsl:if test="$path = concat($base,'/buy')">
                    <xsl:attribute name="class">selected buy</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="html-a-href">
                    <xsl:with-param name="href" select="concat($base,'/buy')"/>
                    <xsl:with-param name="body" select="$buy"/>
                </xsl:call-template>
            </li>
        </xsl:if>
        <xsl:if test="$desc">
            <li>
                <xsl:if test="$path = $base">
                    <xsl:attribute name="class">selected</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="html-a-href">
                    <xsl:with-param name="href" select="$base"/>
                    <xsl:with-param name="body" select="$desc"/>
                </xsl:call-template>
            </li>
        </xsl:if>
        <xsl:if test="$spec">
            <li>
                <xsl:if test="$path = concat($base,'/spec')">
                    <xsl:attribute name="class">selected</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="html-a-href">
                    <xsl:with-param name="href" select="concat($base,'/spec')"/>
                    <xsl:with-param name="body" select="$spec"/>
                </xsl:call-template>
            </li>
        </xsl:if>
        <xsl:if test="$support">
            <li>
                <xsl:if test="$path = concat($base,'/support')">
                    <xsl:attribute name="class">selected</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="html-a-href">
                    <xsl:with-param name="href" select="concat($base,'/support')"/>
                    <xsl:with-param name="body" select="$support"/>
                </xsl:call-template>
            </li>
        </xsl:if>
        </ul>
    </div>
</xsl:template>

<xsl:template match="a:price-link[@model]">
    <xsl:param name="model">
        <xsl:choose>
            <xsl:when test="@model"><xsl:value-of select="@model"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="$name"/></xsl:otherwise>
        </xsl:choose>
    </xsl:param>
    <xsl:variable name="info" select="h:content('model_price',$model)/content"/>
    <xsl:if test="$info/url">
        <xsl:call-template name="html-a-href">
            <xsl:with-param name="href" select="concat($info/url,'/buy')"/>
            <xsl:with-param name="body" select="format-number($info/default_price, '#&#160;##0', 'rub')"/>
        </xsl:call-template>
    </xsl:if>
</xsl:template>

</xsl:stylesheet>

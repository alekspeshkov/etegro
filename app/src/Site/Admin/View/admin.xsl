<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:a="page"
    xmlns:h="helper"
    exclude-result-prefixes="a h"
>
<xsl:import href='stylesheet://page'/>

<xsl:template match="a:user"><xsl:value-of select="h:string('user')"/></xsl:template>

<xsl:template match="a:top-menu">
    <xsl:call-template name="path-children">
        <xsl:with-param name="base" select="string('')"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="a:breadcrumbs">
    <xsl:variable name="page" select="h:form('page')"/>
    <!-- ancestors -->
    <xsl:call-template name="admin-page-menu">
        <xsl:with-param name="pages" select="h:pages('page_full_ancestors_pages',$page)/pages/page"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="a:page-view">
    <xsl:call-template name="html-a-href">
        <xsl:with-param name="href" select="h:string('page_view_url')"/>
        <xsl:with-param name="body" select="text()"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="a:page-validate">
    <xsl:call-template name="html-a-href">
        <xsl:with-param name="href" select="h:string('page_validate_url')"/>
        <xsl:with-param name="body" select="text()"/>
    </xsl:call-template>
</xsl:template>

<xsl:template name="admin-page-menu">
    <xsl:param name="pages"/>
    <xsl:if test="$pages">
        <ul>
            <xsl:for-each select="$pages">
                <li>
                <xsl:if test="concat('page/',page) = $path">
                    <xsl:attribute name="class">selected</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="html-a-href">
                    <xsl:with-param name="href" select="concat('page/',page)"/>
                </xsl:call-template>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:if>
</xsl:template>

<xsl:template mode="head" match="a:page-refresh">
    <!-- trick to refresh before first use -->
    <xsl:variable name="side-effect" select="h:string('page_refresh')"/>
</xsl:template>
<xsl:template match="a:page-refresh"/>

<xsl:template mode="head" match="a:layout-refresh">
    <!-- trick to refresh before first use -->
    <xsl:variable name="side-effect" select="h:string('layout_refresh')"/>
</xsl:template>
<xsl:template match="a:layout-refresh"/>

<xsl:template match="a:layout-options">
    <xsl:for-each select="h:value('layout_list')/value/element[not( layout = preceding::*/layout )]">
        <option value="{layout}"><xsl:value-of select="layout"/></option>
    </xsl:for-each>
</xsl:template>

<xsl:template match="a:empty-layout-options">
    <xsl:for-each select="h:value('layout_list')/value/element[page='']">
        <option value="{layout}"><xsl:value-of select="layout"/></option>
    </xsl:for-each>
</xsl:template>

<xsl:template match="a:page-layout-options">
    <xsl:for-each select="h:value('layout_list')/value/element[page!='']">
        <option value="{page}"><xsl:value-of select="page"/></option>
    </xsl:for-each>
</xsl:template>

<xsl:template match="a:page-template-options">
    <xsl:for-each select="h:value('template_list')/value/element[page!='']">
        <option value="{page}"><xsl:value-of select="page"/></option>
    </xsl:for-each>
</xsl:template>

<xsl:template match="a:site-list">
    <!-- top level elements -->
    <xsl:call-template name="admin-page-menu">
        <xsl:with-param name="pages" select="h:value('site_list')/value/element"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="a:site-options">
    <xsl:for-each select="h:value('site_list')/value/element">
        <option value="{page}"><xsl:value-of select="page"/></option>
    </xsl:for-each>
</xsl:template>

<xsl:template match="a:site-links">
    <xsl:variable name="current-page" select="h:form('page')"/>
    <xsl:variable name="current-node" select="node()"/>
    <xsl:for-each select="h:value('site_list')/value/element">
        <tr>
        <xsl:choose>
        <xsl:when test="page != $current-page">
            <td>
                <xsl:call-template name="html-a-href">
                    <xsl:with-param name="href" select="concat('page/',page)"/>
                    <xsl:with-param name="body" select="string(page)"/>
                </xsl:call-template>
            </td>
            <td>
                <xsl:call-template name="html-a-href">
                    <xsl:with-param name="href" select="concat('page/',page)"/>
                    <xsl:with-param name="body" select="string(label)"/>
                </xsl:call-template>
            </td>
            <td>
                <xsl:call-template name="html-a-href">
                    <xsl:with-param name="href" select="string(base)"/>
                    <xsl:with-param name="body" select="string(base)"/>
                </xsl:call-template>
            </td>
            <td>
                <xsl:value-of select="string(rank)"/>
            </td>
        </xsl:when>
        <xsl:otherwise>
            <xsl:apply-templates select="$current-node"/>
        </xsl:otherwise>
        </xsl:choose>
        </tr>
    </xsl:for-each>
</xsl:template>

<xsl:template match="a:layout-page-links">
    <xsl:for-each select="h:value('layout_list')/value/element">
        <tr>
        <xsl:if test="not( layout = preceding::*/layout )">
            <td rowspan="{1 + count(following::*/layout[. = current()/layout])}">
                <xsl:call-template name="html-a-href">
                    <xsl:with-param name="href" select="concat('layout/',layout)"/>
                    <xsl:with-param name="body" select="string(layout)"/>
                </xsl:call-template>
            </td>
        </xsl:if>
            <td>
                <xsl:call-template name="html-a-href">
                    <xsl:with-param name="href" select="concat('page/',page)"/>
                    <xsl:with-param name="body" select="string(page)"/>
                </xsl:call-template>
            </td>
        </tr>
    </xsl:for-each>
</xsl:template>

<xsl:template match="a:template-page-links">
    <xsl:for-each select="h:value('template_list')/value/element">
        <tr>
        <xsl:if test="not( template = preceding::*/template )">
            <td rowspan="{1 + count(following::*/template[. = current()/template])}">
                <xsl:value-of select="string(template)"/>
            </td>
        </xsl:if>
            <td>
                <xsl:call-template name="html-a-href">
                    <xsl:with-param name="href" select="concat('page/',page)"/>
                    <xsl:with-param name="body" select="string(page)"/>
                </xsl:call-template>
            </td>
        </tr>
    </xsl:for-each>
</xsl:template>

<xsl:template match="a:backup-daily-url">
    <xsl:call-template name="html-a-href">
        <xsl:with-param name="href" select="h:string('backup_daily_url')"/>
        <xsl:with-param name="body" select="h:string('backup_daily_url')"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="a:backup-latest-url">
    <xsl:call-template name="html-a-href">
        <xsl:with-param name="href" select="h:string('backup_latest_url')"/>
        <xsl:with-param name="body" select="h:string('backup_latest_url')"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="a:navigation">
    <xsl:variable name="page" select="h:form('page')"/>
    <xsl:variable name="parent">
        <xsl:call-template name="dirname"><xsl:with-param name="path" select="$page"/></xsl:call-template>
    </xsl:variable>

    <!-- siblings -->
    <xsl:call-template name="admin-page-menu">
        <xsl:with-param name="pages" select="h:pages('page_children_pages',$parent)/pages/page"/>
    </xsl:call-template>

    <xsl:call-template name="admin-page-menu">
        <xsl:with-param name="pages" select="h:pages('page_hidden_children_pages',$parent)/pages/page"/>
    </xsl:call-template>

    <!-- children -->
    <xsl:call-template name="admin-page-menu">
        <xsl:with-param name="pages" select="h:pages('page_children_pages',$page)/pages/page"/>
    </xsl:call-template>

    <xsl:call-template name="admin-page-menu">
        <xsl:with-param name="pages" select="h:pages('page_hidden_children_pages',$page)/pages/page"/>
    </xsl:call-template>
</xsl:template>

</xsl:stylesheet>

<?php declare(strict_types=1);

namespace Di\Interface;

use Di\Interface\ConfigRecordInterface;

interface ConfigInterface
{
    function createRecord(?string $className, array $params = []): ConfigRecordInterface;

    function get(string $name): ?ConfigRecordInterface;

    function extendFrom(ConfigRecordInterface $record, string $name): ConfigRecordInterface;

    function extendParams(ConfigRecordInterface $record, string $name): ConfigRecordInterface;

    /**
     * possible entry format:
     * 1) 'serviceName' => ClassName::class
     * 2) 'serviceName' => [...params]
     * 3) 'serviceName' => ['::class' => ClassName::class]
     * 4) 'serviceName' => ['::class' => ClassName::class, ...params]
     *
     * @param array<string, array|string> $entries
     */
    function configure(array $entries): static;

    function set(string $name, array|string $params): static;
}

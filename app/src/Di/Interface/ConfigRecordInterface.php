<?php declare(strict_types=1);

namespace Di\Interface;

interface ConfigRecordInterface
{
    static function create(?string $className, array $params = []): static;
    function getName(): string;
    function isNameNull(): bool;
    function toArray(): array;
    function extendFrom(?ConfigRecordInterface $record): static;
    function extendParams(?ConfigRecordInterface $record): static;
    function extendWith(?string $serviceName, array $params = []): static;
    function newEntity(callable $factory);
}

<?php declare(strict_types=1);

namespace Common\Http;

class SessionData extends \Common\ArraySymlink
{
    function __construct()
    {
        parent::__construct('$_SESSION');
    }
}

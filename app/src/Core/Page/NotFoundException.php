<?php declare(strict_types=1);

namespace Core\Page;

class NotFoundException extends \UnderflowException
{
}

<?xml version="1.0"?>
<stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform">

<template name="url">
    <param name="href"/><!--"xxx/yyy/zzz"-->
    <param name="base" select="$path"/><!--"xxx/yyy/www"-->
    <choose>
        <when test="starts-with($href,'#') or starts-with($href,'//') or starts-with($href, 'mailto:') or contains($href,'://') ">
            <!-- as is -->
            <value-of select="$href"/>
        </when>
        <when test="starts-with($href,'/')">
            <!-- absolute url -->
            <value-of select="$href"/>
        </when>
        <when test="substring($base,string-length($base))='/'">
            <!-- first step in case of trailing slash -->
            <call-template name="url">
                <with-param name="href" select="concat('../',$href)"/>
                <with-param name="base" select="substring($base,1,string-length($base)-1)"/>
            </call-template>
        </when>
        <when test="contains($base,'/') and substring-before($base,'/') = substring-before($href,'/')">
            <!-- step reduction -->
            <call-template name="url">
                <with-param name="href" select="substring-after($href,'/')"/>
                <with-param name="base" select="substring-after($base,'/')"/>
            </call-template>
        </when>
        <when test="contains($base,'/') and substring-before($base,'/') != substring-before($href,'/')">
            <!-- step back -->
            <call-template name="url">
                <with-param name="href" select="concat('../',$href)"/>
                <with-param name="base" select="substring-after($base,'/')"/>
            </call-template>
        </when>
        <when test="not(starts-with($href,':')) and contains($href,':')">
            <value-of select="concat('./', $href)"/>
        </when>
        <when test="$href=''">
            <value-of select="string('./')"/>
        </when>
        <otherwise>
            <value-of select="$href"/>
        </otherwise>
    </choose>
</template>

<template name="basename">
    <param name="path" select="string(path)"/>
    <choose>
        <when test="substring-after($path,'/')">
            <call-template name="basename"><with-param name="path" select="substring-after($path,'/')"/></call-template>
        </when>
        <otherwise>
            <value-of select="$path"/>
        </otherwise>
    </choose>
</template>

<template name="dirname">
    <param name="path" select="path"/>
    <variable name="basename">
        <call-template name="basename"><with-param name="path" select="$path"/></call-template>
    </variable>
    <value-of select="substring-before(concat($path,'?'),concat('/',$basename,'?'))"/>
</template>

</stylesheet>

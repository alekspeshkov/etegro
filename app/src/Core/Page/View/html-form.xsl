<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:h="helper"
    exclude-result-prefixes="h"
>

<xsl:template match="input[@name and (not(@type) or @type='text' or @type='hidden')]">
    <xsl:variable name="value" select="h:form(@name)"/>
    <xsl:element name="{local-name()}"><xsl:apply-templates select="@*"/>
        <xsl:if test="$value!=''">
            <xsl:attribute name="value"><xsl:value-of select="$value"/></xsl:attribute>
        </xsl:if>
        <xsl:apply-templates/>
    </xsl:element>
</xsl:template>

<xsl:template match="input[@name and @type='checkbox' and not(@checked)]">
    <xsl:element name="{local-name()}"><xsl:apply-templates select="@*"/>
        <xsl:if test="h:form(@name)='on'">
            <xsl:attribute name="checked">checked</xsl:attribute>
        </xsl:if>
    </xsl:element>
</xsl:template>

<xsl:template match="input[@name and @type='checkbox']/@checked">
    <xsl:variable name="value" select="h:form(@name)"/>
    <xsl:if test="($value='') or ($value='on')"><xsl:copy/></xsl:if>
</xsl:template>

<xsl:template match="input[@name and @type='radio' and not(@checked)]">
    <xsl:element name="{local-name()}"><xsl:apply-templates select="@*"/>
        <xsl:if test="current()/@value = h:form(@name)">
            <xsl:attribute name="checked">checked</xsl:attribute>
        </xsl:if>
    </xsl:element>
</xsl:template>

<xsl:template match="input[@name and @type='radio']/@checked">
    <xsl:variable name="value" select="h:form(@name)"/>
    <xsl:if test="$value='' or ($value = ../@value)"><xsl:copy/></xsl:if>
</xsl:template>

<xsl:template match="select[@name]/option[@value and not(@selected)]">
    <xsl:variable name="value" select="h:form(../@name)"/>
    <xsl:element name="{local-name()}">
        <xsl:if test="$value!='' and $value = @value"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
        <xsl:apply-templates select="@* | node()"/>
    </xsl:element>
</xsl:template>

<xsl:template match="select[@name]/option[@value]/@selected">
    <xsl:variable name="value" select="h:form(../../@name)"/>
    <xsl:if test="($value!='') or ($value = ../@value)"><xsl:copy/></xsl:if>
</xsl:template>

<xsl:template match="textarea[@name]">
    <xsl:variable name="value" select="h:form(@name)"/>
    <xsl:element name="{local-name()}"><xsl:apply-templates select="@*"/>
        <xsl:choose>
            <xsl:when test="$value!=''"><xsl:copy-of select="$value"/></xsl:when>
            <xsl:otherwise><xsl:apply-templates/></xsl:otherwise>
        </xsl:choose>
    </xsl:element>
</xsl:template>

</xsl:stylesheet>

<?php declare(strict_types=1);

namespace Site\Admin\Action;

class DbBackup extends Action
{
    function __invoke(\Site\Admin\Backup $backup)
    {
        $this->currentPage->formData->clear();
        $backup->backup();
    }
}

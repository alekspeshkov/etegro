<?php declare(strict_types=1);

namespace Site\Admin\Action;

use Core\Action\InvokeAction;

use Core\Page\Validate;
use Di\Attribute\Required;

class PageContentUpdate extends Action
{
    use InvokeAction;

    #[Required] protected Validate $validate;

    function __invoke()
    {
        $pageName = $this->postData['page'];
        $action = $this->getPage($pageName)->exists() ? PageContentOverwrite::class : PageContentCreate::class;
        return $this->invokeAction($action);
    }

    protected function validateCurrentPage()
    {
        $isOk = true;
        $page = $this->currentPage;

        if (empty($page->formData['page'])) {
            $page->flashData['page_empty'] = true;
            $isOk = false;
        }

        if (empty($page->formData['content'])) {
            $page->flashData['content_empty'] = true;
            $isOk = false;
        }

        foreach ($page->formData->getAll() as $field => $value) {
            $errorMsg = $this->validate->validateString($value);
            if (!empty($errorMsg)) {
                $page->flashData[$field] = $errorMsg;
                $isOk = false;
            }
        }

        return $isOk;
    }

}

<?php declare(strict_types=1);

namespace Di\Interface;

interface ContainerInterface extends \Psr\Container\ContainerInterface
{
    /** resolve service configuration, fetch existing or create and save a new registry entity */
    function get(string $serviceName);

    /** resolve service configuration, create a fresh entity each time without writing it into $registry */
    /** @return object|mixed usually object */
    function newEntity(string $serviceName, array $params = []);

    /** resolve the method params and invoke the method */
    function invoke(object $instance, ?string $method = null, array $params = []);
}

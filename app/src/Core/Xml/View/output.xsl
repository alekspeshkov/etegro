<?xml version="1.0"?>
<stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform">
<output method="html" encoding="UTF-8" doctype-system="about:legacy-compat"/>

<template match="*">
    <element name="{name()}" namespace="{namespace-uri()}">
        <apply-templates select="@* | node()"/>
    </element>
</template>

<template match="@*"><copy/></template>
<!--<template match="text()"><value-of select="normalize-space()"/></template>-->

</stylesheet>

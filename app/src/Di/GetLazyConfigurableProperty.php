<?php declare(strict_types=1);

namespace Di;

use Di\Attribute\Lazy;
use Di\Attribute\Required;
use Di\Interface\ContainerInterface;
use Di\ReflectionHelper;
use Di\GetLazyProperty;

/**
 * Lazy configurable property can declare configuration before its injection
 * @property \Di\ConfigRegistry $lazyConfig
 */
trait GetLazyConfigurableProperty
{
    use GetLazyProperty {
        GetLazyProperty::__get as __getAutoConfig;
    }

    const LAZY_CONFIG_PROPERTY = 'lazyConfig';
    #[Lazy] protected ConfigRegistry $lazyConfig;

    /** application dependency injector */
    #[Required] protected ContainerInterface $container;

    function &__get(string $propertyName)
    {
        // lazy creation of lazyConfig property itself
        if ($propertyName === static::LAZY_CONFIG_PROPERTY) {
            $propertyClassName = ReflectionHelper::getPropertyClassName($this, static::LAZY_CONFIG_PROPERTY);

            //create a personal lazy config for each object
            $this->{static::LAZY_CONFIG_PROPERTY} = $this->container->newEntity($propertyClassName);
            return $this->{static::LAZY_CONFIG_PROPERTY};
        }

        /** @var \Di\Interface\ConfigRecordInterface|null $configRecord */
        $configRecord = $this->{static::LAZY_CONFIG_PROPERTY}->get($propertyName);
        if (!$configRecord) {
            return $this->__getAutoConfig($propertyName);
        }

        if ($configRecord->isNameNull()) {
            $propertyClassName = ReflectionHelper::getPropertyClassName($this, $propertyName);
            $configRecord = $configRecord->extendWith($propertyClassName);
        }

        $this->{$propertyName} = $this->container->newEntity(...$configRecord->toArray());
        return $this->{$propertyName};
    }
}

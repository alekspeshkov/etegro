<?php declare(strict_types=1);

namespace Site\Admin;

class Backup
{
    function __construct(
        protected MysqlDump $dumper,
        protected BackupFile $backupFile
    ) {
    }

    function backup()
    {
        $tables = array_merge(
            AdminPage::$tablesNames,
            [
                PageSite::$tableName,
                \Core\Page\LayoutContent::$tableName,
                \Core\Page\PageRouting::$tableName,
                \Core\User\UserAuth::$tableName,
                \Core\User\UserRoles::$tableName,
                \Core\Action\SessionHandler::$tableName,
            ]
        );
        $dump = $this->dumper->backup($tables);
        $this->backupFile->backup($dump);
    }
}

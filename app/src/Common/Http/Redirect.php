<?php declare(strict_types=1);

namespace Common\Http;

use Common\Str;

class Redirect
{
    protected string $url;

    function __construct(
        protected Request $request,
        protected Response $response
    ) {
    }

    function setPostBackPath(string $path)
    {
        $url = Str::concatUri([$this->request->getBaseUrl(), $path]);
        $this->url = $url;
    }

    function setRedirect(?int $status = 303)
    {
        $url = $this->url ?? $this->request->getFullUrl();
        $this->response->setRedirect($url, $status);
    }
}

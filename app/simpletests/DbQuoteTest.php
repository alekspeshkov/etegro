<?php declare(strict_types=1);

namespace test;

use UnitTestCase;
use Common\Db\Connection as Db;

class DbQuoteTest extends UnitTestCase
{
    protected Db $db;

    function __construct() {
        $this->db = new Db($_ENV['MYSQL_DSN'], $_ENV['MYSQL_USER'], $_ENV['MYSQL_PASSWORD']);
    }

    function test_quote() {
        $db = $this->db;
        $this->assertIdentical('0', $db->quoteParameter(0));
        $this->assertIdentical("'0'", $db->quoteParameter('0'));
        $this->assertIdentical('0', $db->quoteParameter(false));
        $this->assertIdentical('1', $db->quoteParameter(+1.0));
        $this->assertIdentical('-1.1', $db->quoteParameter(-1.1));
        $this->assertIdentical('1', $db->quoteParameter(true));
        $this->assertIdentical("''",  $db->quoteParameter(''));
        $this->assertIdentical("'A'", $db->quoteParameter("A"));
        $this->assertIdentical("'\\'\\\"'", $db->quoteParameter("'\""));
        $this->assertIdentical('NULL', $db->quoteParameter(null));
        $this->assertIdentical('NULL', $db->quoteParameter([null]));
        $this->assertIdentical("'foo'", $db->quoteParameter(['foo']));

        $this->assertIdentical("0,1", $db->quoteParameter([0,1]));
        $this->assertIdentical("0,1", $db->quoteParameter(['a'=>0,1]));
        $this->assertIdentical("(NULL,(NULL)),(0,'1')",
            $db->quoteParameter([
                ['a'=>null,'b'=>[null]],
                ['c'=>0,'d'=>'1']
            ])
        );

    }

    function test_quoteInto() {
        $db = $this->db;
        $this->assertIdentical('fo??o', $db->quoteInto('fo??o'));
        $this->assertIdentical('foNULL?o', $db->quoteInto('fo??o',[[null]]));
    }

}

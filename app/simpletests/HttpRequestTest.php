<?php declare(strict_types=1);

namespace test;

use UnitTestCase;
use Common\Http\Request;
use Common\Http\ServerData;

class HttpRequestTest extends UnitTestCase
{
    function test_construct() {
        $server = $_SERVER;

        $_SERVER['SCRIPT_URI'] = null;

        $scheme = $_SERVER['REQUEST_SCHEME'] ?? 'http';
        $host = $_SERVER['HTTP_HOST'];
        $uri = "$scheme://$host";

        $index = ['REQUEST_URI' => 0, 'PATH_INFO' => 1, 'base' =>2, 'path' => 3];
        $test = [
            ['/', '', '/', ''],
            ['/', '/', '/', ''],
            ['a', '', 'a', ''],
            ['/a', '/a', '/', 'a'],
            ['/a/', '/', '/a/', ''],
            ['a/b/c', '/b/c', 'a/', 'b/c'],
        ];

        foreach ($test as $case) {
            $_SERVER['REQUEST_URI'] = $case[$index['REQUEST_URI']];
            $_SERVER['PATH_INFO'] = $case[$index['PATH_INFO']];

            $serverData = new ServerData;
            $request = new Request($serverData);
            $base = $request->getBaseUrl();
            $path = $request->getPath();

            $this->assertIdentical($base, $uri . $case[$index['base']]);
            $this->assertIdentical($path, $case[$index['path']]);
            $this->assertIdentical($base . $path, $uri . $case[$index['REQUEST_URI']]);
        }

        $_SERVER = $server;
    }

}

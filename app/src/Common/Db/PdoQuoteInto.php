<?php declare(strict_types=1);

namespace Common\Db;

/** Designed to work only with MySQL driver */
trait PdoQuoteInto
{
    /** @var \PDO $this */

    /** @method string quote() quote(string $string, int $type = PDO::PARAM_STR) */

    /**
     * - excess arguments ignored with assertion warning
     * - missing arguments will not change '?' or '~' in template
     */
    function quoteInto(string $sql, array $args = []): string
    {
        $head = '';
        $tail = $sql;

        foreach ($args as $arg) {
            $after = strpbrk($tail, '?~');

            if ($after === false) {
                trigger_error('Ignoring extra parameters in quoteInto()');
                break;
            }

            $head .= substr($tail, 0, -strlen($after));

            switch ($after[0]) {
                case '?': {
                        $head .= $this->quoteParameter($arg);
                        break;
                    }
                case '~': {
                        $head .= $this->quoteIdentifier($arg);
                        break;
                    }
                default: {
                        $head .= $after[0]; // pass as is
                    }
            }

            $tail = substr($after, 1);
        }

        return $head . $tail;
    }

    function quotePair(string $sql, $col, $value): string
    {
        $head = '';
        $tail = $sql;

        $quotedCol = $this->quoteIdentifier($col);
        $quotedVal = $this->quoteParameter($value);

        while (($after = strpbrk($tail, '?~')) !== false) {
            $head .= substr($tail, 0, -strlen($after));

            switch ($after[0]) {
                case '?':
                    $head .= $quotedVal;
                    break;

                case '~':
                    $head .= $quotedCol;
                    break;

                default:
                    $head .= $after[0]; // pass as is

            }

            $tail = substr($after, 1);
        }

        return $head . $tail;
    }

    function quoteParameter(mixed $param, bool $arrayInBraces = false): string
    {
        if (is_array($param)) {
            $array = array_map(fn ($p) => $this->quoteParameter($p, true), $param);
            $csv = implode(',',$array);
            return $arrayInBraces ? "({$csv})" : $csv;
        } elseif (is_string($param) || $param instanceof \Stringable) {
            return $this->quote($param);
        } elseif (is_int($param) || is_float($param)) {
            return "{$param}";
        } elseif (is_bool($param)) {
            return $param ? '1' : '0';
        } elseif (is_null($param)) {
            return 'NULL';
        }

        throw new \InvalidArgumentException('Invalid argument of type ' . get_debug_type($param));
    }

    /** @param string|string[] $id */
    function quoteIdentifier(array|string $id): string
    {
        if (is_array($id)) {
            return implode(',', array_map([$this, __FUNCTION__], $id));
        }

        $id = str_replace('`', '``', $id);

        return "`{$id}`";
    }

}

<?php declare(strict_types=1);

namespace Di;

use Di\Interface\AppConfigInterface;
use Di\Interface\ConfigRecordInterface;

class AppConfigRegistry extends ConfigRegistry implements AppConfigInterface
{
    function resolve(string $serviceName, #[\SensitiveParameter] array $params = []): ConfigRecordInterface
    {
        $record = $this->createRecord($serviceName, $params);
        return $this->resolveFrom($record);
    }

    function resolveFrom(ConfigRecordInterface $record): ConfigRecordInterface
    {
        do {
            $isResolved = $this->resolveOnce($record);
        } while (!$isResolved);

        return $record;
    }

    /** @param \Di\Interface\ConfigRecordInterface& $record reference is needed there as ConfigRecordInterface is immutable */
    function resolveOnce(ConfigRecordInterface &$record): bool
    {
        $name = $record->getName();
        $record = $this->extendFrom($record, $name);
        $isResolved = ($name === $record->getName());
        return $isResolved;
    }

    function set(string $serviceName, #[\SensitiveParameter] array|string $params): static
    {
        $record = $this->createRecord($serviceName, $this->normalizeParams($params));
        $name = $record->getName();
        $record = $this->extendParams($record, $name);

        if ($name !== $serviceName) {
            $record = $this->extendParams($record, $serviceName);

            if ($this->hasNoCircularReference($serviceName, $name)) {
                $this->configs[$serviceName] = $record;
            }
        }

        $this->configs[$name] = $record;

        return $this;
    }

    protected function hasNoCircularReference(string $serviceName, string $name): bool
    {
        $key = $name;
        while (isset($this->configs[$key])) {
            if ($serviceName === $key) {
                break;
            }
            if ($key === $this->configs[$key]->getName()) {
                break;
            }
            $key = $this->configs[$key]->getName();
        }

        assert($serviceName !== $key, "Infinite aliasing loop registering service '{$serviceName}' as class '{$name}'");
        return $serviceName !== $key;
    }
}

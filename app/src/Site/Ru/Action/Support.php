<?php declare(strict_types=1);

namespace Site\Ru\Action;

use Common\Http\PostData;
use Common\Http\UploadedFiles;
use Core\Action\ActionInterface;
use Core\Page\CurrentPage;

class Support implements ActionInterface
{
    function __invoke(CurrentPage $currentPage, PostData $postData, UploadedFiles $uploadedFiles)
    {
        $currentPage->formData->setAll($postData->getAll());
        $currentPage->flashData->clear();

        $formData = &$currentPage->formData;
        $flashData = &$currentPage->flashData;

        $visitor = function ($file) use (&$formData, &$flashData) {
            $tmpname = $file['tmp_name'];
            if (is_uploaded_file($tmpname) && $file['size'] > 0) {
                $formData['file_tmp_name'] = $tmpname;
                $formData['file_name'] = $file['name'];
                $formData['file_type'] = $file['type'];
            } else {
                $flashData[$file['input_name']] = UPLOAD_ERR_NO_FILE;
            }
        };

        $errors = $uploadedFiles->visit($visitor);
        $flashData->merge($errors);

        if ($this->isValid($formData, $flashData)) {
            if ($this->sendMail($formData)) {
                $flashData['sent'] = true;
            } else {
                $flashData['unknown_error'] = true;
            }
        }
    }

    protected function isValid($formData, &$flashData)
    {
        $isOk = true;

        if (!$formData['name']) {
            $flashData['name'] = true;
            $isOk = false;
        }

        if (!$formData['email']) {
            $flashData['email'] = true;
            $isOk = false;
        } else {
            if (!$this->isValidEmail($formData['email'])) {
                $flashData['badmail'] = true;
                $isOk = false;
            }
        }

        if (!$formData['serial']) {
            $flashData['serial'] = true;
            $isOk = false;
        } else {
            $serial = $formData['serial'];
            if (!is_numeric($serial) || (int)$serial != $serial) {
                $flashData['badserial'] = true;
                $isOk = false;
            }
        }

        if (!$formData['problem']) {
            $flashData['problem'] = true;
            $isOk = false;
        }

        return $isOk;
    }

    protected function isValidEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            if (!function_exists('checkdnsrr')) {
                // Windows PHP port does not have 'checkdnsrr' function
                return true;
            }
            [, $domain] = explode('@', $email);
            if (checkdnsrr("{$domain}.", 'MX')) {
                return true;
            }
            if (checkdnsrr("{$domain}.", 'A')) {
                return true;
            }
        }
        return false;
    }

    protected function sendMail($formData)
    {
        $email = 'support@etegro.com';

        $headers  = "From: www@etegro.com\r\n";
        $headers .= "Bcc: etegro.webmaster@gmail.com\r\n";

        $reply = $formData['email'];
        if (!empty($reply)) {
            $headers .= "Reply-To: $reply\r\n";
        }

        $subject = 'Запрос поддержки с сайта: ' . $formData['serial'];

        $body  = "Контактное лицо*: " . $formData['name'] . "\n";
        $body .= "Ваш e-mail*: " . $formData['email'] . "\n";
        if ($formData['city']) {
            $body .= "Город: " . $formData['city'] . "\n";
        }
        if ($formData['org']) {
            $body .= "Название организации: " . $formData['org'] . "\n";
        }
        if ($formData['phone']) {
            $body .= "Контактный телефон: " . $formData['phone'] . "\n";
        }
        if ($formData['plan']) {
            $body .= "Гарантийный план: " . $formData['plan'] . "\n";
        }
        $body .= "Серийный номер*: " . $formData['serial'] . "\n";
        if ($formData['model']) {
            $body .= "Модель: " . $formData['model'] . "\n";
        }
        if ($formData['os']) {
            $body .= "Операционная система: " . $formData['os'] . "\n";
        }
        $body .= "Описание проблемы*: " . $formData['problem'];

        if ($formData['file_name']) {
            $uid = uniqid();

            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: multipart/mixed; boundary={$uid}";

            $text = chunk_split(base64_encode($body));

            $body  = "--{$uid}\r\n";
            $body .= "Content-type: text/plain; charset=UTF-8\r\n";
            $body .= "Content-Transfer-Encoding: base64\r\n";
            $body .= "\r\n";

            $body .= $text;
            $body .= "--{$uid}\r\n";

            $body .= "Content-Type: application/octet-stream; name=";
            $body .= '"' . $formData['file_type'] . '"' . "\r\n";
            $body .= "Content-Disposition: attachment; filename=";
            $body .= '"' . $formData['file_name'] . '"' . "\r\n";
            $body .= "Content-Transfer-Encoding: base64\r\n";
            $body .= "\r\n";

            $file_content = file_get_contents($formData['file_tmp_name']);
            $file_content = chunk_split(base64_encode($file_content));
            unlink($formData['file_tmp_name']);

            $body .= $file_content;
            $body .= "--{$uid}--";
        } else {
            $headers .= "Content-type: text/plain; charset=UTF-8\r\n";
            $headers .= "Content-Transfer-Encoding: base64\r\n";
            $body = chunk_split(base64_encode($body));
        }

        $subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';
        //mail($email, $subject, $body, $headers);

        return true;
    }
}

<?php declare(strict_types=1);

namespace Core\Page;

class XmlWriter extends \Common\XmlWriter
{
    static function xmlPage($content)
    {
        return static::xml($content, 'page xmlns:a="page"');
    }
}
